#pragma once

/* Change these to change various things. Some of them probably
 * shouldn't be touched, cause I haven't tested with different settings.
 */

/* By default notefiles made with -F and -l have a .nxt suffix. */
#define TSK_NOTEFILE_SUFFIX ".nxt"

/* The name of the main notefile and the name of any notefiles
 * created with -l. */
#define TSK_NOTEFILE_NAME "notes" TSK_NOTEFILE_SUFFIX

#if 0 /* Does nothing now */
// The number of notes selected <= which notes are fully printed
#define PRINT_FULL_THRESHOLD 1
#endif

/* The format of strings used to print. */
#define TSK_ISO_DATE "%y-%m-%dT%H:%M:%S"
#define TSK_ISO_DATE_NO_T "%y-%m-%d %H:%M:%S"

// You can set truncate len in tsk_options. If you leave it as 0, this value will
// be used. There's no don't-truncate option. Probably there should be.
// Just set it to a big number if you want that.
// Also, truncating isn't just an snprintf. It tries to make it appealing, so
// if truncation would cut the string off in the middle of the word, that word is,
// if possible, removed and " ..." is stuck in there.
//
// The truncate len is quite small because it's used for parts of
// messages.
//
// 		note in cmd <etc>, in item <you know>
//
// Both of those bits in angle brackets are truncated. The whole
// message isn't. Perhaps there should be a second value for whole
// messages. It also might be the case that having one or even two
// numbers for all messages isn't good enough. You *do* want a fairly
// low number here, because if the commands and things are really long
// it looks awful and makes it hard to see error messages. You won't
// notice they're there.
#define TSK_TRUNCATE_LEN 50

#if 0 /* Not used any more */
#define FULL_STOP_AFTER_BODY 1
#endif

#if 0 /* Not used any more */
#define CAPITALISE_SENTENCES 1
#endif

// Colours

/* Default --errors and --autocmd-errors. See item.h for those. */
/* I've set them to everything except ERR_TYPE_NON_CHANGES. */
