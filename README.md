# libnote

This is the backend for note, the cli/tui note-taking app that should
have been calle "tsk."

You could use this to make your own note-taking program, if you
wanted. You won't, and probably shouldn't. I haven't (yet) properly
clearly defined what the interface is. In brief it's the functions
parse\_cmds, do\_actions write\_to\_notefiles.

And there's a note\_options struct, a "print\_ctx" struct. It's messy.
I do mean to polish it until is shines, though. Eventually.

