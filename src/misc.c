#define _GNU_SOURCE
#include "misc.h"
#include "tsk-item.h"
#include "subopt.h"
#include "lib/n-fs/n-fs.h"
#include "lib/rab/rab.h"
#include "useful.h"
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <glob.h>
#include <unistd.h>
#include <sys/stat.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <search.h>
#include "ncmp.h"

bool tsk_all_arr_is (int *arr, int n_arr, int is) {
	bool r = 1;
	for (int *p = arr; p < arr + n_arr; p++) {
		if (*p != is) {
			r = 0;
			break;
		}
	}
	return r;
}

int tsk_index_of_arr (void *it, void *arr, int n, size_t size) {
	for (void *p = arr; p < arr + n; p++) {

		if (!memcmp (it, p, size))
			return p - arr;
	}
	return -1;
}

int tsk_index_of_str_match (char *str, char **arr, size_t n_arr) {
	for (char **a = arr; a < arr + n_arr; a++)
		if (!strcmp (*a, str))
			return a - arr;
	return -1;
}

int *remove_int_dups_inline (int **r, size_t *n_res) {

	// Obviously no need to do anything it it's 0 or 1 long
	if (*n_res == 0 || *n_res == 1)
		return *r;

	int saved_res[*n_res];
	int nsaved_res = *n_res;

	memcpy (saved_res, *r, nsaved_res * sizeof *saved_res);
	$qsort (saved_res, *n_res, ncmp_ints);

	*n_res = 0;
	int *q = *r;

	int this = *saved_res;
	*q++ = this;
	(*n_res)++;
	for (int *p = saved_res; p - saved_res < nsaved_res; p++) {
		if (*p != this) {
			this = *p;
			*q++ = this;
			(*n_res)++;
		}
	}
	assert (*n_res);
	return *r;
}

char *tsk_fmt (char *buf, size_t n_buf, bool dup, char *_fmt, ...) {
	va_list ap;
	va_start (ap, _fmt);

	// There's no point checking this, right? Either it'll be
	// truncated or it won't. Maybe a crash is better.
	vsnprintf (buf, n_buf, _fmt, ap);
	va_end (ap);
	return dup? strdup (buf): buf;
}

int tsk_chomp_char (char *str, char char_to_chomp) {
	char *p = &str[strlen (str) - 1];
	int chars_chomped = 0;
	if (*p == char_to_chomp) {
		while (*p == char_to_chomp) p--, chars_chomped++;
		*++p = '\0';
	}
	return chars_chomped;
}

// Returns: a pointer to the truncated str -- for convenience.
//
// Copies a truncated s to r.
//
// If r ends in the middle of a word, truncate some more.
//
// insert_dots: add dots if the str was truncated.
//
// stop_at_line_end: truncate at newlines.
//
// Doesn't check for buffer overflow. You provide the buffer, and you
// know your truncate_len. You will need to consider the " ..." if you
// do insert_dots.
char *get_truncated_str (tsk_ctx *ctx, char *pr, char *s, size_t truncate_len,
		bool insert_dots, bool stop_at_line_end) {

	if (!truncate_len)
		truncate_len = ctx->opts->truncate_len;
	char *p = s, *q = pr;
	for (; *p && (stop_at_line_end ? *p != '\n' : 1)
			&& p - s < truncate_len; p++, q++) {

		// Not checking if running over buffer.
		if (!*p)
			break;
		*q = *p;
	}
	if (p - s < truncate_len) {
		*q = '\0';
		return pr;
	}

	// If on a word, skip back
	char *marker = --q;
	while (!isspace (*q)) {

		// If there's no spaces you'd segfault without this
		if (q == s) {
			q = marker;
			break;
		}
		q--;
	}

	// Trim white
	while (isspace (*q)) {
		if (q == s) {
			q = marker;
			break;
		}
		q--;
	}
	q++;
	if (insert_dots)
		*q++ = ' ', *q++ = '.', *q++ = '.', *q++ = '.';
	*q = '\0';

	return pr;
}

// Returns a wrapped, truncated string with the quotes escaped. It's
// called get_display_str to help me remember that this is
// the function to call if I want to display a string.
int get_display_str (tsk_ctx *ctx, int n_pr, char *pr, struct display_str_opts *_opts,
		char *str) {
	struct display_str_opts *opts = _opts ?:
		&(struct display_str_opts) {.truncate_len = ctx->opts->truncate_len};

	assert (n_pr > ctx->opts->truncate_len * 3);
	char truncated_buf[BUFSIZ];
			/* Don't forget the 0 */ /* + 1 */
			/* potential dots */ /* + 4]; */
	if (!opts->dont_truncate)
		assert (get_truncated_str (ctx, truncated_buf, str, opts->truncate_len, 1,
					opts->stop_truncation_at_line_end));
	else {

		// This is dumb but I can't be bothered to fix it. memcpying a
		// string into a buffer of size to->truncate_len + whatever
		// isn't "not truncating". It will truncate. Actually, it will
		// probably smash tha stack, as it already has.
		if (!memccpy (truncated_buf, str, 0, sizeof truncated_buf))
			truncated_buf[sizeof truncated_buf - 1] = 0;
	}

	// In the worst case, every character in the buffer would be
	// quotes, so we multiply n truncated_buf by two, and we add two
	// for the quotes at either end.
	char quoted_buf[BUFSIZ];
	assert (get_quote_escaped_str (sizeof quoted_buf, quoted_buf, truncated_buf,
				opts->gqesm_mode) != -1);
	if (opts->no_wrap)

		// Can truncate.
		memcpy (pr, quoted_buf, n_pr);
	else
		assert (wrap (pr, n_pr, opts->wrap_opts, "%s", quoted_buf) != -1);
	return 0;
}

int get_display_strf_ap (tsk_ctx *ctx, int n_pr, char *pr,
		struct display_str_opts *opts, char *fmt, va_list ap) {
	char buf[n_pr];
	assert (vsnprintf (buf, n_pr, fmt, ap) != -1);
	return get_display_str (ctx, n_pr, pr, opts, buf);
}

int get_display_strf (tsk_ctx *ctx, int n_pr, char *pr, struct display_str_opts *opts,
		char *fmt, ...) {
	va_list ap;
	va_start (ap);
	int rc = get_display_strf_ap (ctx, n_pr, pr, opts, fmt, ap);
	va_end (ap);
	return rc;
}

bool tsk_is_all_x (char *s, char *e, int (*cmpfunc) (int),
		bool ignore_trailing_white)  {
	if (!e)
		e = strchr (s, '\0');
	while (*s && s < e) {
		if (!cmpfunc (*s)) {
			if (ignore_trailing_white && isspace (*s)) {
				for (; *s && s - e; s++) {
					if (!isspace (*s)) {
						return 0;
					}
				}
				return 1;
			}
			return 0;
		}
		s++;
	}
	return 1;
}

bool tsk_contains_unescaped_strs (char *str, char **strs, int n_strs) {
	char *p;
	$fori (i, n_strs) {
		if (p = strstr (str, strs[i]))
			if (p == str || *(p - 1) != '\\')
				return 1;
	}
	return 0;
}

char *tsk_get_str (char *fmt, ...) {
	va_list ap;
	va_start (ap);
	char *str;
	int bytes = vasprintf (&str, fmt, ap);
	va_end (ap);
	if (bytes == -1)
		return 0;
	return str;
}


bool tsk_file_is_in_dir (char *file, char *dir) {
	DIR *dp = opendir (dir);
	bool r = 0;
	if (!dp)
		return 0;
	struct dirent *dirent;
	while (dirent = readdir (dp)) {
		if ($strmatch (dirent->d_name, file)) {
			r = 1;
			break;
		}
	}
	if (closedir (dp)) {
		// Do nothing, I guess. Who give a shit?
	}
	return r;
}


int tsk_mkdir_if_not_exist (char *path, mode_t mode) {
	if (mkdir (path, mode)) {
		if (errno != EEXIST)
			return 1;
	}
	errno = 0;
	return 0;
}

// NOTE: not buffer overflow checking
char *tsk_get_strs_with_commas (char *buf, size_t max_n_buf,
		char **strs, size_t n_strs, char *joining_word) {

#define return_NULL_if_over if (n_buf >= max_n_buf) return 0;

	if (n_strs == 1) {
		sprintf (buf, "%s", strs[0]);
		return buf;
	}

	int n_buf = 0;
	// I'm checking if you're over after every write. I think you have to
	// do this.
	$fori (i, n_strs) {
		if (i > 0) {
			if (i < n_strs - 1)
				n_buf += sprintf (buf + n_buf, ", ");
			else
				n_buf += sprintf (buf + n_buf, " %s ", joining_word);
			return_NULL_if_over;
		}
		n_buf += sprintf (buf, "%s", strs[i]);
		return_NULL_if_over;
	}
	return_NULL_if_over;
	return buf;
}

// Returns 1 for a failure. The actual regex success or failure is
// returned in pr.
int tsk_regmatch (bool *pr, char *str, char *pattern_s, char *pattern_e,
		char err_buf[static TSK_MAX_ERR_BUF]) {
	assert (pattern_e);

	// We temporarily add a zero to where pattern_e is. I think this
	// is amazing. But it might be bad form.
	char orig_pattern_e_char = *pattern_e;
	*pattern_e = 0;

	regex_t regex;
	int r = 1;
	if (0 != (r = regcomp (&regex, pattern_s, REG_EXTENDED | REG_NOSUB))) {
		regerror (r, &regex, err_buf, TSK_MAX_ERR_BUF);
		goto out;
	};
	*pr = !regexec (&regex, str, 0, 0, 0);
	regfree (&regex);
	r = 0;
out:
	*pattern_e = orig_pattern_e_char;
	return r;
}

char *tsk_strrchrstart (char *s, char the_char) {
	char *r = strchr (s, '\0');
	if (r = strrchr (r, the_char))
		return r;
	return s;
}

void tsk_chomp_newlines (char *s) {
	char *p = strchr (s, '\0') - 1;
	if (p < s)
		return;
	while (*p == '\n')
		p--;
	*++p = '\0';
}

void tsk_strs (struct tsk_strs *tsk_strs) {
	struct tsk_strs out = {};
	arr_reserve (&out, tsk_strs->n);
	arr_each (tsk_strs, _) {
		if (!arr_find (&out, &_, ncmp_strs))
			arr_add (&out, strdup (*_));
	}
	$free_arr (tsk_strs->d,  tsk_strs->n);
	free (tsk_strs->d);
	*tsk_strs = out;
}

// Doesn't allocate, returns pointer to buf.
//
// n_buf includes the '\0'.

char *tsk_get_quoted_str_if_spaces (char *r, size_t a_res, size_t *n_buf,
		char *str) {
	if (strpbrk (str, (char []) {'\t', ' ', '\0'})) {
		size_t ret = snprintf (r, a_res, "\"%s\"", str);
		if (n_buf)
			*n_buf = ret;
	} else r = str;
	return r;
}

int tsk_line_len_without_spaces (char *p) {
	char *end = strchrnul (p, '\n');
	end--;
	while (isspace (*end))
		end--;
	return end++, end - p;
}

/*
 * Escape "str" for use as a shell argument with system ().
 * This uses single quotes, except when we know we need to use double quotes
 * (MS-DOS and MS-Windows without 'shellslash' set).
 * Escape a newline, depending on the 'shell' option.
 * When "do_special" is TRUE also replace "!", "%", "#" and things starting
 * with "<" like "<cfile>".
 * When "do_newline" is FALSE do not escape newline unless it is csh shell.
 * Returns the result in allocated memory, 0 if we have run out.
 */
char *tsk_vim_strsave_shellescape (char *str, int do_newline) {
	unsigned	length;
	char		*p;
	char		*d;
	char		*escaped_str;

	// Only csh and similar shells expand '!' within single quotes.
	// For sh and the like we must not put a backslash before it, it
	// will be taken literally.  If do_special is set the '!' will be
	// escaped twice. Csh also needs to have "\n" escaped twice when
	// do_special is set.

	// First count the number of extra bytes required.
	length = (unsigned) strlen (str) + 3;  // two quotes and a trailing NUL
	for (p = str; *p; p++) {
		if (*p == '\'')
			length += 3;		// ' => '\''
		if ((*p == '\n' && do_newline))
			++length;			// insert backslash
	}

	// Allocate memory for the result and fill it.
	escaped_str = u_malloc (length);
	if (escaped_str != 0)
	{
		d = escaped_str;

		// add opening quote
		*d++ = '\'';

		for (p = str; *p != '\0'; ) {
			if (*p == '\'')
			{
				*d++ = '\'';
				*d++ = '\\';
				*d++ = '\'';
				*d++ = '\'';
				++p;
				continue;
			}
			if (*p == '\n' && do_newline) {
				*d++ = '\\';
				*d++ = *p++;
				continue;
			}
			*d++ = *p++;
		}

		// add terminating quote and finish with a NUL
		*d++ = '\'';
		*d = '\0';
	}

	return escaped_str;
}

// Ignoring spaces in the cmp, that is. I suppse this function would
// match with = "this     ".
bool tsk_strnmatch_ignore_spaces (char *cmp, char *with, size_t n_with) {
	while (isspace (*cmp)) cmp++;
	return !strncmp (cmp, with, n_with)? 1: 0;
}

struct tsk_ints regexec_arr (size_t n_arr, char **arr, regex_t *preg) {
	struct tsk_ints r = {};
	u_each (a, n_arr, arr) {
		if (0 == regexec (preg, *a, 0, 0, 0))
			arr_add (&r, a - arr);
	}
	return r;
}

int regmatch_arr (struct tsk_ints *pr, char *pattern, size_t n_arr, char **arr,
		char err_buf[static TSK_MAX_ERR_BUF]) {
	regex_t regex;
	int ret = regcomp (&regex, pattern, REG_EXTENDED | REG_NOSUB);
	if (ret) {
		char msg_buf[TSK_MAX_ERR_BUF - 100];
		regerror (ret, &regex, msg_buf, sizeof msg_buf);
		snprintf (err_buf, TSK_MAX_ERR_BUF, "Couldn't compile regex: %s", msg_buf);
		return 1;
	};

	*pr = regexec_arr (n_arr, arr, &regex);
	return 0;
}

char *tsk_get_editor (tsk_ctx *ctx, tsk_cmd *cmd) {
	char *editor = getenv ("VISUAL");
	if (editor)
		return editor;

	editor = getenv ("EDITOR");
	if (editor)
		return editor;

	editor = n_fs_exepath ("vim");
	if (editor)
		return editor;
	editor = n_fs_exepath ("vi");
	if (editor)
		return editor;
	editor = n_fs_exepath ("emacs");
	if (editor)
		return editor;
	editor = n_fs_exepath ("nvim");
	if (editor)
		return editor;
	editor = n_fs_exepath ("vis");
	if (editor)
		return editor;

	tsk_warn (ctx, cmd, "\
EDITOR and VISUAL not set. Various other editors tried aren't in your PATH");
	return 0;
}

int tsk_glob_path (tsk_ctx *ctx, tsk_cmd *cmd, char *pattern, char *dir,
		struct tsk_strs *out) {

	glob_t gb;
	char full_pattern[PATH_MAX];
	int r = 0;
	if (dir)
		r = snprintf (full_pattern, PATH_MAX, "%s/%s", dir, pattern);
	else
		r = snprintf (full_pattern, PATH_MAX, "%s", pattern);
	if (r > PATH_MAX)

		// This tsk_err is fine. It's only used in tsk_parse_cmd
		// and there's no error to print.
		return tsk_warn (ctx, cmd, "Your data_dir plus pattern is greater than PAX_MAX");

	r = glob (full_pattern, 0, 0, &gb);
	switch (r) {
		case GLOB_NOSPACE: assert (0);
		case GLOB_ABORTED: assert (0);
		case GLOB_NOMATCH: goto fail;
		default: break;
	}

	out->d = gb.gl_pathv;
	out->n = out->a = gb.gl_pathc;

	return 0;
fail:
	return 1;
}

char *str_toupper (char *r, int n_buf, char *str) {
	char *q = r;
	char *p;
	for (p = str; *p; p++) {
		*q++ = toupper (*p);
	}
	*q = 0;
	assert (p - str < n_buf);
	return r;
}

bool is_unescaped_chr (char *str, char *p, char chr) {
	if (*p == chr && p == str
			|| *p == chr && p > str && *(p - 1) != '\\')
	/* if (p > str && (*p == chr && *(p - 1) != '\\') */
	/* 		|| p == str) */
		return 1;
	return 0;
}

int join_and_quote (int n_pr, char *pr, size_t n_strs, char **strs) {

	// I think it's reasonable to make sure that the buffer comes out
	// of this function "valid", ie at least an empty string.
	*pr = 0;
	if (!n_strs)
		return 0;
	FILE *f = fmemopen (pr, n_pr, "w");
	assert (f);

	char word_buf[BUFSIZ];
	u_each (_, n_strs, strs) {
		int len = get_quote_escaped_str (sizeof word_buf, word_buf, *_,
				GQESO_SURROUND_WITH_QUOTES_IF_SPACES);
		fprintf (f, "%.*s", len, word_buf);
		if (_ - strs != n_strs - 1)
			fputc (' ', f);
	}

	fclose (f);
	pr[n_pr - 1] = 0;
	return 1;

}

// Note that this doesn't allocate. It just returns a pointer
// to the basename if the notefile is a shortcut notefile.
char *get_pretty_notefile_name (tsk_notefile *notefile) {
	char *r;
	if (notefile->flags & TSK_FILE_FLAG_IS_SHORTCUT) {
		if (r = strrchr (notefile->path, '/'))
			r++;
		else
			r = notefile->path;
	} else
		r = notefile->path;
	return r;
}

enum prop_id time_id_to_prop (enum time_id time_id) {
	switch (time_id) {
		case TIME_ON:              return NP_ON;
		case TIME_POSTPONE:        return NP_POSTPONE;
		case TIME_BEFORE:          return NP_BEFORE;
		case TIME_ENDING:          return NP_ENDING;
		case TIME_INTERVAL:        return NP_INTERVAL;
		case TIME_STOP_REPEATING:  return NP_STOP_REPEATING;
		case TIME_EXCEPT:          return NP_EXCEPT;
		case TIME_EXCEPT_INTERVAL: return NP_EXCEPT_INTERVAL;
		case TIME_CREATION:        return -1;
		case TIME_NOW:             return -1;
		case TIME_REMINDER:        return NP_REMINDER;
		case TIME_LAST_REMINDED:   return NP_LAST_REMINDED;
		case N_TIMES: assert (0);
	}
	assert (0);
}

enum item_type prop_id_to_item_type (enum prop_id prop_id) {
	switch (prop_id) {
	case NP_TAG:               return IT_TAG;
	case NP_ANNOTATION:        return IT_ANNOTATION;
	case NP_LINK:              return IT_LINK;
	case NP_ATTACHMENT:        return IT_ATTACHMENT;
	default: assert (0);
	}
	assert (0);
}

int append_to_comma_list_indexes (struct tsk_ints *list, char *s, char *e,
		int n_keywords, void *keywords, rab_getter *getter,
		char err_buf[static TSK_MAX_ERR_BUF]) {
	int new[n_keywords];
	int n_new = get_comma_list_indexes (s, e, n_keywords, keywords, getter, err_buf,
			n_keywords, new);
	if (n_new > 0)
		arr_pusharr (list, new, n_new);
	return 0;
}

int get_comma_list_indexes (char *s, char *e, int n_objs, void *objs,
		rab_getter *getter, char err_buf[static TSK_MAX_ERR_BUF],
		int max_out, int out[max_out]) {

	// We return 0 if nothing is passed. This makes letting you do
	// Help -- no arg -- easier of me. It's not going to lead you to
	// writing Remove has= because has requires an argument.
	if (!s)
		return (unsigned) -1;
	assert (e);

	char *duped = strndupa (s, e - s);
	int __matches[64];
	rab_completion_matches matches = {.idxes = __matches};
	int r_n_out = 0;
	for (;;) {
		char *token = strsep (&duped, ",");
		if (!token)
			break;
		if  (!*token) {
			snprintf (err_buf, TSK_MAX_ERR_BUF, "Couldn't get a token");
			return -1;
		}
		int rc = rab_get_completion (token, n_objs, objs, getter, n_objs, &matches);
		assert (!rc);
		assert (matches.count < u_len (__matches));
		if (matches.count > 1) {
			snprintf (err_buf, TSK_MAX_ERR_BUF, "Too many matches for %s", token);
			return -1;
		} else if (matches.count == 1)
			out[r_n_out++] = matches.idxes[0];
		else {
			snprintf (err_buf, TSK_MAX_ERR_BUF, "No match for \"%s\"", token);
			return -1;
		}
	}
	return r_n_out;
}

int get_bitfield_from_comma_list (char *s, char *e, int n_objs, void *objs,
		rab_getter *getter, char err_buf[static TSK_MAX_ERR_BUF], int *out) {

	// We could not initialise it. That's more flexible. But this is
	// more convenient.
	*out = 0;
	int matches[n_objs];
	int n_matches = get_comma_list_indexes (s, e, n_objs, objs, getter,
			err_buf, u_len (matches), matches);
	if (n_matches == -1)
		return 1;
	for (int i = 0; i < n_matches; i++)
		*out |= (1 << matches[i]);
	return 0;
}

// Returns the number of matches.
int bitfield_to_int_indexes (int *pr, int bitfield,
		int max_bits) {
	int r = 0;
	$fori (i, max_bits) {
		int bit = bitfield  & (1 << i);
		if (bit)
			pr[r++] = i;
	}
	return r;
}

// Returns 1 for a proper failure and an empty string for no match.
int get_flag_comma_string (int n_pr, char *pr, int bitfield, int max_bits_and_strings,
		struct subopt *subopts) {
	int match_idxes[N_PROPS] = {};
	int n_match_idxes = bitfield_to_int_indexes (match_idxes,
			bitfield, max_bits_and_strings);
	if (!n_match_idxes)
		return 0;

	FILE *f = fmemopen (pr, n_pr, "w");
	assert (f);
	$fori (i, n_match_idxes) {
		fputs (subopts[match_idxes[i]].lng, f);
		if (i != n_match_idxes - 1)
			fputc (',', f);
	}
	fflush (f);
	return 0;
}

int get_quote_escaped_str (size_t n_pr, char *pr, char *str,
		enum get_quote_escaped_str_mode mode) {
	FILE *f = fmemopen (pr, n_pr * sizeof *pr, "w");
	bool added_initial_quote = 0;
	int bottom_bits = mode &= 0x0000ffff;
	int top_bits = mode >> 8;
	if (bottom_bits == GQESO_SURROUND_WITH_QUOTES_ALWAYS ||
			mode == GQESO_SURROUND_WITH_QUOTES_IF_SPACES && strpbrk (str, "\t \n")) {
		fputc ('"', f);
		added_initial_quote = 1;
	}

	if (top_bits & GQESO_DONT_ESCAPE)
		fputs (str, f);
	else
		u_each_sent (_, str) {
			if (*_ == '"' && !top_bits & GQESO_DONT_ESCAPE)
				fputc ('\\', f);
			fputc (*_, f);
		}
	if (added_initial_quote)
		fputc ('"', f);

	int r = ftell (f);
	fclose (f);
	return r;
}

// Returns the match or -1 for no match
struct match_longest_str_info match_longest_str (const char *s, int n_strs,
		const char **strs) {
	match_longest_str_info	r = {.idx = -1};

	for (int i = 0; i < n_strs; i++) {

		// We get the length of your token with strchrnul. This will
		// work with, eg, "notef=/tmp/whatever". There is a corner
		// case. Say you have an option, "n", which takes an arbitrary
		// string. You might end up typing "notefile=". This isn't a
		// problem in getopt because they sensibly make you preface
		// your args with hyphens, but it is one here. But it's not a
		// real problem. Rather, if a user (me) did type something
		// like that, I could just change the short option so that
		// doesn't happen. Here, you'd never type "notefile=" by
		// accident since "n" takes a index list, not a string.
		int this_len = strchrnul (s, '=') - s;
		if (strs[i] && !strncmp (s, strs[i], this_len)) {
			if (strs[i][this_len] == 0) {
				r.matching_len = this_len;
				r.idx = i;
				break;
			}

			if (this_len > r.matching_len) {
				r.matching_len = this_len;
				r.idx = i;
			}
		}
	}
	return r;
}
