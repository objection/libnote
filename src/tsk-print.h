#pragma once
#include "tsk-types.h"
#include "tsk-item.h"

enum tsk_msg_progress_state  {
	TMPS_BEGIN,
	TMPS_IN_PROGRESS,
	TMPS_DONE,
};

enum tsk_confirmation_type {
	TCT_SUPER_LOCK,
	TCT_LOCK,
	TCT_CONFIRM,
	TCT_NO_CONFIRMATION_NECESSARY,
};

typedef struct tsk_confirmation tsk_confirmation;
struct tsk_confirmation {
	struct tsk_item *old_item, *new_item;
	enum tsk_confirmation_type type;
	enum prop_id changed_props;
	int user_choice;
	bool user_confirms;
	void *user;
};
$make_arr (struct tsk_confirmation, tsk_confirmations);

typedef struct tsk_print_ctx tsk_print_ctx;
struct tsk_print_ctx {

	// Can't put the note
	void *user; // Yours
	FILE *f;
	int lines, cols;

	int colour_index; // Print fills this out
	bool is_error;

	// This is an atrocious thing that might go away. It'll be  in
	// TMPS_BEGIN when you first get it in your pc.print function.
	// This is so you can clear the screen or whatever you need to do.
	// From there on it'll be in TMPS_IN_PROGRESS. After everything's
	// done the func will be called again with print_progress =
	// TMPS_DONE. So you know it's all over.
	//
	// Note that in TMPS_BEGIN and TMPS_DONE, func will be called with
	// fmt = 0.
	enum tsk_msg_progress_state progress;

	// Putting these functions in here so my functions aren't
	// ridiculously long. print is the one that just prints, eg,
	// something that calls printf.
	int (*print) (struct tsk_print_ctx *, char *, ...);
	int (*msg) (struct tsk_print_ctx *, char *, ...);

	// For turning on colour. This will only work with things that
	// "turn" on colour or attributes in some way, like terminals and
	// escape sequences, ncurses and attr{on/off}. Maybe html.
	//
	// It returns a length. In ncurses there's not "length" to turning
	// on a colour, but using escape sequences there is.
	int (*colour) (struct tsk_print_ctx *);

	bool (*confirm) (struct tsk_print_ctx *, char *fmt, ...);
	bool (*confirm_super_lock_change) (struct tsk_print_ctx *, struct tsk_confirmation *);
	bool (*confirm_change) (struct tsk_print_ctx *, struct tsk_confirmation *);
	int (*multi_confirm) (tsk_ctx *, tsk_cmd *cmd,
			struct tsk_confirmations *);
	int (*input) (int, char *, struct tsk_print_ctx *, char *, ...);
};

int tsk_print (tsk_ctx *ctx, tsk_cmd *cmd, view_node view_node, int view_cursor,
		bool props_were_specified, enum prop_id print_what);
int tsk_print_fmt (tsk_ctx *ctx, tsk_cmd *cmd, int cursor, struct action_piece action_piece,
		struct tsk_items *items, struct tsk_statement *statement)
	TSK_HIDE;
