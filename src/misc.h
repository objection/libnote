#pragma once
#include "lib/rab/rab.h"
#include "tsk-err.h"
#include "tsk-item.h"
#include "tsk-types.h"
#include "wrap.h"
#include <dirent.h>
#include <fnmatch.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define TSK_N_SCRATCH 1024 * 4

enum get_quote_escaped_str_mode {
	GQESO_SURROUND_WITH_QUOTES_ALWAYS = 1,
	GQESO_SURROUND_WITH_QUOTES_IF_SPACES,
	GQESO_DONT_ESCAPE = 1 << 8,
};

typedef struct match_longest_str_info match_longest_str_info;
struct match_longest_str_info {

	// The index of the match.
	int idx;

	// The number of chars that match.
	int matching_len;
};

struct display_str_opts {
	struct wrap_opts *wrap_opts;
	bool stop_truncation_at_line_end;
	bool no_wrap;
	enum get_quote_escaped_str_mode gqesm_mode;

	// Without this, we'll use to->truncate_len.
	int truncate_len;
	bool dont_truncate;
};
typedef struct display_str_opts dso;

bool tsk_all_arr_is (int *arr, int n_arr, int is)
	TSK_HIDE;
int tsk_index_of_arr (void *it, void *arr, int n, size_t size )
	TSK_HIDE;
int tsk_set_difference (struct tsk_ints *buf, int *b, size_t nb)
	TSK_HIDE;
void tsk_and (struct tsk_ints *r, int *list2, size_t nlist2, bool rmdups)
	TSK_HIDE;
char *tsk_fmt (char *buf, size_t nbuf, bool dup, char *_fmt, ...)
	TSK_HIDE;
char *tsk_get_number_suffix (size_t _num)
	TSK_HIDE;
int tsk_chomp_char (char *str, char char_to_chomp)
	TSK_HIDE;
char *get_truncated_str (tsk_ctx *ctx, char *pr, char *s, size_t truncate_len,
		bool insert_dots, bool stop_at_line_end)
	TSK_HIDE;
bool tsk_is_all_x (char *s, char *e, int (*cmpfunc) (int),
		bool ignore_trailing_white)
	TSK_HIDE;
int tsk_index_of_str_match (char *str, char **arr, size_t n_arr)
	TSK_HIDE;
bool tsk_contains_unescaped_strs (char *str, char **strs, int n_strs)
	TSK_HIDE;
char *tsk_get_str (char *fmt, ...)
	TSK_HIDE;
char **tsk_paths_of_files_in_dir_glob (char *pattern, char *dir,
		size_t *n_res, mode_t mode, bool full_path)
	TSK_HIDE;
bool tsk_file_is_in_dir (char *file, char *dir)
	TSK_HIDE;
int tsk_mkdir_if_not_exist (char *path, mode_t mode)
	TSK_HIDE;
char *tsk_get_strs_with_commas (char *buf, size_t max_n_buf, char **strs, size_t
		n_strs, char *joining_word)
	TSK_HIDE;
int *tsk_get_removed_dups_ints (int *ints, size_t n, size_t *res_n)
	TSK_HIDE;
int tsk_regmatch (bool *pr, char *str, char *pattern_s, char *pattern_e,
		char err_buf[static TSK_MAX_ERR_BUF])
	TSK_HIDE;
char *tsk_get_editor (tsk_ctx *ctx, tsk_cmd *cmd)
	TSK_HIDE;
char *tsk_strrchrstart (char *s, char the_char)
	TSK_HIDE;
void tsk_chomp_newlines (char *s)
	TSK_HIDE;
void tsk_strs_remove_dups (struct tsk_strs *tsk_strs)
	TSK_HIDE;
char *tsk_get_quoted_str_if_spaces (char *buf, size_t a_buf, size_t *n_buf,
		char *str)
	TSK_HIDE;
int tsk_line_len_without_spaces (char *p)
	TSK_HIDE;
bool tsk_intnmatch (int *a, int *b, size_t n)
	TSK_HIDE;
char *tsk_vim_strsave_shellescape (char *str, int do_newline)
	TSK_HIDE;
bool tsk_strnmatch_ignore_spaces (char *cmp, char *with, size_t n_with)
	TSK_HIDE;
struct tsk_ints regexec_arr (size_t n_arr, char **arr, regex_t *preg)
	TSK_HIDE;
int tsk_glob_path (tsk_ctx *ctx, tsk_cmd *cmd, char *glob_pattern, char *dir, struct tsk_strs *out)
	TSK_HIDE;
char *str_toupper (char *r, int n_buf, char *str)
	TSK_HIDE;
bool is_unescaped_chr (char *str, char *p, char chr)
	TSK_HIDE;
int join_and_quote (int n_pr, char *pr, size_t n_strs, char **strs)
	TSK_HIDE;
char *get_pretty_notefile_name (struct tsk_notefile *notefile)
	TSK_HIDE;
int *remove_int_dups_inline (int **r, size_t *n_res)
	TSK_HIDE;
enum prop_id time_id_to_prop (enum time_id time_id)
	TSK_HIDE;
int get_comma_list_indexes (char *s, char *e, int n_objs, void *objs,
		rab_getter *getter, char err_buf[static TSK_MAX_ERR_BUF], int max_out,
		int out[max_out])
	TSK_HIDE;
int append_to_comma_list_indexes (struct tsk_ints *list, char *s, char *e,
		int n_objs, void *objs, rab_getter *getter, char err_buf[static TSK_MAX_ERR_BUF])
	TSK_HIDE;
int get_bitfield_from_comma_list (char *s, char *e, int n_objs, void *objs,
		rab_getter *getter, char err_buf[static TSK_MAX_ERR_BUF], int *out)
	TSK_HIDE;
int get_quote_escaped_str (size_t n_pr, char *pr, char *str,
		enum get_quote_escaped_str_mode mode)
	TSK_HIDE;
enum item_type prop_id_to_item_type (enum prop_id prop_id)
	TSK_HIDE;
int get_display_str (tsk_ctx *ctx, int n_pr, char *pr, struct display_str_opts *_opts,
		char *str)
	TSK_HIDE;
int get_display_strf_ap (tsk_ctx *ctx, int n_pr, char *pr, struct display_str_opts *opts,
		char *fmt, va_list ap)
	TSK_HIDE;
int get_display_strf (tsk_ctx *ctx, int n_pr, char *pr, struct display_str_opts *opts,
		char *fmt, ...)
	TSK_HIDE;
int regmatch_arr (struct tsk_ints *pr, char *pattern, size_t n_arr, char **arr,
		char err_buf[static TSK_MAX_ERR_BUF])
	TSK_HIDE;
