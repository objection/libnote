#pragma once

#include "tsk-item.h"

tsk_view *view_clone (tsk_view *view)
	TSK_HIDE;
int view_free (tsk_view *view)
	TSK_HIDE;
int view_set_diff (tsk_view *a, tsk_view *b)
	TSK_HIDE;
int view_copy (tsk_view *to, const tsk_view *from)
	TSK_HIDE;
void view_and (tsk_view *a, tsk_view *b)
	TSK_HIDE;
int view_remove_dups (tsk_view *view)
	TSK_HIDE;
