#pragma once
#include "tsk-item.h"

int action_change (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *view, int view_cursor,
		struct action_piece action_piece, struct tsk_statement *statement)
	TSK_HIDE;
int init_added_items (tsk_ctx *ctx, tsk_cmd *cmd, struct action action, struct tsk_items *items,
		struct tsk_statement *statement)
	TSK_HIDE;
int tsk_change_in_editor (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *view, struct tsk_statement *statement,
		struct mod_piece edit_in_editor_mod_piece);
int action_change_prop (tsk_ctx *ctx, tsk_view *view, int view_cursor,
		struct action_piece action_piece)
	TSK_HIDE;
