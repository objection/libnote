#pragma once
#include <stdio.h>
#include "tsk-types.h"
#include "tsk-item.h"

#define BEGIN_MSG($ctx) \
	do { \
		$ctx->pc->progress = TMPS_BEGIN; \
		$ctx->pc->msg (ctx->pc, 0); \
		$ctx->pc->progress = TMPS_IN_PROGRESS; \
	} while (0)
#define END_MSG($ctx) \
	do { \
		ctx->pc->colour_index = TSK_COLOUR_OFF; \
		ctx->pc->progress = TMPS_DONE; \
		ctx->pc->msg (ctx->pc, 0); \
	} while (0)

#define add_text($ctx, ...) $ctx->pc->msg ($ctx->pc __VA_OPT__(,) __VA_ARGS__)

int tsk_plain_msg (tsk_ctx *ctx, char *fmt, ...)
	TSK_HIDE;
void tsk_warn_ap (tsk_ctx *ctx, tsk_cmd *cmd, char *fmt, va_list ap)
	TSK_HIDE;
int tsk_warn (tsk_ctx *ctx, tsk_cmd *cmd, char *fmt, ...)
	TSK_HIDE;
int tsk_warn_no_cmd (tsk_ctx *ctx, char *fmt, ...)
	TSK_HIDE;
int tsk_note_flags_are_right_or_die (int cursor)
	TSK_HIDE;
struct err_info get_cmd_err_info (tsk_cmd *cmd, int cmd_words_idx,
		int statement_start_idx)
	TSK_HIDE;
void start_colour (tsk_ctx *ctx, enum tsk_colour colour)
	TSK_HIDE;
void stop_colour (tsk_ctx *ctx)
	TSK_HIDE;
