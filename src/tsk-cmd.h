
#pragma once
#include "tsk-item.h"
#include "tsk-print.h"

enum {

	// A big number since you might want to complete files. I think
	// Zsh has a max number of completions it will show anyway.
	TSK_MAX_COMPLETION_MATCHES = 256,
};

enum cmd_token_type {
	CT_TYPE_FILTER,
	CT_TYPE_ACTION,
	CT_TYPE_OPERATOR,
	CT_TYPE_MOD,
	CT_TYPE_SPECIAL,
};

enum cmd_token_kind {
	CTK_NOT_SET,
	CTK_OPEN_BRACKET,
	CTK_CLOSE_BRACKET,
	CTK_SEMICOLON,
	CTK_EQUALS,
	CTKA_ADD,
	CTKA_ANNOTATION,
	CTKA_ATTACHMENT,
	CTKA_BEFORE,
	CTKA_COPY,
	CTKA_COPY_CREATE,
	CTKA_COUNT,
	CTKA_CMD,
	CTK_FILE,
	CTK_FILE_CREATE,
	CTK_MATCH_FILE,
	CTK_MOD_FORCE,
	CTK_MOD_NORMAL,
	CTK_MOD_MSG,
	CTK_MOD_CONFIRM,
	CTKA_DONE,
	CTKA_EFFORT,
	CTKA_ENDING,
	CTKA_EXCEPT,
	CTKA_EXCEPT_INTERVAL,
	CTKA_INTERVAL,
	CTKA_LINK,
	CTKA_LOCK,
	CTKA_NAME,
	CTKA_SUPER_LOCK,
	CTKA_FILE,
	CTKA_FILE_CREATE,
	CTKA_ON,
	CTKA_OPEN,
	CTKA_POSTPONE,
	CTKA_PRIORITY,
	CTKA_REMOVE,
	CTK_REMOVE,
	CTK_FROM,
	CTKA_REMINDER,
	CTKA_SCOPY,
	CTKA_SCOPY_CREATE,
	CTKA_SFILE,
	CTKA_SFILE_CREATE,
	CTKA_STOP_REPEATING,
	CTKA_TAG,
	CTKA_PRINT_TAGS,
	CTKA_ITEM_TYPE,
	CTKA_UNSET,
	CTKA_UPDATE,
	CTK_AND,
	CTK_ANNOTATION,
	CTK_ATTACHMENT,
	CTK_BEFORE,
	CTKA_BODY,
	CTK_CHANGED,
	CTK_CREATION,
	CTK_DONE,
	CTK_EFFORT,
	CTK_ENDING,
	CTK_EXCEPT,
	CTK_EXCEPT_INTERVAL,
	CTKA_FMT,
	CTK_GREP,
	CTK_HAS,
	CTK_INTERVAL,
	CTK_LINK,
	CTK_PARENT,
	CTK_PREV_LIST,
	CTK_LOCKED,
	CTK_SUPER_LOCKED,
	CTKA_NAG,
	CTK_NAME,
	CTK_NEW,
	CTK_COPY,
	CTK_COPY_CREATE,
	CTK_NONE,
	CTKA_NOP,
	CTK_NOT,
	CTK_MOD_PRINT,
	CTK_MOD_EDIT,
	CTK_SFILE,
	CTK_SFILE_CREATE,
	CTK_MATCH_SFILE,
	CTK_LAST_REMINDED,
	CTKA_LAST_REMINDED,
	CTK_ON,
	CTK_OR,
	CTK_PROP,
	CTK_POSTPONE,
	CTK_PRIORITY,
	CTK_RELATIVE_RANGE,
	CTKA_REMIND,
	CTKA_LIST_SHORTCUTS,
	CTK_REMINDER,
	CTK_STOP_REPEATING,
	CTK_TAG,
	CTK_TYPE,
	CTKA_HELP,
	CTKA_WRITE,
	CTKA_WRITE_QUIT,
	CTKA_QUIT,
	CTKS_COMPLETE_ALL,

	CTK_IMPLICIT_LIST,
	CTK_END,
	CTK_ERROR,
	CTK_INT_LIST,
	CTK_NULL_BYTE,
	CTK_WORD,
	N_CTK,
};

typedef struct tsk_cmd_keyword tsk_cmd_keyword;
struct tsk_cmd_keyword {

	// We do the OO thing where we pretend that this is a keyword, so
	// this needs to stay at the start.
	tsk_keyword keyword;

	bool dont_offer_as_completion;
	enum cmd_token_type type;
};

typedef struct tsk_completion_match tsk_completion_match;
struct tsk_completion_match {
	char *str;

	// Might as well put this here. I only need
	// info->short_help_descr but this isn't hurting anyone.
	struct tsk_cmd_keyword *keyword;
};

typedef struct tsk_completions tsk_completions;
struct tsk_completions {
	tsk_completion_match matches[TSK_MAX_COMPLETION_MATCHES];
	bool is_arg;
	int count;
};

struct cmd_token {
	char *s, *e;
	int cmd_words_idx;

	// Whether(char []) {short_match, 0} it's the long version rather than single char. Long
	// ones need a = or spaces between it and its val, like getopt.
	//
	// Trickiness: if_long_expect_equals sets equals to is_long,
	// and cmd_get_value retains it. This is so match functions can
	// know if it was a long keyword. Actually, only match_body uses
	// this. There's a comment there.
	//
	// The proper way would be to hang on to previous tokens, I
	// suppose.
	bool is_long;

	//  Meaning it was something like ":c'This is this', or
	//  ":confirm_if_needed='This is this'", and not, eg, ":c 'This is
	//  this'" or ":confirm_if_needed 'This is this'".
	bool has_attached_arg;
	enum cmd_token_kind kind;

};

typedef struct tsk_cmd_keyword_matches tsk_cmd_keyword_matches;
struct tsk_cmd_keyword_matches {
	struct rab_completion_matches *matches;
	struct cmd_token t;
};

extern char *CMD_TOKEN_TYPE_STRS[];
extern struct tsk_cmd_keyword CMD_KEYWORDS[N_CTK];
extern struct rab_getter *TSK_CMD_KEYWORD_LONG_NAME_GETTER;

int tsk_update_ctx (tsk_ctx tsk_ctx[static 1], bool initialise,
		tsk_print_ctx *pc, tsk_options *opts);
void tsk_free_cmd (struct tsk_cmd *cmd);
int tsk_n_ons_within_time_period (struct tsk_item *note,
		struct ts_ts_range range)
	TSK_HIDE;
int cmp_list_elem (const void *_a, const void *_b, void *arg)
	TSK_HIDE;

struct tsk_cmd tsk_parse_cmd (tsk_ctx *ctx,
		char **cmd_words, size_t n_cmd_words, enum cmd_source cmd_source,
		struct tsk_cmd *prev_cmd);
int tsk_get_completions_from_str (tsk_ctx *ctx, char *cmd_str, struct tsk_cmd *prev_cmd,
		tsk_completions *out);
void nuke_tsk_completion_match (tsk_completion_match *match);
void tsk_nuke_completion (tsk_completions *tsk_completions);
