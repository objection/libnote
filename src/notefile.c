#define _GNU_SOURCE
#include "notefile.h"
#include "misc.h"
#include "useful.h"
#include "config.h"
#include "tsk-print.h"
#include "tsk-cmd.h"
#include "rab.h"
#include <unistd.h>
#include <cwalk.h>
#include <assert.h>

struct rab_getter NOTEFILE_GETTER = {
	.func = get_notefile_path_member,
	.type_size = sizeof (tsk_notefile),
};

char *tsk_get_notefile_in_dir (tsk_ctx *ctx, char *dir) {
	char absolute[PATH_MAX];
	char *r = 0;
	if (tsk_file_is_in_dir (TSK_NOTEFILE_NAME, dir)) {
		sprintf (absolute, "%s/%s", dir, TSK_NOTEFILE_NAME);
		r = u_malloc (PATH_MAX * sizeof *r);
		cwk_path_get_relative (ctx->opts->cwd, absolute, r, PATH_MAX);
	}
	return r;
}

char **get_notefile_path_member (const void *elem, void *) {
	return &((tsk_notefile *) elem)->path;
}

int get_comma_list_str_of_items_notefiles (tsk_ctx *ctx, int n_pr, char *pr,
		struct tsk_ints *notefile_idxes, bool insert_space_after_comma) {

	char buf[BUFSIZ / 2];
	FILE *f = fmemopen (buf, n_pr * sizeof *pr, "w");
	assert (f);
	char notefile_buf[BUFSIZ];
	arr_each (notefile_idxes, _) {

		// get_pretty_notefile_name doesn't allocate.
		char *notefile_str = get_pretty_notefile_name (&ctx->notefiles.d[*_]);

		get_display_str (ctx, sizeof notefile_buf, notefile_buf, &(dso) {
				.gqesm_mode = GQESO_SURROUND_WITH_QUOTES_IF_SPACES,
				.no_wrap = 1}, notefile_str);
		fputc (ctx->notefiles.d[*_].flags & TSK_FILE_FLAG_IS_SHORTCUT ? 's' : 'f', f);
		fputs (notefile_buf, f);
		if (_ - notefile_idxes->d != notefile_idxes->n - 1) {
			fputc (',', f);
			if (insert_space_after_comma)
				fputc (' ', f);
		}
	}

	fclose (f);
	pr[n_pr - 1] = 0;
	assert (get_truncated_str (ctx, pr, buf, n_pr, 1, 0));
	return 0;
}

tsk_notefile create_notefile_struct (tsk_ctx *ctx, char *path_or_basename,
		enum notefile_flags flags) {

	char *path = (char [PATH_MAX]) {};
	if (flags & TSK_FILE_FLAG_IS_SHORTCUT) {
		char *dot = strrchr (path_or_basename, '.');
		if (!dot || strcmp (dot, ".nxt")) {
			snprintf (path, PATH_MAX, "%s/%s.nxt", ctx->opts->data_dir,
					path_or_basename);
		} else
			snprintf (path, PATH_MAX, "%s/%.*s.nxt", ctx->opts->data_dir,
					(int) (dot - path_or_basename), path_or_basename);

	} else
		path = path_or_basename;

	return (tsk_notefile) {
		.path = strdup (path),
		.flags = flags,
	};
}

int free_notefile (tsk_notefile *notefile) {
	free (notefile->path);
	*notefile = (tsk_notefile) {};
	return 0;
}

int cmp_notefiles_by_path (const void *a, const void *b) {
	return strcmp (((tsk_notefile *) a)->path, ((tsk_notefile *) b)->path);
}

int cmp_notefiles_not_not_sfiles_by_path (const void *_a, const void *_b) {
	const tsk_notefile *a = _a;
	const tsk_notefile *b = _b;
	int diff = (a->flags & TSK_FILE_FLAG_IS_SHORTCUT) - (b->flags & TSK_FILE_FLAG_IS_SHORTCUT);
	if (diff)
		return diff;
	return strcmp (a->path, b->path);
}

