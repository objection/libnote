#pragma once
#include "tsk-types.h"
#include <stdio.h>

typedef struct tsk_keyword tsk_keyword;

// This is a pretty deluxe struct. It's got loads of members. Only the
// cmd_keywords use all these, but later you could well use them for
// everything, say for example the props. It's be useful for
// completion. You could display at least short short_help_descr as
// you complete the "has" CSV.
struct tsk_keyword {
	char *long_name;
	char short_name;
	char *long_help_descr;
	char *short_help_descr;
	char *args_descr;

	bool args_are_optional;

	// This should probably be an array but the thing is laying out
	// arrays statically in C is a pain. I'd have to make a
	// MAX_EXAMPLES constant, and then we're have to either manually
	// put in an n_examples or dynamically figure it out afterwards.
	// That's tricky itself, not really that much less tricky than
	// parsing this string.
	char *examples_str;
};

#define KEYWORDS \
	x(KEYWORD_NOTHING,         0, 0) \
	x(KEYWORD_ANNOTATION,      ".annotation", 11) \
	x(KEYWORD_ATTACHMENT,      ".attachment", 11) \
	x(KEYWORD_AUTOCMD,         ".autocmd", 8) \
	x(KEYWORD_BEFORE,          ".before", 7) \
	x(KEYWORD_BODY,            ".body", 5) \
	x(KEYWORD_COMMAND,         ".command", 8) \
	x(KEYWORD_CREATED,         ".created", 9) \
	x(KEYWORD_DONE,            ".done", 5) \
	x(KEYWORD_EFFORT,          ".effort", 7) \
	x(KEYWORD_ENDING,          ".ending", 7) \
	x(KEYWORD_EXCEPT,          ".except", 7) \
	x(KEYWORD_EXCEPT_INTERVAL, ".except-interval", 16) \
	x(KEYWORD_INTERVAL,        ".interval", 9) \
	x(KEYWORD_LINK,            ".link", 5) \
	x(KEYWORD_LOCKED,          ".locked", 7) \
	x(KEYWORD_SUPER_LOCKED,    ".super-locked", 13) \
	x(KEYWORD_NAME,            ".name", 5) \
	x(KEYWORD_NOTE,            ".note", 5) \
	x(KEYWORD_NOTEFILE_ID,     ".notefile-id", 12) \
	x(KEYWORD_ON,              ".on", 3) \
	x(KEYWORD_LAST_REMINDED,   ".last-reminded", 14) \
	x(KEYWORD_REMINDER,        ".reminder", 9) \
	x(KEYWORD_POSTPONE,        ".postpone", 14) \
	x(KEYWORD_PRIORITY,        ".priority", 9) \
	x(KEYWORD_STOP_REPEATING,  ".stop-repeating", 15) \
	x(KEYWORD_TAG,             ".tag", 4) \
	x(KEYWORD_WHERE_FROM,      ".from", 5) \
	x(KEYWORD_REMOVE,          ".remove", 7)

#define x(a, b, c) a,
enum keyword_id {KEYWORDS N_KEYWORD};
#undef x

extern int KEYWORD_NS[N_KEYWORD] TSK_HIDE;

extern enum prop_id PROP_DEPS[N_PROPS] TSK_HIDE;

extern tsk_keyword PROP_KEYWORDS[N_PROPS] TSK_HIDE;

enum time_id {
	TIME_ON,
	TIME_POSTPONE,
	TIME_BEFORE,
	TIME_ENDING,
	TIME_INTERVAL,
	TIME_STOP_REPEATING,
	TIME_EXCEPT,
	TIME_EXCEPT_INTERVAL,
	TIME_CREATION,
	TIME_NOW,
	TIME_REMINDER,
	TIME_LAST_REMINDED,
	N_TIMES,
};

#define ITS \
	x(IT_ANNOTATION, "annotation") \
	x(IT_ATTACHMENT, "attachment") \
	x(IT_AUTOCMD,    "autocmd") \
	x(IT_LINK, 		 "link") \
	x(IT_NOTE, 		 "note") \
	x(IT_TAG, 		 "tag") \
	x(IT_FROM, 		 "from")

#define x(a, b) a,
enum item_type : int {ITS N_IT};
#undef x

// This has to be four because of the 0. Apparently.
extern tsk_keyword ITEM_TYPE_KEYWORDS[N_IT] TSK_HIDE;

struct tsk_times {

	// time-str expect an array of ts_constants, but it's clearer
	// to work with members, hence we use the union trick..
	union {
		struct ts_constant d[N_TIMES];
		struct {
			struct ts_constant on;
			struct ts_constant postpone;
			struct ts_constant before;
			struct ts_constant ending;
			struct ts_constant interval;
			struct ts_constant stop_repeating;
			struct ts_constant except;
			struct ts_constant except_interval;
			struct ts_constant creation;
			struct ts_constant now;
			struct ts_constant reminder;
			struct ts_constant last_reminded;
		};
	};
};
extern struct tsk_times TIMES TSK_HIDE;

// Where possible, these are named after the actual cmd names,
// like A_SFILE is "Sfile". I'm not saying this is a good way of doing
// things, but it does let me make the names short and comprehensible
// -- to me. Certain actions, like A_ADD don't command names to
// match them. Truthfully I could add a \"add\", though I won't. But
// A_ADD_OR_CHANGE will never have a corrosponding keyword. Maybe
// that's a sign it shouldn't exist in this enum.
#define ACTION_IDS \
	x (A_NOT_SET,        "not set") \
	x (A_ADD,            "add item") \
	x (A_CHANGE,         "change item") \
	x (A_COPY,           "Copy") \
	x (A_COPY_CREATE,    "Copycreate") \
	x (A_COUNT,          "Count") \
	x (A_CMD,            "Cmd") \
	x (A_EDIT,           "Edit") \
	x (A_FILE,           "File") \
	x (A_FILE_CREATE,    "Filecreate") \
	x (A_FMT,            "Fmt") \
	x (A_HELP,           "Help") \
	x (A_LIST_SHORTCUTS, "List-shortcuts") \
	x (A_NAG,            "Nag") \
	x (A_NOP,            "Nop") \
	x (A_OPEN,           "Open") \
	x (A_PRINT,          "print") \
	x (A_PRINT_TAGS,     "Print-tags") \
	x (A_QUIT,           "Quit") \
	x (A_REMIND,         "Remind") \
	x (A_REMOVE,         "Remove") \
	x (A_SCOPY,          "Scopy") \
	x (A_SCOPY_CREATE,   "Scopycreate") \
	x (A_SFILE,          "Sfile") \
	x (A_SFILE_CREATE,   "Sfilecreate") \
	x (A_UNSET,          "Unset") \
	x (A_UPDATE,         "Update") \
	x (A_WRITE,          "Write") \
	x (A_WRITE_QUIT,     "Write-quit")

#define x(a, b) a,
enum action_id {ACTION_IDS N_ACTION_IDS};
#undef x

extern char *ACTION_ID_STRS[N_ACTION_IDS];

typedef struct view_node view_node;
struct view_node {
	int item_idx;
	struct tsk_view *next;
};

typedef struct tsk_view tsk_view;
struct tsk_view {
	view_node *d;
	size_t n, a;
	int cursor;
	bool props_were_specified;
};

// Actions are like CHANGE, but when you change you can change
// multiple properties. Hence this, used in action, is an "action
// piece", like a "piece of the action".
struct action_piece {
	char *str;
	enum prop_id prop;
	int cmd_words_idx;
};
$make_arr (struct action_piece, action_pieces);

struct mod_piece {
	char *str;
	int cmd_words_idx;
};
$make_arr (struct mod_piece, mod_pieces);
struct mod {
	struct mod_pieces pieces;
	union {
		enum prop_id prop_id;
		bool yes;
	} val;
};

struct peristent_mods {
	struct mod print_what;

	// This isn't a mod because it's dealt with in cmd. The struct
	// mods exists entirely so we can print good error messages in
	// action.c
	enum confirmation_mode confirmation_mode;
};
extern struct peristent_mods PERSISTENT_MODS;

struct action {
	struct action_pieces pieces;
	enum action_id id;

	struct peristent_mods mods;

	// This and edit_in_editor aren't in persistent_mods because I
	// think it makes no sense at all to always edit in editor.

	char *msg;
	// This hardly needs a struct mod. You could replace it with
	// bool edit_in_editor and int edit_in_editor_cmd_words_idx -- but
	// maybe this is prettier.
	struct mod edit_in_editor;
};

struct err_info {
	char line[BUFSIZ], header[BUFSIZ], line_prefix_str[16];

	// The statement idx, normally, which is the same as a line numer,
	// and the line number, in read-notefile.
	int linenr, start_col, bad_word_len;
};
$make_arr (struct err_info, err_infos);

enum statement_flags {
	SF_FAILED = 1 << 0,
	SF_AT_ACTION = 1 << 1,
	SF_ACTION_MODIFIES_NOTES = 1 << 2,
};

struct tsk_statement {
	struct tsk_view *view;
	struct action action;
	enum statement_flags flags;
	struct tsk_ints notefiles_referenced;
	int cmd_words_idx, cmd_words_end;
};

struct note {

	struct tsk_times times;
	int effort;
	int priority;

	// Needs to be a pointer, as far as I can see, since item hasn't
	// been declared yet.
	struct tsk_items *props;
};

$make_arr (struct tsk_statement, tsk_statements);

enum cmd_source {CMD_SOURCE_FRONTEND, CMD_SOURCE_AUTOCMD};

extern char *CMD_SOURCE_STRS[2] TSK_HIDE;

enum tsk_item_flags {

	// Meaning you did ",\"This is this\" :edit" and then quit without
	// saving. Used to properly restore items in maybe_restore_items.
	// Without this you'll get a note with the body "This is this"
	// when really since you'd cancelled you'd expect to see no note
	// at all. Needs to be here, or at least not a global flag, else
	// note ,"this" :confirm \; note :edit" wouldn't work. You could
	// make the flag per-statement but restore items doesn't consider
	// statements.
	TIF_USER_CANCELLED_EDIT_IN_EDITOR = 1 << 0,
};

// An item is a thing you read in from the notefile, a note or an
// autocmd. There's also a struct tsk_cmd. That's different. An
// cmd-type item is turned into a struct tsk_cmd.
typedef struct tsk_item tsk_item;
struct tsk_item {
	union {
		struct note;
		struct note note;
	};

	// Use by saved items. It's easier this way -- I hope.
	int item_idx;
	char *name;

	// The body of the note for notes. For autocmds, the body is eg
	// -o0d..1d "this". It's what gets saved to disc.
	char *body;

	// The notefiles the item belongs to. When notes are read in they
	// will have only one of these. They will only gain more if you do
	// Copy or Scopy. Copying notefiles is just a matter of changing
	// the notefile ID. write_to_notefiles writes items to the
	// notefiles that corrospond to these IDs.
	struct tsk_ints notefile_idxes;

	enum tsk_item_flags flags;
	struct tsk_items *froms;
	enum item_type type;
	enum prop_id has;
};

typedef struct tsk_items tsk_items;
struct tsk_items {
	tsk_item *d;
	size_t n, a;
};

enum tsk_cmd_flags {
	TCF_FAILED = 1 << 0,

	// It means you did :edit and wrote the file and didn't just quit.
	// It doesn't compare notes or anything, though now I think of it,
	// I could do that now that I have the maybe_restore_items thing.
	TCF_ITEMS_WERE_CHANGED_IN_EDITOR = 1 << 1,
	TCF_NEW_VIEW_DISCARDED_OLD_VIEW = 1 << 2,
};

typedef struct tsk_cmd tsk_cmd;
struct tsk_cmd {

	// "text" and "words" are the same. The second is just sundered.
	// I'll probably get rid of "text".
	char *text;
	struct tsk_strs words;

	// The words that comprise the view. In "snotes n-1 Remove" it
	// would be {"snotes", "n-1"}. This is really important for the
	// TUI at least. You need some way to display how you're
	// continually filtering the view. You don't need this to filter
	// the view; the view you pass in to parse_cmd that. But maybe
	// that will go away. With this, you don't need that, even if
	// that's a little more efficient.
	struct tsk_strs view_words;
	struct tsk_statements statements;
	enum cmd_source source;
	enum tsk_cmd_flags flags;
	struct err_infos err_infos;
	void *user;
};

// Probably not an awesome place for this.
typedef struct tsk_ctx tsk_ctx;
struct tsk_ctx {
	struct tsk_items items;
	struct tsk_options *opts;
	struct tsk_print_ctx *pc;
	struct tsk_strs sfiles;
	struct tsk_notefiles notefiles;
};

$ncmp_dec_by_member (tsk_item, type, enum item_type);
$ncmp_dec_by_member (tsk_item, index, int);
extern char *KEYWORD_STRS[64] TSK_HIDE;
extern struct rab_getter *TSK_KEYWORD_LONG_NAME_GETTER;

struct tsk_item *tsk_copy_notes (struct tsk_item *notes, int nnotes);
int tsk_cmp_item_start_date (const void *a, const void *b)
	TSK_HIDE;
int tsk_cmp_item (const void *a, const void *b)
	TSK_HIDE;
struct tsk_item tsk_item_dup (struct tsk_item *orig, bool dup_props);
void tsk_free_note (struct tsk_item *note);
void tsk_free_item (struct tsk_item *item);
void tsk_note_merge (struct tsk_item first, struct tsk_item *second);
char *tsk_get_item_type_str (enum item_type type)
	TSK_HIDE;
struct tsk_item *tsk_find_item (struct tsk_items *items, struct tsk_item *new)
	TSK_HIDE;
struct tsk_item tsk_make_item (enum item_type item_type, struct ts_time creation_date);
struct tsk_item tsk_make_item_with_body (char *str, enum item_type item_type,
		struct ts_time creation_date)
	TSK_HIDE;
int tsk_cmp_item_autocmds_first (const void *a, const void *b)
	TSK_HIDE;
void tsk_free_statement (struct tsk_statement *statement)
	TSK_HIDE;
enum prop_id item_type_to_prop (enum item_type type)
	TSK_HIDE;
int note_is_missing_this_prop_dep (struct tsk_item *item, enum prop_id checking)
	TSK_HIDE;
void *prop_id_to_member (struct tsk_item *item, enum prop_id prop_id)
	TSK_HIDE;
int cmp_prop (void *a, void *b, enum prop_id prop_id)
	TSK_HIDE;
int stringify_prop_val (tsk_ctx *ctx, size_t n_buf, char *buf, enum prop_id prop_id,
		void *val, enum prop_id item_has, bool quote_if_string)
	TSK_HIDE;
bool item_was_loaded_by_at_least_one_list (tsk_ctx *ctx, struct tsk_item *item)
	TSK_HIDE;
bool item_belongs_to_at_least_this_notefile (struct tsk_item *item, int notefile_idx)
	TSK_HIDE;
void add_necessary_hases (struct tsk_item *item)
	TSK_HIDE;
int cmp_item_by_type (const void *a, const void *b)
	TSK_HIDE;
char **keyword_full_name_getter_fn (const void *it, void *)
	TSK_HIDE;
void tsk_free_items (struct tsk_items *items)
	TSK_HIDE;
