#pragma once

#include "tsk-types.h"
#include "tsk-item.h"

enum write_flags {
	WF_WRITE_NOTEFILE_ID = 1 << 0,
	WF_WRITE_ABSOLUTE_NUMBER = 1 << 1,

	// For "note :edit", which would add a note, putting you right in
	// Vim.
	WF_ALLOW_EMPTY_BODY = 1 << 2,

	// Again for :edit: \"note n-1 R :edit\". It prints .remove
	WF_ALLOW_REMOVED = 1 << 3,
};

int tsk_write (tsk_ctx *ctx, struct tsk_items *items, struct tsk_cmd *_cmd);
int write_to_notefiles (tsk_ctx *ctx, tsk_cmd *cmd, struct tsk_notefiles *notefiles,
		struct tsk_items *items)
	TSK_HIDE;
int write_items (tsk_ctx *ctx, tsk_cmd *cmd, char *notefile, FILE *f, struct tsk_items *items,
		tsk_view *view, bool append, enum write_flags flags)
	TSK_HIDE;
void write_item (struct tsk_item *item, ssize_t selection_i,
		enum write_flags flags, struct tsk_items *items)
	TSK_HIDE;
