#pragma once

#include "tsk-change.h" // IWYU pragma: export
#include "tsk-print.h" // IWYU pragma: export
#include "tsk-cmd.h" // IWYU pragma: export
#include "tsk-err.h" // IWYU pragma: export
#include "tsk-item.h" // IWYU pragma: export
#include "tsk-action.h" // IWYU pragma: export
#include "tsk-write.h" // IWYU pragma: export

#define DEFAULT_CLI_ERROR_REPORTING \
	ERR_TYPE_ALL
#define DEFAULT_TUI_ERROR_REPORTING \
	ERR_TYPE_ALL
/* Default --autocmd-errors. See item.h for those. */
#define DEFAULT_AUTOCMD_ERROR_REPORTING \
	(ERR_TYPE_CMD_PARSING | ERR_TYPE_SYSTEM | \
		ERR_TYPE_RULE_TRANSGRESSION | ERR_TYPE_REAL_CHANGES)


