#define _GNU_SOURCE

#include "tsk-action.h"
#include "tsk-change.h"
#include "tsk-write.h"
#include "read-notefile.h"
#include "notefile.h"
#include "misc.h"
#include "n-fs.h"
#include "useful.h"
#include <sys/stat.h>

static int set_on (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor, struct action_piece action_piece,
		struct tsk_statement *statement) {

	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	time_t before_diff = 0;

	if (item->has & (1 << NP_BEFORE))

		// item: there was something here about setting before >
		// on but I didn't understand it.
		before_diff = item->times.before.time.ts - item->times.on.time.ts;

	time_t ending_diff = 0;

	if (item->has & (1 << NP_ENDING))
		ending_diff = item->times.ending.time.ts - item->times.on.time.ts;

	item->times.on.time = ts_maths_time_from_str (action_piece.str, 0, &TIMES.now.time,
			item->times.d, N_TIMES, 0);

	if (ts_errno) {
		action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement, "Bad time str: %s",
				ts_strerror (ts_errno));
		return 1;
	}

	if (item->has & (1 << NP_BEFORE)) {
		item->times.before.time = ts_time_from_ts (item->times.on.time.ts + before_diff);

		if (item->has & (1 << NP_ENDING))
			item->times.ending.time = ts_time_from_ts (item->times.on.time.ts
						+ ending_diff);

		if (item->times.on.time.ts < TIMES.now.time.ts) {

			// We once issued a confirmation prompt here under certain
			// conditions. But confirming down here is a pain,
			// seriously a pain. I could do it. I could. But I'm
			// asking myself if it's worth it. It's not.
			if (item->has & (1 << IP_NEW)) {
#if 0
				if (!turnstile (cursor,
							pc->confirm, "Set the item to before now [y/n]? "))
					return 1;
#endif
			} else {

#if 0
				if (!turnstile (cursor, pc->confirm,
							"Set %ld%s item to before now [y/n]? ",
							cursor, rab_get_num_suffix (cursor)))
					return 1;
#endif
			}
		}

		item->times.on.name = TIMES.on.name;
	}
	return 0;
}

static int change_ts_time (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor, struct ts_time *time,
		enum time_id time_id, struct action_piece action_piece,
		struct tsk_statement *statement) {

	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	*time = ts_maths_time_from_str (action_piece.str, 0, &TIMES.now.time, item->times.d,
			N_TIMES, 0);

	if (ts_errno) {
		action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement,
				"Bad time str: %s", ts_strerror (ts_errno));
		return 1;
	}

	item->times.d[time_id].name = TIMES.d[time_id].name;
	return 0;
}

static int change_ts_time_range (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct ts_time_range *range, enum time_id time_id,
		struct action_piece action_piece, struct tsk_statement *statement) {

	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	*range = ts_maths_time_range_from_str (action_piece.str, 0,
			&TIMES.now.time, item->times.d, N_TIMES, 0);

	if (ts_errno) {
		action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement,
				"Bad time range: %s", ts_strerror (ts_errno));
		return 1;
	}

	item->times.d[time_id].name = TIMES.d[time_id].name;
	return 0;
}

static int change_ts_dur (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor, struct ts_dur *dur,
		enum time_id time_id, struct action_piece action_piece,
		struct tsk_statement *statement) {

	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	*dur = ts_dur_from_dur_str (action_piece.str, 0);
	if (ts_errno) {
		action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement, "Bad time dur: %s", ts_strerror (ts_errno));
		return 1;
	}
	item->times.d[time_id].name = TIMES.d[time_id].name;
	return 0;
}

// Adds a tag, annotation, etc if it's not already there.
// There once was and should be again a way to change a prop.
static int add_prop (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor, struct action_piece action_piece,
		enum item_type type, struct tsk_statement *statement) {

	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	if (item->type != IT_NOTE) {
		action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement,
				"\
You can't add a prop to a %s", ITEM_TYPE_KEYWORDS[item->type].long_name);
		return 1;
	}
	int r = 0;

	struct tsk_item new = {.body = action_piece.str, .type = type};
	if (!item->props)
		item->props = u_calloc (sizeof *item->props);
	if (!tsk_find_item (item->props, &new)) {
		new.body = strdup (action_piece.str);
		if (arr_add (item->props, new) == -1)
			assert (!"Couldn't make new item");
	}
	return r;
}

static int change_number (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor, size_t offset, char *name, int new_value,
		struct action_piece action_piece, struct tsk_statement *statement) {
	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	char *p_offset = (char *) item + offset;
	int *the_int = (int *) p_offset;
	*the_int = new_value;

	//  This means priority has a range of 11.
	if (*the_int > 10 || *the_int < 1)
		return action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement,
				"%s should be between 1 and 10 inclusive", name);
	return 0;
}

// Leave this in for now. It's how I'll do generic dependency shit.
// Right now there's no need to.
#if 0
#define $make_topsort_elem(T, name) \
	struct name##_top_sort_elem { \
		T t; \
		int deps[4], n_deps, idx, depth; \
	};

$make_topsort_elem (enum time_id, time_id);

/* recursively resolve compile order; negative means loop */
static int get_depth (struct time_id_top_sort_elem *list, int idx, int bad)
{
	int max, i, t;

	if (!list[idx].n_deps)
		return list[idx].depth = 1;

	if ((t = list[idx].depth) < 0) return t;

	list[idx].depth = bad;
	for (max = i = 0; i < list[idx].n_deps; i++) {
		if ((t = get_depth (list, list[idx].deps[i], bad)) < 0) {
			max = t;
			break;
		}
		if (max < t + 1) max = t + 1;
	}
	return list[idx].depth = max;
}

static int cmp_elem (const void *a, const void *b) {
	return (*(struct time_id_top_sort_elem *) a).depth -
		(*(struct time_id_top_sort_elem *) b).depth;
}
#endif

static int parse_item_type (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece, struct tsk_statement *statement) {

#define $is(_type, _len) \
	({ \
	 	assert (!ITEM_TYPE_KEYWORDS[_type].short_name); \
		$strnmatch (action_piece.str, ITEM_TYPE_KEYWORDS[_type].long_name, _len); \
	})

	if ($is (IT_AUTOCMD, 7))
		return IT_AUTOCMD;
	else if ($is (IT_NOTE, 4))
		return IT_NOTE;

	action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement,  "Not a type");
	return -1;
#undef $is
}

static int set_item_type (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece, struct tsk_statement *statement) {
	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	enum item_type item_type = parse_item_type (ctx, cmd, view_cursor, action_piece,
			statement);
	if (item_type == -1)
		return 1;
	item->type = item_type;
	return 0;
}

static int set_body (tsk_ctx *ctx, tsk_view *view, int view_cursor,
		char *action_str) {
	auto item = ctx->items.d + view->d[view_cursor].item_idx;
	item->body = 0;
	item->body = strdup (action_str);
	return 0;
}

static int set_name (tsk_ctx *ctx, int view_cursor, char *action_str,
		struct tsk_statement *statement) {
	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	item->name = 0;
	item->name = strdup (action_str);

	// I took this out. Obviously you do want to make sure names are
	// unique to the notefile the note is in, but holy shit, I can't
	// figure out how to do it.
#if 0
	if (!name_is_unique_to_notefile (cursor, items)) {
		action_err (view_cursor,
				"Notefile \"%s\" has a note with that name",
				.d[item->notefile_idx]);
		return 1;
	}
#endif
	return 0;
}

static int set_before (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor, struct action_piece action_piece,
		struct tsk_statement *statement) {
	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	auto times = &item->times;
	int rc = change_ts_time (ctx, cmd, view_cursor, &times->before.time,
			TIME_BEFORE, action_piece, statement);
	if (rc)
		return rc;

	if (item->has & (1 << NP_INTERVAL) &&
			times->before.time.ts > times->on.time.ts +
			ts_secs_from_dur (times->interval.dur, &times->on.time)) {

		action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement,
				"You're trying to set before to more than on + interval");
		return 1;
	}
	return 0;
}

static int set_postpone (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece, struct tsk_statement *statement) {

	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	auto times = &item->times;
	int rc = change_ts_time (ctx, cmd, view_cursor, &times->postpone.time, TIME_POSTPONE,
			action_piece, statement);
	if (rc)
		return rc;

	if (item->has & (1 << NP_INTERVAL) &&
			times->postpone.time.ts > times->on.time.ts +
			ts_secs_from_dur (times->interval.dur, &times->on.time)) {

		action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement,
				"Can't postpone a note later than when it's due to repeat");
		return 1;
	}
	return 0;
}

static int set_interval (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece, struct tsk_statement *statement) {

	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	auto times = &item->times;
	int rc = change_ts_dur (ctx, cmd, view_cursor, &times->interval.dur, TIME_ON,
			action_piece, statement);
	if (rc)
		return rc;
	if (times->interval.dur.secs < 0) {

		// only need to check one member, cause if one's
		// negative, both are
		action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement,
				"Can't have a negative interval");
		return 1;
	}
	return 0;
}

static int set_ending (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor, struct action_piece action_piece,
		struct tsk_statement *statement) {
	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	auto times = &item->times;
	auto ending = &times->ending.time;
	auto on = &times->on.time;
	int rc = change_ts_time (ctx, cmd, view_cursor, &times->ending.time, TIME_ENDING,
			action_piece, statement);
	if (rc)
		return rc;

	if (ending->ts < on->ts) {
		action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement,
				"You're trying to set note \"%35s\"'s ending before on",
				item->body);
		return 1;
	}
	if (item->has & (1 << NP_INTERVAL) &&
			ending->ts >= on->ts + ts_secs_from_dur (times->interval.dur, on)) {
		action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement,"\
You're trying to set note \"%35s\"'s ending to after its on plus its interval",
				item->body);
		return 1;
	}
	return 0;
}

static int set_stop_repeating (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece, struct tsk_statement *statement) {

	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	auto times = &item->times;
	int rc = change_ts_time (ctx, cmd, view_cursor, &times->stop_repeating.time,
			TIME_STOP_REPEATING, action_piece, statement);
	if (rc)
		return rc;

	if (times->stop_repeating.time.ts < times->on.time.ts +
			ts_secs_from_dur (times->interval.dur, &times->on.time)) {

		action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement,
				"Stop-repeating must be greater and on plus the interval");
		return 1;
	}
	return 0;
}

static int set_except (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor, struct action_piece action_piece,
		struct tsk_statement *statement) {

	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	auto times = &item->times;
	int rc = change_ts_time_range (ctx, cmd, view_cursor, &times->except.time_range,
			TIME_EXCEPT, action_piece, statement);
	if (rc)
		return rc;

	if (times->except.time_range.s.ts < times->on.time.ts) {

		action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement,
				"Except must be greater than on");
		return 1;
	}
	return 0;
}

static int set_reminder (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece,
		struct tsk_statement *statement) {

	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	int rc = change_ts_dur (ctx, cmd, view_cursor, &item->times.reminder.dur,
			TIME_REMINDER, action_piece, statement);
	if (rc)
		return rc;
	return 0;
}

static int set_except_interval (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece,
		struct tsk_statement *statement) {

	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	int rc = change_ts_dur (ctx, cmd, view_cursor, &item->times.except_interval.dur,
			TIME_EXCEPT, action_piece, statement);
	if (rc)
		return rc;
	return 0;
}

static int set_effort (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor, struct action_piece action_piece,
		struct tsk_statement *statement) {
	int num;
	if ($get_int (&num, action_piece.str, 0))
		return 1;
	if (change_number (ctx, cmd, view_cursor, offsetof (struct tsk_item, effort),
			PROP_KEYWORDS[NP_PRIORITY].long_name, num, action_piece, statement))
		return 1;
	return 0;
}

static int set_priority (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece, struct tsk_statement *statement) {
	int num;
	if ($get_int (&num, action_piece.str, 0))
		return 1;
	if (change_number (ctx, cmd, view_cursor, offsetof (struct tsk_item, priority),
				PROP_KEYWORDS[NP_PRIORITY].long_name, num, action_piece, statement))
		return 1;
	return 0;
}

int action_change_prop (tsk_ctx *ctx, tsk_view *view, int view_cursor,
		struct action_piece action_piece) {
	int r = 1;
	switch (action_piece.prop) {
		case IP_BODY: r = set_body (ctx, view, view_cursor, action_piece.str); break;
		default: assert (!"With prop arrays, you can only change the body");
	}
	return r;
}

// Don't check if eg., postpone has an on in the functions that this
// calls. It's all checked at the bottom of this function with
// note_flags_are_right_or_die.
//
// Returns n_changes, so I know whether to announce the autocmd. Don't
// announce the autocmd if it's not changed anything.
int action_change (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *view, int view_cursor,
		struct action_piece action_piece, struct tsk_statement *statement) {

	auto item = ctx->items.d + view->d[view_cursor].item_idx;

	if (action_piece.prop == (unsigned) -1)
		return 0;

	int r;

	switch (action_piece.prop) {
		case IP_TYPE:            r = set_item_type (ctx, cmd, view_cursor, action_piece, statement); break;
		case IP_BODY:            r = set_body (ctx, view, view_cursor, action_piece.str); break;
		case IP_NAME:            r = set_name (ctx, view_cursor, action_piece.str, statement); break;
		case IP_REMOVE:          r = 0; break;
		case IP_LOCK:            r = 0; break;
		case IP_SUPER_LOCK:      r = 0; break;
		case NP_DONE:            r = 0; break;
		case NP_ON:              r = set_on (ctx, cmd, view_cursor, action_piece, statement); break;
		case NP_BEFORE:          r = set_before (ctx, cmd, view_cursor, action_piece, statement); break;
		case NP_POSTPONE:        r = set_postpone (ctx, cmd, view_cursor, action_piece, statement); break;
		case NP_INTERVAL:        r = set_interval (ctx, cmd, view_cursor, action_piece, statement); break;
		case NP_ENDING:          r = set_ending (ctx, cmd, view_cursor, action_piece, statement); break;
		case NP_STOP_REPEATING:  r = set_stop_repeating (ctx, cmd, view_cursor, action_piece, statement); break;
		case NP_EXCEPT:          r = set_except (ctx, cmd, view_cursor, action_piece, statement); break;
		case NP_REMINDER:        r = set_reminder (ctx, cmd, view_cursor, action_piece, statement); break;
		case NP_EXCEPT_INTERVAL: r = set_except_interval (ctx, cmd, view_cursor, action_piece, statement); break;
		case NP_ANNOTATION:      r = add_prop (ctx, cmd, view_cursor, action_piece, IT_ANNOTATION, statement); break;
		case NP_TAG:             r = add_prop (ctx, cmd, view_cursor, action_piece, IT_TAG, statement); break;
		case NP_LINK:            r = add_prop (ctx, cmd, view_cursor, action_piece, IT_LINK, statement); break;
		case NP_ATTACHMENT:      r = add_prop (ctx, cmd, view_cursor, action_piece, IT_ATTACHMENT, statement); break;
		case NP_EFFORT:          r = set_effort (ctx, cmd, view_cursor, action_piece, statement); break;
		case NP_PRIORITY:        r = set_priority (ctx, cmd, view_cursor, action_piece, statement); break;
		default:                 assert (!"Should be allowed here?");
	}

	if (!r) {
		item->has |= (1 << action_piece.prop);
		item->has |= (1 << IP_CHANGED);
	}

	// Don't let the user make something locked and super-locked.
	if (item->has & (1 << IP_LOCK) && item->has & (1 << IP_SUPER_LOCK))
		item->has &= ~(1 << IP_LOCK);

	return r;
}

// mtim didn't seem to work. ctim seems to.
static struct timespec get_last_ctime (char *path) {
	struct stat stat_buf;
	stat (path, &stat_buf);
	return stat_buf.st_ctim;
}

// Add all new items to the items array, sticking them in in order. If
// the new items array has more items, push them to the items array.
// If the new items array has less items, remove the remainder.
static int merge_edited_items (tsk_ctx *ctx, tsk_view *view,
		struct tsk_items *items_from_editor) {

	int i = 0;

	while (i < view->n) {

		// If there's fewer items in items_from_editor, remove the
		// remainder.
		if (i >= items_from_editor->n)
			ctx->items.d[view->d[i].item_idx].has |= (1 << IP_REMOVE);

		// Else add them in where they should go.
		else {
			tsk_free_item (&ctx->items.d[view->d[i].item_idx]);
			ctx->items.d[view->d[i].item_idx] = items_from_editor->d[i];
		}
		i++;
	}

	// Add any added notes on to the end.
	while (i < items_from_editor->n)
		arr_add (&ctx->items, items_from_editor->d[i++]);
	return 0;
}

// Writes the selection to a file, opens it in EDITOR.
//
// Adds IP_REMOVE to the notes that were written and pushes the new
// ones on to the end.
//
// I don't try to insert the changed notes to their old positions
// because I want the user to be able to add and remove notes.
//
// The order of the notefile should be maintained because the notes
// are always sorted by creation time at the end of the program.
static int get_items_from_editor (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *view,
		struct tsk_statement *statement, struct mod_piece edit_in_editor_mod_piece,
		struct tsk_items *out) {

	// Use mkstemp because I need the str name of the file so
	// I can pass it to system.
	char tmp_name[] = "/tmp/XXXXXX.nxt";
	int tmp_fd = mkstemps (tmp_name, 4);
	if (tmp_fd == -1)
		return action_err (ctx, cmd, AE_NO_ITEM_TO_PRINT,
				edit_in_editor_mod_piece.cmd_words_idx, 0, statement,
				"Temporary file didn't get created");

	*out = (typeof (*out)) {};

	enum write_flags flags = WF_WRITE_NOTEFILE_ID | WF_WRITE_ABSOLUTE_NUMBER
		| WF_ALLOW_REMOVED;
	if (statement->action.id == A_ADD)
		flags |= WF_ALLOW_EMPTY_BODY;
	if (write_items (ctx, cmd, tmp_name, 0, &ctx->items, view, 0, flags))
		return 1;

	char *editor = tsk_get_editor (ctx, cmd);
	if (!editor)
		return 1;

	// This is for checking if the user has actually written the file
	// they're editing. It's only important if you "note db'hohohoho'
	// edit". If you exit your editor without saving you want to
	// abandon the whole thing. If you write, you want to go through
	// with it, even if you didn't add anything to the file.
	struct timespec mod_times[2];
	mod_times[0] = get_last_ctime (tmp_name);

	// Edit the notes.
	n_fs_systemf ("%s %s", editor, tmp_name);

	mod_times[1] = get_last_ctime (tmp_name);

	// If it's the same, you haven't edited. Since this is change time
	// (right?), this will exit even if you save it. That's fine.
	if (mod_times[1].tv_nsec + mod_times[1].tv_sec ==
			mod_times[0].tv_nsec + mod_times[0].tv_sec) {

		// We need something like this for maybe_restore_items.
		arr_each (view, _)
			ctx->items.d[_->item_idx].flags |= TIF_USER_CANCELLED_EDIT_IN_EDITOR;
		return 1;
	}

	struct rn_err_info rn_err_info;

	// Get the edited notes.
	if (read_notefile (ctx, (tsk_notefile []) {create_notefile_struct (ctx, tmp_name, 0)},
				RF_EXPECT_NOTEFILE_ID | RF_RETAIN_NUMBER | RF_DONT_ADD_NOTEFILE,
				&rn_err_info, out) == -1)
		return action_err (ctx, cmd, AE_NO_ITEM_TO_PRINT,
				edit_in_editor_mod_piece.cmd_words_idx,
				&rn_err_info.err_info, statement, "%s", rn_err_info.msg);
	return 0;
}

#if 0
static int get_user_to_assign_notefile (struct tsk_items *items) {

	int r = 0;
	pc->msg (pc, "Which notefile or range of notefiles to write to?\n");

	for (unsigned i = 1; i < .n; i++) {

		// Note get_pretty_notefile_name doesn't allocate.
		char *notefile_str = get_pretty_notefile_name (&.d[i]);

		pc->msg (pc, "    %d %s\n", i - 1, notefile_str);
	}

	char *input = pc->input (pc, "");
	struct s2ia_info info;
	struct tsk_ints selection;
	info = s2ia_str_to_int_arr (&selection.d, &selection.n, input, 0,
			.n, 0);
	if (info.status) {
		r = action_err (AE_NO_ITEM_TO_PRINT, AE_ERR_IS_WHOLE_LINE, 0,
				"Couldn't parse: %s", statement, s2ia_error_strs[info.status]);
		goto out;
	}

	$fori (i, selection.n)
		selection.d[i] += 1;

	assert (selection.n);

	$fori (i, selection.n)
		arr_add (&items->d[items->n - 1].notefile_idxes, *selection.d);

out:
	free (input);
	free (selection.d);
	return r;
}
#endif

static int check_and_set_notefile_ids (tsk_ctx *ctx, tsk_cmd *cmd, struct tsk_items *items_from_editor,
		struct mod_piece edit_in_editor_mod_piece, struct tsk_statement *statement) {

	char truncated_body[BUFSIZ];
	arr_each (items_from_editor, _) {

		if (!_->notefile_idxes.n) {
			get_display_str (ctx, sizeof truncated_body, truncated_body,
					&(dso) {.no_wrap = 1}, _->body);

			// Here, you've done Edit and removed a notefile ID. I had
			// it automatically writing to the last notefile in that
			// case, but then there's weird corner cases. It's a dumb
			// feature. Just disallow you removing the notefile id. In
			// future we could run the user through
			// get_user_to_assign_notefile, potentially for the second
			// time. And of course we could remember their choice if
			// they've gone through that before and just apply it.
			// But. That's just too complicated. No one would remember
			// that.
			action_err (ctx, cmd, AE_NO_ITEM_TO_PRINT,
					edit_in_editor_mod_piece.cmd_words_idx, 0, statement, "\
You've ended up with no notefile id for item \"%s\"",
					truncated_body, arr_last (&ctx->notefiles)->path);
			return 1;
		}
	}
	return 0;
}

int tsk_change_in_editor (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *view,
		struct tsk_statement *statement, struct mod_piece edit_in_editor_mod_piece) {

	struct tsk_items items_from_editor;
	if (get_items_from_editor (ctx, cmd, view, statement, edit_in_editor_mod_piece,
				&items_from_editor))
		return 1;
	if (check_and_set_notefile_ids (ctx, cmd, &items_from_editor, edit_in_editor_mod_piece,
				statement))
		return 1;
	merge_edited_items (ctx, view, &items_from_editor);

	free (items_from_editor.d);
	return 0;
}

int init_added_item (tsk_ctx *ctx, tsk_cmd *cmd, struct tsk_item *item, struct action action,
		struct tsk_statement *statement) {

	// Need to set the item on the spot, I think, because else the
	// following would give shit errors:
	//
	// 		note On=1d ,"n-1" Yautocmd
	// There would be no error for the On. You *could* add a sort of
	// anti dependency check. You also could reorder action pieces to
	// they appear in a certain order. Or you could just do this.
	enum item_type item_type = -1;
	arr_each (&action.pieces, _) {
		if (_->prop == (1 << IP_TYPE)) {
			item_type = parse_item_type (ctx, cmd, AE_NO_ITEM_TO_PRINT, *_, statement);
			if (item_type == -1)
				return 1;
		}
	}
	if (item_type == -1)
		item_type = IT_NOTE;

	int notefile_idx = item->notefile_idxes.d[0];
	auto new_item = tsk_make_item (item_type, TIMES.now.time);
	arr_add (&new_item.notefile_idxes, notefile_idx);
	add_necessary_hases (&new_item);
	tsk_free_item (item);
	*item = new_item;

	return 0;
}

// Half of the creation of an item is done in cmd.c. There's a reason
// for it but I don't know what it is. 2023-06-29T18:15:11+01:00: now
// we can create multiple items.
int init_added_items (tsk_ctx *ctx, tsk_cmd *cmd, struct action action, struct tsk_items *items,
		struct tsk_statement *statement) {

	arr_each (statement->view, _)
		init_added_item (ctx, cmd, items->d + _->item_idx, action, statement);
	return 0;
}
