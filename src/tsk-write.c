#include "tsk-write.h"
#include "misc.h"
#include "config.h"
#include "lib/n-fs/n-fs.h"
#include "tsk-err.h"
#include "view.h"
#include "useful.h"
#include <sys/file.h>
#include <assert.h>

// I think this sort of thing is good. write.c only deals with one
// file at a time. So rather than cluttering up the code with needless
// arguments, just make it file-local.
thread_local static FILE *TMP_F;

static void write_props (struct tsk_items *items,
		enum prop_id parent_note_has) {
	if (!items)
		return;
	for (struct tsk_item *prop = items->d;
			prop - items->d < items->n; prop++) {

		if (prop->has & (1 << IP_REMOVE))
			continue;
		enum prop_id note_prop = item_type_to_prop (prop->type);

		if (parent_note_has & (1 << note_prop))
			fprintf (TMP_F, "\t.%s = %s\n", ITEM_TYPE_KEYWORDS[prop->type].long_name,
					prop->body);
	}
}

int write_to_notefiles (tsk_ctx *ctx, tsk_cmd *cmd, struct tsk_notefiles *notefiles,
		struct tsk_items *items) {

	int r = 0;
	tsk_view view = {};

	// When you copy an item you want it to end up at the end of the
	// notefile it's being copied. So we'll make a list of all items
	// that have been copied and append them to the view. Without
	// this, copied items usually end up at the beginnings of the
	// notefiles they're being copied to. Eg: In "note f/tmp/this.nxt
	// n-1 F/tmp/that.nxt", /tmp/that.nxt would be loaded after
	// /tmp/this.nxt, and so "n-1" would end up first in
	// /tmp/that.nxt.
	tsk_view copied_items_view = {};

	for (size_t notefile_idx = 0; notefile_idx < notefiles->n; notefile_idx++) {

		copied_items_view.n = view.n = 0;

		arr_each (items, _) {

			if (item_belongs_to_at_least_this_notefile (_, notefile_idx) &&
					!(_->has & (1 << IP_REMOVE))) {

				// Don't append if we're dealing with the notefile
				// the copied item originally belonged to.
				if ((_->has & (1 << IP_COPY)) && (

							/* We did a move */
							_->notefile_idxes.n == 1 ||

							// We did a copy but aren't talking about
							// the notefile idx the note originally
							// had.
						_->notefile_idxes.d[0] != notefile_idx))

					arr_add (&copied_items_view,
							(view_node) {.item_idx = _ - items->d});
				else
					arr_add (&view, (view_node) {.item_idx = _ - items->d});
			}
		}

		arr_pusharr (&view, copied_items_view.d, copied_items_view.n);

		if (write_items (ctx, cmd, notefiles->d[notefile_idx].path, 0, items, &view, 0, 0))
			r = 1;
	}
	view_free (&view);
	return r;
}

// It looks like this function returns, basically, a temporary buffer.
static char *get_str_with_keywords_escaped (char *str, char **keywords,
		size_t n_keywords) {

	char *r = 0;
	FILE *f = fmemopen (r, sizeof r, "w");
	assert (f);

	for (char *_ = str; *_; _++) {
		if (*_ == '.') {
			bool found = 0;
			u_each (keyword, n_keywords, keywords) {
				if (!strncmp (*keyword, _, strlen (*keyword))) {
					found = 1;
					break;
				}
			}
			if (found) {
				fputc ('\\', f);
				fputc ('.', f);
			} else {
				fputc (*_, f);
			}

		} else {
			fputc (*_, f);
		}
	}
	fclose (f);
	return r;
}

static inline int fprint_time (struct ts_time *ts_time, enum keyword_id keyword_id) {

	char scratch[TSK_N_SCRATCH];
	ts_str_from_time (scratch, TSK_N_SCRATCH, ts_time, TSK_ISO_DATE);
	assert (0 == ts_errno);
	fprintf (TMP_F, "\t%s = %s\n", KEYWORD_STRS[keyword_id], scratch);
	return 0;
}

static inline int fprint_time_range (struct ts_time_range *range,
		enum keyword_id keyword_id) {

	char scratch[TSK_N_SCRATCH];
	ts_str_from_time (scratch, TSK_N_SCRATCH, &range->s, TSK_ISO_DATE);

	assert (!ts_errno);
	fprintf (TMP_F, "\t%s = %s", KEYWORD_STRS[keyword_id], scratch);

	ts_str_from_time (scratch, TSK_N_SCRATCH, &range->e, TSK_ISO_DATE);

	assert (!ts_errno);
	fprintf (TMP_F, "..%s\n", scratch);
	return 0;
}

static int fprint_dur (struct ts_dur *dur, enum keyword_id keyword_id) {

	char scratch[TSK_N_SCRATCH];
	ts_str_from_dur (scratch, TSK_N_SCRATCH, dur, 0, 0);
	assert (0 == ts_errno);
	fprintf (TMP_F, "\t%s = %s\n", KEYWORD_STRS[keyword_id], scratch);
	return 0;
}

// fprintf ordinary strs, like the body or the name.
static void fprint_str (char *scratch, char *name, char *str) {
	if (name)
		fprintf (TMP_F, "\t%s = ", name);

	// if it's a body, there's no name to be printed
	else
		fprintf (TMP_F, "\t");

	if (str && strchr (str, '.') &&
			tsk_contains_unescaped_strs (str, KEYWORD_STRS + 1, N_KEYWORD - 1)) {

		char *tmp = get_str_with_keywords_escaped (str, KEYWORD_STRS + 1, N_KEYWORD - 1);
		free (str);
		str = tmp;

		fprintf (TMP_F, "%s\n", scratch);
	} else
		fprintf (TMP_F, "%s\n", str ? str : "");
}

static int write_time_prop (struct tsk_item *item, enum prop_id prop_id,
		enum time_id id, enum keyword_id keyword_id) {
	if (item->has & (1 << prop_id)) {
		switch (item->times.d[id].type) {
			case TS_CONSTANT_TYPE_DURATION:
				fprint_dur (&item->times.d[id].dur, keyword_id);
				break;
			case TS_CONSTANT_TYPE_TIME:
				fprint_time (&item->times.d[id].time, keyword_id);
				break;
			case TS_CONSTANT_TYPE_TIME_RANGE:
				fprint_time_range (&item->times.d[id].time_range, keyword_id);
				break;
		}
	}
	return 0;
}

void write_item (struct tsk_item *item, ssize_t selection_i,
		enum write_flags flags, struct tsk_items *items) {

	char scratch[TSK_N_SCRATCH];
	fprintf (TMP_F, ".%s %zu =\n", ITEM_TYPE_KEYWORDS[item->type].long_name,
			flags & WF_WRITE_ABSOLUTE_NUMBER? item - items->d: selection_i);

	// Leave these in. FIXME: check the bytes aren't complete
	// garbage. Only problem with doing that is Unicode. Still, you
	// could check to see that the bytes aren't non-printable
	// characters.
	if (!(item->has & IP_REMOVE) && !(flags & WF_ALLOW_EMPTY_BODY) &&
			(!item->body || !*item->body)) {
		assert (!"\
You've allowed a note with an empty body to be passed to write_item. This \
is a proper bug");
	}
	if (item->body && *item->body)
		fprint_str (scratch, 0, item->body);

	if (item->has & (1 << IP_NAME))
		fprint_str (scratch, KEYWORD_STRS[KEYWORD_NAME], item->name);

	if (item->type == IT_NOTE) {

		fprint_time (&item->times.creation.time, KEYWORD_CREATED);
		write_time_prop (item, NP_ON, TIME_ON, KEYWORD_ON);
		write_time_prop (item, NP_REMINDER, TIME_REMINDER, KEYWORD_REMINDER);
		write_time_prop (item, NP_LAST_REMINDED, TIME_LAST_REMINDED,
						  KEYWORD_LAST_REMINDED);
		/* if (item->has & NP_LAST_REMINDED)    fprintf (tmp_f, "\t%s\n", */
		/* 		                               keyword_strs[KEYWORD_REMINDED]); */
		write_time_prop (item, NP_POSTPONE, TIME_POSTPONE, KEYWORD_INTERVAL);
		write_time_prop (item, NP_INTERVAL, TIME_INTERVAL, KEYWORD_INTERVAL);
		write_time_prop (item, NP_STOP_REPEATING, TIME_STOP_REPEATING,
														KEYWORD_STOP_REPEATING);
		write_time_prop (item, NP_EXCEPT, TIME_EXCEPT, KEYWORD_EXCEPT);
		write_time_prop (item, NP_EXCEPT_INTERVAL, TIME_EXCEPT_INTERVAL,
														KEYWORD_EXCEPT_INTERVAL);
		write_time_prop (item, NP_BEFORE, TIME_BEFORE, KEYWORD_BEFORE);
		write_time_prop (item, NP_ENDING, TIME_ENDING, KEYWORD_ENDING);

		if (item->has & (1 << NP_DONE))     fprintf (TMP_F, "\t%s\n", KEYWORD_STRS[KEYWORD_DONE]);
		if (item->has & (1 << NP_EFFORT))   fprintf (TMP_F, "\t%s = %d\n",
				                               KEYWORD_STRS[KEYWORD_EFFORT], item->effort);
		if (item->has & (1 << NP_PRIORITY))  fprintf (TMP_F, "\t%s = %d\n",
                                               KEYWORD_STRS[KEYWORD_PRIORITY],
                                               item->priority);

		write_props (item->props, item->has);
	}

	if (item->has & (1 << IP_LOCK))
		fprintf (TMP_F, "\n\t%s\n", KEYWORD_STRS[KEYWORD_LOCKED]);
	if (item->has & (1 << IP_SUPER_LOCK))
		fprintf (TMP_F, "\n\t%s\n", KEYWORD_STRS[KEYWORD_SUPER_LOCKED]);

	if (flags & WF_WRITE_NOTEFILE_ID) {
		arr_each (&item->notefile_idxes, _)
			fprintf (TMP_F, "\n\t%s = %d\n", KEYWORD_STRS[KEYWORD_NOTEFILE_ID], *_);
	}
	if (item->froms && item->froms->n) {
		fputc ('\n', TMP_F);
		arr_each (item->froms, _)
			fprintf (TMP_F, "\t%s = %s\n", KEYWORD_STRS[KEYWORD_WHERE_FROM], _->body);
	}

	if (flags & WF_ALLOW_REMOVED && item->has & (1 << IP_REMOVE))
		fprintf (TMP_F, "\n\t.remove\n");
	fprintf (TMP_F, "\n");
}

// Writes to a notefile or to an tmp_f. Which one it does depends on
// if tmp_f is 0. It writes to a tmpfile () first regardless. The
// tmpfile is made file-local.
//
int write_items (tsk_ctx *ctx, tsk_cmd *cmd, char *notefile, FILE *f, struct tsk_items *items,
		tsk_view *view, bool append, enum write_flags flags) {

	TMP_F = tmpfile ();
	if (!TMP_F)
		return tsk_warn (ctx, cmd, "Tmpfile didn't get created");


	if (!view->n)
		fprintf (TMP_F, "\n");
	else {
		size_t i = 0;
		for (i = 0; i < view->n; i++)
			write_item (items->d + view->d[i].item_idx, i, flags, items);

		if (i != view->n)
			return tsk_warn (ctx, cmd, "There was an error writing to the tmpfile");
	}

	FILE *out_f = f ?: fopen (notefile, append ? "a" : "w");
	if (!out_f)
		return tsk_warn (ctx, cmd, "fopen failed on \"%s\"", notefile);

	if (fileno (out_f) > STDERR_FILENO && flock (fileno (out_f), LOCK_EX))
		return tsk_warn (ctx, cmd, "Couldn't lock notefile %s", notefile);

	if (append)
		fputc ('\n', out_f);
	rewind (TMP_F);

	n_fs_copy_fd_traditionally (fileno (TMP_F), fileno (out_f));

	if (fileno (out_f) > STDERR_FILENO)
		fclose (out_f);

	fclose (TMP_F);
	TMP_F = nullptr;
	return 0;
}

int tsk_write (tsk_ctx *ctx, struct tsk_items *items, struct tsk_cmd *cmd) {
	return write_to_notefiles (ctx, cmd, &ctx->notefiles, items);
}
