#pragma once

// This file and its C file are their own thing entirely because the
// CMD_KEYWORDS array is massive. Makes you scroll too much.

#include "tsk-cmd.h"
extern struct tsk_cmd_keyword CMD_KEYWORDS[N_CTK];
