#define _GNU_SOURCE

#include <asm-generic/errno-base.h>
#include <asm-generic/errno.h>
#include <search.h>
#include "slurp.h"
#include "useful.h"
#include "tsk-item.h"
#include "misc.h"
#include "read-notefile.h"
#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include <assert.h>

static struct hsearch_data HT_KEYWORDS;
static bool HT_INITIALISED;
static struct rn_err_info *RN_ERR_INFO;

typedef struct rn_ctx rn_ctx;
struct rn_ctx {
	char *file_buf;
	char *notefile_path;
};

#define KINDS \
	x(KIND_NOTHING, 0) \
	x(KIND_KEYWORD, "a keyword") \
	x(KIND_END_KEYWORD, 0) \
	x(KIND_VALUE, "a value") \
	x(KIND_INDEX, "an index") \
	x(KIND_EQUALS, "an equals") \
	x(KIND_ERROR, 0) \
	x(KIND_NEWLINE, "a newline") \
	x(KIND_EOF, 0)

enum kind {
#define x(a, b) a,
	KINDS
#undef x
	KIND_N
};

#define x(a, b) b,
static char *KIND_STRS[] = {KINDS};
#undef x

#define x(a, b, c) b,
char *KEYWORD_STRS[64] = {KEYWORDS};
#undef x

#define x(a, b, c) c,
int KEYWORD_NS[] = {KEYWORDS};
#undef x

struct rn_token {
	char *s;
	char *e;
	enum kind kind;
	enum keyword_id keyword_id;
};

static int N_TOKENS;

static struct err_info get_err_info (rn_ctx *rn_ctx, struct rn_token *t) {
	struct err_info r = {};
	char *p = rn_ctx->file_buf;
	while (p < t->s) {
		if (*p == '\n')
			r.linenr++;
		p++;
	}
	while (p > rn_ctx->file_buf && *p != '\n')
		p--;
	if (*p == '\n')
		p++;

	char *line_start = p;
	char *line_end = strchrnul (line_start, '\n');

	(void) p;

	strncpy (r.line, line_start, line_end - line_start);

	r.start_col = t->s - line_start;

	p = t->s;
	while (!isspace (*p))
		p++;

	// Because of how the parser is, the token length might go all the
	// way to the end of the line. It might do other things; who
	// knows. Anyway, we can just count the length of the word.
	r.bad_word_len = p - t->s;

	return r;
}

static int rn_errx (rn_ctx *rn_ctx, struct rn_token *t, char *fmt, ...) {
	va_list ap;
	va_start (ap);
	char msg_buf[BUFSIZ];
	vsnprintf (msg_buf, sizeof msg_buf, fmt, ap);
	va_end (ap);
	if (t)
		RN_ERR_INFO->err_info = get_err_info (rn_ctx, t);
	else
		RN_ERR_INFO->err_reading_file = 1;
	snprintf (RN_ERR_INFO->msg, sizeof RN_ERR_INFO->msg, "%s", msg_buf);

	// There's only a line if we had an error parsing. If not, we
	// failed to open the file, do it's not right to say "In
	// <notefile_path>". We're just using the existence of the line (*line
	// != 0) to encode that, here and in tsk-err.c:print_header.
	if (*RN_ERR_INFO->err_info.line) {
		snprintf (RN_ERR_INFO->err_info.header, sizeof RN_ERR_INFO->err_info.header,
				"In %s:", rn_ctx->notefile_path);
	}
	return 1;
}

char **get_str_from_index (const void *it, void *user) {
	return &((char **) user)[*(int *) it];
}

static int rn_looking_for_but_generic (rn_ctx *rn_ctx, struct rn_token *t, char **strs,
		int *looking_for, bool quote_words) {

	char *str = rab_get_comma_list (0, looking_for,
			&(rab_pclo) {
				.flags = quote_words ? 0 : RAB_PCLF_DONT_QUOTE_WORDS,
				.and_str = "or",
				.getter = &(rab_getter) {
					.type_size = sizeof *looking_for,
					.func = get_str_from_index,
					.user = strs,
					.sentinel = (int []) {-1},
				}
			});
	rn_errx (rn_ctx, t, "Looking for %s, but found %.*s", str, (int) (t->e - t->s), t->s);
	free (str);
	return 1;
}

static int rn_looking_for_kind_but (rn_ctx *rn_ctx, struct rn_token *t, enum kind *looking_for) {
	return rn_looking_for_but_generic (rn_ctx, t, KIND_STRS, (int *) looking_for, 0);
}

static int rn_looking_for_keyword_but (rn_ctx *rn_ctx, struct rn_token *t,
		enum keyword_id *looking_for) {
	return rn_looking_for_but_generic (rn_ctx, t, KEYWORD_STRS, (int *) looking_for, 1);
}

static inline enum keyword_id which_keyword (char *pos) {
	char *e = pos;
	while (!strchr (" \n\t=", *e))
		e++;
	char scratch[256];
	sprintf (scratch, "%.*s", (int) (e - pos), pos);
	static struct entry *match;
	int ret = hsearch_r ((struct entry) {.key = scratch}, FIND, &match, &HT_KEYWORDS);
	if (!ret) {
		if (errno == ESRCH) {
			errno = 0;
			return KEYWORD_NOTHING;
		} else
			assert (0);
	}

	// This is in in the manpage. The data member of hsearch entry is
	// a pointer, but I don't need a pointer. This (int) (long) thing
	// lets me treat it as an int.
	return (int) (long) match->data;
}

static void lex_get_value (rn_ctx *rn_ctx, struct rn_token *t) {
	while (*t->e) {
		if (*t->e == '=') {

			// note: I already see a bug here. If your body was e.g., "1 ="
			if (tsk_is_all_x (t->s, t->e, isdigit, 1)) {
				t->kind = KIND_INDEX;
				break;
			}
		}

		// Values are terminated by keywords
		if (*t->e == '.' && which_keyword (t->e)
				&& (t->e > rn_ctx->file_buf && t->e[-1] != '\\'))
			break;
		t->e++;
	}
	if (!isspace (*t->e))
		t->e--;
	while (isspace (*t->e))
		t->e--;
	t->e++;
	if (!t->kind)
		t->kind = KIND_VALUE;
}

// Doesn't return errors, it seems. If it's not keyword or whatever
// it's a value. It's cause I don't use quotes.

static void rn_get_token (rn_ctx *rn_ctx, struct rn_token *t, struct rn_token *last) {
	*last = *t;
	while (isspace (*t->e))
		t->e++;

	assert (t->e[0] != -1);

	struct rn_token r = (struct rn_token) {
		.s = t->e, .e = t->e + 1,
	};
	switch (*r.s) {
		case '\0':
			r.kind = KIND_EOF;
			break;
		case '.':
			if (*r.e - 1 == '\\')
				lex_get_value (rn_ctx, &r);
			else if ((r.keyword_id = which_keyword (r.s)) != KEYWORD_NOTHING) {
				r.kind = KIND_KEYWORD;
				r.e += KEYWORD_NS[r.keyword_id] - 1;
			} else if (*r.s == 'e'
					&& $strnmatch (r.s + 1, "endnotes", u_len ("endnotes") - 1))
				r.kind = KIND_EOF;
			else
				lex_get_value (rn_ctx, &r);

			// NOTE: get rid of this, replace with just .endnote and then
			// keep strstr the file for more .note
			break;
		case '=':
			r.kind = KIND_EQUALS;
			break;
		case '\n':
			r.kind = KIND_NEWLINE;
			break;
		case '\\': // fall-through
		default:
			lex_get_value (rn_ctx, &r);
			break;
	}
	N_TOKENS++;
	*t = r;

}

static int get_kind_or_die (rn_ctx *rn_ctx, enum kind kind, struct rn_token *t,
		struct rn_token *last) {
	rn_get_token (rn_ctx, t, last);
	if (t->kind != kind) {
		rn_looking_for_kind_but (rn_ctx, t, (enum kind []) {kind, -1});
		return 1;
	}
	return 0;
}

static int expect_equals_and_value (rn_ctx *rn_ctx, struct rn_token *t, struct rn_token *last) {
	rn_get_token (rn_ctx, t, last);
	if (t->kind == KIND_INDEX)
		rn_get_token (rn_ctx, t, last);
	if (t->kind != KIND_EQUALS) {
		rn_looking_for_kind_but (rn_ctx, t, (enum kind []) {KIND_EQUALS, -1});
		return 1;
	}
	if (get_kind_or_die (rn_ctx, KIND_VALUE, t, last))
		return 1;
	return 0;
}

static int expect_equals_get_index_get_value (rn_ctx *rn_ctx, int *r, struct rn_token *t,
		struct rn_token *last) {
		// This is naff, I know.
	rn_get_token (rn_ctx, t, last);
	if (t->kind == KIND_INDEX) {
		if ($get_int (r, t->s, t->e))
			return rn_errx (rn_ctx, t, "Bad int");
		rn_get_token (rn_ctx, t, last);
	}
	if (t->kind != KIND_EQUALS)
		return rn_looking_for_kind_but (rn_ctx, t, (enum kind []) {KIND_EQUALS, -1});
	if (get_kind_or_die (rn_ctx, KIND_VALUE, t, last))
		return 1;

	return 0;
}

static int get_str_val (rn_ctx *rn_ctx, char **str, struct rn_token *t,
		struct rn_token *last) {
	if (*str)
		return rn_errx (rn_ctx, t, "You've defined name twice");
	if (get_kind_or_die (rn_ctx, KIND_EQUALS, t, last))
		return 1;
	if (get_kind_or_die (rn_ctx, KIND_VALUE, t, last))
		return 1;
	*str = strndup (t->s, t->e - t->s);
	return 0;
}

static int rn_parse_dur (rn_ctx *rn_ctx, struct ts_constant *r,
		struct rn_token *t, struct rn_token *last, enum time_id time_id) {

	*r = (struct ts_constant) {
		.type = TS_CONSTANT_TYPE_DURATION,
		.name = TIMES.d[time_id].name
	};
	if (expect_equals_and_value (rn_ctx, t, last))
		return 1;

	r->dur = ts_dur_from_dur_str (t->s, t->e);

	if (ts_errno)
		return rn_errx (rn_ctx, t, "Failed to parse dur: %s", ts_strerror (ts_errno));
	return 0;
}

static int rn_parse_time (rn_ctx *rn_ctx, struct ts_constant *r, struct rn_token *t,
		struct rn_token *last, enum time_id time_id) {

	*r = (struct ts_constant) {
		.type = TS_CONSTANT_TYPE_TIME,
		.name = TIMES.d[time_id].name,
	};
	if (expect_equals_and_value (rn_ctx, t, last))
		return 1;

	r->time = ts_time_from_str (t->s, t->e, &TIMES.now.time,
			TS_MATCH_ALL);

	if (ts_errno)
		return rn_errx (rn_ctx, t, "Failed to parse date/time: %s",
				ts_strerror (ts_errno));
	return 0;
}

static int rn_parse_time_range (rn_ctx *rn_ctx, struct ts_constant *r,
		struct rn_token *t, struct rn_token *last, enum time_id time_id) {

	*r = (struct ts_constant) {
		.type =  TS_CONSTANT_TYPE_TIME_RANGE,
		.name = TIMES.d[time_id].name,
	};
	if (expect_equals_and_value (rn_ctx, t, last))
		return 1;

	r->time_range = ts_time_range_from_str (t->s, t->e, &TIMES.now.time, TS_MATCH_ALL);

	if (ts_errno)
		return rn_errx (rn_ctx, t, "Failed to parse time range: %s",
				ts_strerror (ts_errno));

	return 0;
}

static int get_body (rn_ctx *rn_ctx, char **pr, struct rn_token *t, struct rn_token *last) {
	int index;
	if (expect_equals_get_index_get_value (rn_ctx, &index, t, last))
		return 1;
	(void) index;

	*pr = strndup (t->s, t->e - t->s);
	return 0;
}

static int get_str_item (rn_ctx *rn_ctx, struct tsk_item *pr, struct rn_token *t, struct rn_token *last,
		enum item_type type) {
	if (expect_equals_and_value (rn_ctx, t, last))
		return 1;
	*pr = (struct tsk_item) {.type = type};
	pr->body = strndup (t->s, t->e - t->s);
	return 0;
}

// This checks for dups as it goes. I MEAN. If you're checking for
// dups, you're checking for dups. There's no way around it. And this
// is ... better than doing it later and memcpying?
 static int add_str_item_if_not_dup (rn_ctx *rn_ctx, struct rn_token *t,
		 struct rn_token *last, struct tsk_item *item, enum item_type type) {
	struct tsk_item prop;
	if (get_str_item (rn_ctx, &prop, t, last, type))
		return 1;
	if (!tsk_find_item (item->props, &prop))
		arr_add (item->props, prop);
	return 0;
}

// Maybe this could allocate, since it's going to copy it once it's
// made it anyway. Presumable it's faster to copy, since dstrs x2s
// its allocations.

static int rn_parse_int (rn_ctx *rn_ctx, int *r, struct rn_token *t,
		struct rn_token *last) {
	if (expect_equals_and_value (rn_ctx, t, last))
		return 1;
	if ($get_int (r, t->s, 0)) {
		rn_errx (rn_ctx, t, "Couldn't get number");
		return 1;
	}
	return 0;
}

static int check_prop_deps (rn_ctx *rn_ctx, struct tsk_item *item, enum prop_id checking,
		struct rn_token *t) {
	int missing_dep = note_is_missing_this_prop_dep (item, checking);
	if (missing_dep > -1) {
		rn_errx (rn_ctx, t, "Can't have %s without %s", PROP_KEYWORDS[checking].long_name,
				PROP_KEYWORDS[missing_dep].long_name);
		return 1;
	}
	return 0;
}

// This is a mess, I know. FIXME. This or some portion of it should
// run at the end of the program.
static int update_note (rn_ctx *rn_ctx, struct rn_token *t, struct tsk_item *item) {

	auto times = &item->times;
	auto on = &times->on.time;
	auto except = &times->except.time_range;
	auto interval = &times->interval.dur;
	auto ending = &times->ending.time;
	auto except_interval = &times->except_interval.dur;
	auto before = &times->before.time;
	auto stop_repeating = &times->stop_repeating.time;

	if (item->props && item->props->n) {

		// I'll make this temporary to keep myself from getting confused
		// with the indirection.
		auto tmp_items = item->props->d;
		$qsort (tmp_items, item->props->n, cmp_item_by_type);
		item->props->d = tmp_items;
	}

	times->now = (struct ts_constant) {
		.time = ts_time_from_ts (time (nullptr)), .name = times->now.name,
		.type = TS_CONSTANT_TYPE_TIME
	};

	// NOTE: at this point we should only have on and before and
	// interval_diff_relative_to_on.
	if (!memcmp (&times->creation, &(struct ts_constant) {}, sizeof times->creation))
		times->creation.time = TIMES.now.time;

	// Make sure there's no interval without has save the diff so I
	// can recalculate before if interval.has and on changes. No need
	// for ts_dur here.

	time_t on_before_diff = item->has & (1 << NP_BEFORE)
		? before->ts - on->ts
		: 0;

	time_t on_ending_diff = item->has & (1 << NP_ENDING)
		? ending->ts - on->ts
		: 0;

	// Only put forward on if now is after on + interval, because the
	// item only recurs at interval.
	if (item->has & (1 << NP_ON) && item->has & (1 << NP_INTERVAL)
			&& TIMES.now.time.ts > on->ts + ts_secs_from_dur (*interval, on)) {

		ts_add_dur_to_time_until (on, TIMES.now.time.ts, interval, 0);

		if (item->has & (1 << NP_EXCEPT) && item->has & (1 << NP_EXCEPT_INTERVAL))
			ts_add_dur_to_time_range_until (except, TIMES.now.time.ts, except_interval);

		item->has &= ~(1 << NP_DONE);

		// Postpone goes away when the next one comes around
		// regardless. This is probably the right thing to do
		item->has &= ~(1 << NP_POSTPONE);
		item->has &= ~(1 << NP_LAST_REMINDED);
		if (item->has & (1 << NP_BEFORE))
			*before = ts_time_from_ts (on->ts + on_before_diff);
		if (item->has & (1 << NP_ENDING))
			*ending = ts_time_from_ts (on->ts + on_ending_diff);

		if (item->has & (1 << NP_STOP_REPEATING) && TIMES.now.time.ts > stop_repeating->ts) {
			item->has &= ~(1 << NP_INTERVAL);
			item->has &= ~(1 << NP_STOP_REPEATING);
		}
	}

	if (item->has & (1 << NP_BEFORE) && before->ts < on->ts)
		return rn_errx (rn_ctx, t, "Note's before is less than its on");

	if (item->has & (1 << NP_ENDING) && ending->ts < on->ts)
		return rn_errx (rn_ctx, t, "Note's ending is less than on");

	if (item->has & (1 << NP_INTERVAL))
		*ending = ts_time_from_ts (u_min (ending->ts,
					(on->ts + ts_secs_from_dur (*interval, on))));
	return 0;
}

#define $rn_parse_time_oe(_which, _id) \
			if (rn_parse_time (rn_ctx, _which, t, last, _id)) \
				return 1;
#define $rn_parse_dur_oe(_which, _id) \
			if (rn_parse_dur (rn_ctx, _which, t, last, _id)) \
				return 1;
#define $rn_parse_time_range_oe(_which, _id) \
			if (rn_parse_time_range (rn_ctx, _which, t, last, _id)) \
				return 1;

int parse_non_note_prop (rn_ctx *rn_ctx, tsk_ctx *ctx, struct tsk_item *pr,
		struct rn_token *t, struct rn_token *last, enum read_flags flags) {
	switch (t->keyword_id) {
		case KEYWORD_REMOVE:
			pr->has |= (1 << IP_REMOVE);
			break;
		case KEYWORD_NAME:
			if (get_str_val (rn_ctx, &pr->name, t, last))
				return 1;
			break;
		case KEYWORD_CREATED:

			// Autocmds should probably have creation times. Why
			// not? But the times are in tsk_note, so that's that.
			if (pr->type == IT_NOTE)
				$rn_parse_time_oe (&pr->times.creation, TIME_CREATION);
			break;
		case KEYWORD_LOCKED:
			pr->has |= (1 << IP_LOCK);
			break;
		case KEYWORD_SUPER_LOCKED:
			pr->has |= (1 << IP_SUPER_LOCK);
			break;
		case KEYWORD_WHERE_FROM:
			struct tsk_item new = {};
			if (get_str_item (rn_ctx, &new, t, last, IT_FROM))
				return 1;
			if (!pr->froms)
				pr->froms = u_calloc (sizeof (struct tsk_items));
			arr_add (pr->froms, new);
			pr->has |= (1 << IP_FROMS);
			break;
		case KEYWORD_NOTEFILE_ID:
			if (flags & RF_EXPECT_NOTEFILE_ID) {
				int notefile_idx;
				if (rn_parse_int (rn_ctx, &notefile_idx, t, last))
					return 1;
				assert (notefile_idx >= 0 && notefile_idx < ctx->notefiles.n);
				arr_add (&pr->notefile_idxes, notefile_idx);
			}
			break;
		default:
			if (t->keyword_id != KEYWORD_NOTE
					&& t->keyword_id != KEYWORD_AUTOCMD
					&& t->kind != KIND_EOF) {
				rn_looking_for_keyword_but (rn_ctx, t,
						(enum keyword_id []) {KEYWORD_NOTE, KEYWORD_AUTOCMD, -1});
				return 1;
			} else
				return -1;
	}
	return 0;
}

static int parse_non_note_props (rn_ctx *rn_ctx, tsk_ctx *ctx, struct tsk_item *pr, struct rn_token *t,
		struct rn_token *last, enum read_flags flags) {

	int r = 1;
	for (;;) {
		rn_get_token (rn_ctx, t, last);
		r = parse_non_note_prop (rn_ctx, ctx, pr, t, last, flags);

		// Awful, I know.
		if (r == 1)
			return 1;
		else if (r == -1)
			break;
	}
	*t = *last;
	return 0;
}

static int get_note (rn_ctx *rn_ctx, tsk_ctx *ctx, struct tsk_item *pr, struct rn_token *t,
		struct rn_token *last, enum read_flags flags) {

	pr->props = u_calloc (sizeof (struct tsk_items));
	pr->type = IT_NOTE;
	pr->effort = pr->priority = -1;

	if (get_body (rn_ctx, &pr->body, t, last))
		return 1;
	pr->has |= (1 << IP_BODY);

	$fori (i, N_TIMES)
		pr->times.d[i].type = TIMES.d[i].type;

	bool got_eof_or_new_item = 0;
	while (!got_eof_or_new_item) {
		rn_get_token (rn_ctx, t, last);
		if (t->kind == KIND_EOF)
			break;
		if (t->kind != KIND_KEYWORD) {
			rn_looking_for_kind_but (rn_ctx, t, (enum kind []) {KIND_KEYWORD, -1});
			return 1;
		}
		switch (t->keyword_id) {

			case KEYWORD_ON:
				$rn_parse_time_oe (&pr->times.on, TIME_ON);
				pr->has |= (1 << NP_ON);

				break;

			case KEYWORD_REMINDER:
				$rn_parse_dur_oe (&pr->times.reminder, TIME_REMINDER);
				pr->has |= (1 << NP_REMINDER);
				break;

			case KEYWORD_LAST_REMINDED:
				$rn_parse_time_oe (&pr->times.last_reminded, TIME_LAST_REMINDED);
				pr->has |= (1 << NP_LAST_REMINDED);
				break;

			case KEYWORD_POSTPONE:
				$rn_parse_time_oe (&pr->times.postpone, TIME_POSTPONE);
					return 1;
				pr->has |= (1 << NP_POSTPONE);

				// Here is where we get rid of postpone if we're past it
				if (TIMES.now.time.ts > pr->times.postpone.time.ts)
					pr->has &= ~(1 << NP_POSTPONE);
				break;

			case KEYWORD_BEFORE:
				$rn_parse_time_oe (&pr->times.before, TIME_BEFORE);
				pr->has |= (1 << NP_BEFORE);
				break;

			case KEYWORD_ENDING:
				$rn_parse_time_oe (&pr->times.ending, TIME_ENDING);
				pr->has |= (1 << NP_ENDING);
				break;

			case KEYWORD_EFFORT:
				if (rn_parse_int (rn_ctx, &pr->effort, t, last))
					return 1;
				pr->has |= (1 << NP_EFFORT);
				break;

			case KEYWORD_PRIORITY:
				if (rn_parse_int (rn_ctx, &pr->priority, t, last))
					return 1;
				pr->has |= (1 << NP_PRIORITY);
				break;

			case KEYWORD_DONE:
				pr->has |= (1 << NP_DONE);
				break;

			case KEYWORD_INTERVAL:
				$rn_parse_dur_oe (&pr->times.interval, TIME_INTERVAL);
				pr->has |= (1 << NP_INTERVAL);
				break;

			case KEYWORD_EXCEPT:
				$rn_parse_time_range_oe (&pr->times.except, TIME_EXCEPT);
				pr->has |= (1 << NP_EXCEPT);
				break;

			case KEYWORD_EXCEPT_INTERVAL:
				$rn_parse_dur_oe (&pr->times.except_interval, TIME_EXCEPT_INTERVAL);
				pr->has |= (1 << NP_EXCEPT_INTERVAL);
				break;

			case KEYWORD_AUTOCMD: 		// fall-through
			case KEYWORD_NOTE: 			// fall-through
				got_eof_or_new_item = 1;
				break;

			case KEYWORD_STOP_REPEATING:
				$rn_parse_time_oe (&pr->times.stop_repeating, TIME_STOP_REPEATING);
				pr->has |= (1 << NP_STOP_REPEATING);
				break;

			case KEYWORD_ANNOTATION:
				if (add_str_item_if_not_dup (rn_ctx, t, last, pr, IT_ANNOTATION))
					return 1;
				pr->has |= (1 << NP_ANNOTATION);
				break;

			case KEYWORD_TAG:
				if (add_str_item_if_not_dup (rn_ctx, t, last, pr, IT_TAG))
					return 1;
				pr->has |= (1 << NP_TAG);
				break;

			case KEYWORD_LINK:
				if (add_str_item_if_not_dup (rn_ctx, t, last, pr, IT_LINK))
					return 1;
				pr->has |= (1 << NP_LINK);
				break;

			case KEYWORD_ATTACHMENT:
				if (add_str_item_if_not_dup (rn_ctx, t, last, pr, IT_ATTACHMENT))
					return 1;
				pr->has |= (1 << NP_ATTACHMENT);
				break;

			default:
				if (parse_non_note_prop (rn_ctx, ctx, pr, t, last, flags))
					return 1;
				break;
		}
	}

	if (update_note (rn_ctx, t, pr))
		return 1;

	*t = *last;
	return 0;
}

static int get_autocmd (rn_ctx *rn_ctx, tsk_ctx *ctx, struct tsk_item *pr,
		struct rn_token *t, struct rn_token *last, enum read_flags flags) {
	pr->type = IT_AUTOCMD;
	if (get_body (rn_ctx, &pr->body, t, last))
		return 1;
	pr->has |= (1 << IP_BODY);

	if (parse_non_note_props (rn_ctx, ctx, pr, t, last, flags))
		return 1;
	return 0;
}

static void init_keyword_hash_table_if_need_be () {
	if (HT_INITIALISED)
		return;
	struct entry *whatever;
	assert (hcreate_r (64, &HT_KEYWORDS));
	for (long i = 1; i < N_KEYWORD; i++) {
		struct entry item = {.key = KEYWORD_STRS[i], .data = (void *) i};
		assert (hsearch_r (item, ENTER, &whatever, &HT_KEYWORDS));
	}
	HT_INITIALISED = 1;
}

// Returns: trickily, -1 for a proper error, 1 for the file not being
// there. That's because parse_notefile_list error if the file's not
// there, else you'd never be able to start a new notefile.
int read_notefile (tsk_ctx *ctx, tsk_notefile * const notefile,
		enum read_flags flags, struct rn_err_info *_rn_err_info, tsk_items *out) {

	int n_items_before = out->n;
	$zero_obj (_rn_err_info);
	RN_ERR_INFO = _rn_err_info;
	init_keyword_hash_table_if_need_be ();

	rn_ctx rn_ctx = {
		.notefile_path = notefile->path,
	};

	struct stat sb;

	if (stat (notefile->path, &sb)) {

		// A non-existent file isn't an error because if it was you
		// couldn't do "note f /tmp/new-file ,'whatever'".
		if (errno == ENOENT) {

			// This should get turned into "<the notefile>: no such
			// file or directory".
			rn_errx (&rn_ctx, 0, "%s", rn_ctx.notefile_path);
			return 1;
			/* No rn_errx, no cmd_err; no token, no error line to print */
		} else {
			rn_errx (&rn_ctx, 0, "stat failed on \"%s\"", rn_ctx.notefile_path);
			return -1;
		}
	}
	else if ((sb.st_mode & S_IFMT) != S_IFREG) {
		ctx->opts->flags |= TSK_FLAG_PRINT_ERRNO;

		/* No need for rn_errx, no cmd_err; no token, no error line to print */
		rn_errx (&rn_ctx, 0, "%s isn't a regular file", rn_ctx.notefile_path);
		return -1;
	}

	// Note: I'm setting errno to 0 here, because I need it for the
	// above tsk_err.
	errno = 0;
	rn_ctx.file_buf = slurp (notefile->path);
	if (!rn_ctx.file_buf) {
		errno = 0;

		// When you do f, it doesn't matter if a notefile exists --
		// it'll be created.
		//
		// But that's not the case for S (shortcut notefiles). If
		// they're not there, it's a match failure. Perhaps this means
		// I should have this logic here. But my believe right now is
		// error messages are better issued in the lowest common
		// denominater function. Else you might forget to deal with
		// it. So, it's here.
		if (flags & notefile->flags & TSK_FILE_FLAG_IS_SHORTCUT) {

			snprintf (RN_ERR_INFO->msg, sizeof RN_ERR_INFO->msg, "Can't read \"%s\"",
					notefile->path);
		}

		// Return 1 regardless. It _is_ a
		// failure.parse_shortcut_notefile_list and
		// parse_notefile_list interpret this differently.
		return 1;
	}
	int r = -1;
	struct rn_token t = (struct rn_token) {
		.e = rn_ctx.file_buf,
		.s = rn_ctx.file_buf
	}, last = {};

	while (t.kind != KIND_EOF) {
		rn_get_token (&rn_ctx, &t, &last);
		struct tsk_item item = {};
		if (t.kind == KIND_EOF)
			break;
		switch (t.keyword_id) {
			case KEYWORD_AUTOCMD:
				if (get_autocmd (&rn_ctx, ctx, &item, &t, &last, flags))
					goto out;
				break;
			case KEYWORD_NOTE:
				if (get_note (&rn_ctx, ctx, &item, &t, &last, flags))
					goto out;
				break;
			default:
				rn_looking_for_keyword_but (&rn_ctx, &t, (enum keyword_id []) {KEYWORD_NOTE,
						KEYWORD_AUTOCMD, -1});
				goto out;

			if (!item.body) {
				rn_errx (&rn_ctx, &t, "Item has no body");
				goto out;
			}
		}

		$fori (i, N_PROPS) {
			if (check_prop_deps (&rn_ctx, &item, i, &t))
				goto out;
		}
		// You don't need super-locked and locked. That'd be beyond
		// annoying.
		if (item.has & (1 << IP_SUPER_LOCK) && item.has & (1 << IP_LOCK))
			item.has &= ~(1 << IP_LOCK);

		if (!(flags & RF_EXPECT_NOTEFILE_ID))

			// Trickness: we might have no notefiles here, but we will
			// by the end.
			arr_add (&item.notefile_idxes, ctx->notefiles.n);

		add_necessary_hases (&item);
		arr_add (out, item);
	}
	r = 0;
out:

	if (!r && !(flags & RF_DONT_ADD_NOTEFILE)) {
		notefile->n_items_first_loaded_from_this_notefile = ctx->items.n - n_items_before;
		arr_add (&ctx->notefiles, *notefile);
	}
	free (rn_ctx.file_buf);
	return r;
}


