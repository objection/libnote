#define _GNU_SOURCE
#include "tsk-err.h"
#include "misc.h"
#include "wrap.h"
#include "tsk-print.h"
#include "useful.h"
#include <stdarg.h>
#include <errno.h>
#include <ctype.h>
#include <assert.h>

static int CHARS_PRINTED;
enum { TE_ERR_IS_WHOLE_LINE = -1 };

static void add_colour (tsk_ctx *ctx, enum tsk_colour colour) {
	ctx->pc->colour_index = colour;
	CHARS_PRINTED += ctx->pc->colour (ctx->pc);
}

void start_colour (tsk_ctx *ctx, enum tsk_colour colour) {
	if (ctx->opts->flags & TSK_FLAG_USE_COLOUR)
		add_colour (ctx, colour);
}

void stop_colour(tsk_ctx *ctx) {
	if (ctx->opts->flags & TSK_FLAG_USE_COLOUR)
		add_colour (ctx, TSK_COLOUR_OFF);
}

static int print_header (tsk_ctx *ctx, tsk_cmd *cmd, char *text, bool printing_given_header) {

	if (!*text)
		return 0;

	char quoted_truncated[BUFSIZ];
	get_display_str (ctx, sizeof quoted_truncated, quoted_truncated,
			&(dso) {.no_wrap = 1, .stop_truncation_at_line_end = 1}, text);

	char wrapped[BUFSIZ];
	if (printing_given_header)
		wrap (wrapped, sizeof wrapped, 0, "%s\n", quoted_truncated);
	else {
		struct wrap_opts wo = {};

		// For no particular reason we want the "note:" to not be
		// printed in colour.
		if (ctx->opts->prog_name)
			wo.to_subtract_from_first_line = add_text (ctx, "%s: ",
					ctx->opts->prog_name) + 2;
		wrap (wrapped, sizeof wrapped, &wo, "From %s \"%s\":\n",
				CMD_SOURCE_STRS[cmd->source], quoted_truncated);
	}

	start_colour (ctx, TSK_COLOUR_ERROR_HEADER);
	add_text (ctx, "%s", wrapped);
	stop_colour (ctx);
	return 0;
}

static int get_wrapped_msg (int n_pr, char *pr, char *fmt,
		bool indent_first_line, va_list ap) {

	char unformatted_msg[BUFSIZ];

	// Must save errno in case anything in this function produces one.
	// What happens if errno is produced by this function? Chaos. Not
	// really. But bugs.
	int saved_errno = errno;

	// We can't just directly call wrap_ap, because we might have an
	// errno to print.
	vsnprintf (unformatted_msg, sizeof unformatted_msg, fmt, ap);

	wrap (pr, n_pr, &(wrap_opts) {.first_line_indent = indent_first_line}, "%s%s%s",

			unformatted_msg, saved_errno ? ": " : "", errno ? strerror (errno): "");
	return 0;
}

static int print_err_indicator (tsk_ctx *ctx, char *_, char *err_word_start,
		char *err_word_end, int overall_indent, bool is_second_line) {

	if (!is_second_line) {
		$fori (i, overall_indent)
			add_text (ctx, " ");
	}

	start_colour (ctx, TSK_COLOUR_ERROR_HEADER);

	bool have_reached_non_space = 0;

	for (; *_ && *_ != '\n'; _++) {
		if (!have_reached_non_space && !isspace (*_))
			have_reached_non_space = 1;
		if (have_reached_non_space &&
				_ >= err_word_start && _ < err_word_end)
			add_text (ctx, "~");
		else
			add_text (ctx, " ");
	}
	stop_colour (ctx);
	return 0;
}

static int print_err_line_and_indicator (tsk_ctx *ctx, struct err_info *err_info,
		char *line_symbol) {

	if (!*err_info->line)
		return 0;
	char linenr_str[16];
	snprintf (linenr_str, 16, "%s%d", line_symbol, err_info->linenr);
	int line_number_and_pipe_symbol_len = add_text (ctx, "     %4s | ", linenr_str);

	// It *is_ a line, like a statement line or a line in a notefile,
	// but it will not be a line of text once we've wrapped it.
	char line_display_str[BUFSIZ];

	struct display_str_opts dso = {

		// Ignore truncate-len, entirely because I'm happy with the
		// way I manage to underline only the lines with the error
		// word on it. In any case, you couldn't just truncate. You'd
		// need to also start on the line whether the error word is.
		/* .truncate_len = err_info->start_col + to->truncate_len, */
		.gqesm_mode = GQESO_DONT_ESCAPE,
		.wrap_opts = &(struct wrap_opts) {
			.overall_indent = line_number_and_pipe_symbol_len,
			.to_subtract_from_first_line = line_number_and_pipe_symbol_len,
			.second_line_indent_incr = 1
		}
	};
	get_display_str (ctx, sizeof line_display_str, line_display_str, &dso,
			err_info->line);

	char *start_col = line_display_str + err_info->start_col;
	char *end_col = start_col + err_info->bad_word_len;

	char *line_start = line_display_str;
	bool is_second_line = 0;
	for (char *_ = line_display_str;; _++) {
		if (!*_ || *_ == '\n') {

			// 0ew.............$
			// 0...............$
			// .................
			// .................
			char *line_end = strchrnul (line_start, '\n');
			/* bool error_is_on_this_line = start_col >= line_start && end_col < line_end; */
			if (start_col < line_end && end_col > line_start) {
				add_text (ctx, "\n");
				print_err_indicator (ctx, line_start, start_col, end_col,
						dso.wrap_opts->overall_indent, is_second_line);
			}

			if (_ < start_col)
				start_col += dso.wrap_opts->second_line_indent_incr * 4 +
					dso.wrap_opts->overall_indent;
			if (_ < end_col)
				end_col += dso.wrap_opts->second_line_indent_incr * 4 +
					dso.wrap_opts->overall_indent;
			line_start = _ + 1;
			is_second_line = 1;

			if (!*_)
				break;
		}
		add_text (ctx, "%c", *_);

	}

	add_text (ctx, "\n");

	return 0;
}

// Prints a list of err_infos and then the error message. At
// present and probably forever there's only ever max two err infos;
// always the "from note X" one, and sometimes the read-notefile one.
// The "from note" is fixed, created here; the read-notefile one is
// created in read-notefile and store in err_info.header.
void tsk_warn_ap (tsk_ctx *ctx, tsk_cmd *cmd, char *fmt, va_list ap) {
	assert (cmd);

	BEGIN_MSG (ctx);

	bool should_print_header = cmd->source == CMD_SOURCE_AUTOCMD ||
		ctx->opts->flags & TSK_FLAG_ALWAYS_PRINT_CMD;
	bool should_indent_msg_first_line = 0;

	arr_each (&cmd->err_infos, _) {
		should_indent_msg_first_line = 1;
		if (_ - cmd->err_infos.d == 0) {
			if (should_print_header)
				print_header (ctx, cmd, cmd->text, 0);
			print_err_line_and_indicator (ctx, _, ";");
		} else {
			if (should_print_header)
				print_header (ctx, cmd, _->header, 1);
			print_err_line_and_indicator (ctx, _, _->line_prefix_str);
		}
	}

	char msg[BUFSIZ];
	get_wrapped_msg (sizeof msg, msg, fmt, should_indent_msg_first_line, ap);
	add_text (ctx, "%s\n", msg);

	END_MSG (ctx);
}

int tsk_plain_msg (tsk_ctx *ctx, char *fmt, ...) {

	// Prints a plain "msg", that is to say something that calls the
	// pc->msg function.

	BEGIN_MSG (ctx);

	va_list ap;
	va_start (ap);

	char msg[BUFSIZ];
	get_wrapped_msg (sizeof msg, msg, fmt, false, ap);
	va_end (ap);
	add_text (ctx, "%s\n", msg);

	END_MSG (ctx);
	return 0;
}

int tsk_warn (tsk_ctx *ctx, tsk_cmd *cmd, char *fmt, ...) {
	va_list ap;
	va_start (ap);
	ctx->pc->is_error = 1;
	tsk_warn_ap (ctx, cmd, fmt, ap);
	ctx->pc->is_error = 0;
	va_end (ap);

	// Return 1 so I can just return tsk_err
	return 1;
}

// For simple errors that don't happen when parsing or running cmds,
// eg, in tsk_update_ctx's load_notefile. There's no cmd to print out, no
// error info.
int tsk_warn_no_cmd (tsk_ctx *ctx, char *fmt, ...) {
	va_list ap;
	va_start (ap);

	auto pc = ctx->pc;
	pc->is_error = 1;

	BEGIN_MSG (ctx);

	struct wrap_opts wo = {};
	if (ctx->opts->prog_name)
		wo.to_subtract_from_first_line = add_text (ctx, "%s: ", ctx->opts->prog_name) + 2;

	char wrapped[BUFSIZ];
	wrap_ap (wrapped, sizeof wrapped, &wo, fmt, ap);
	pc->msg (pc, "%s\n", wrapped);
	va_end (ap);

	pc->is_error = 0;
	END_MSG (ctx);

	return 1;
}

static int get_err_line_info (tsk_cmd *cmd, struct err_info *pr,
		int cmd_words_start_idx, int statement_words_len, int err_word_idx) {
	FILE *f = fmemopen (pr->line, sizeof pr->line, "w");
	assert (f);

	// Construct the line, quoting each word, getting the start_col
	// and bad_word_len.
	char word_buf[BUFSIZ];
	int end_idx = cmd_words_start_idx + statement_words_len;
	for (int i = cmd_words_start_idx; i < end_idx; i++) {
		int word_len = get_quote_escaped_str (sizeof word_buf, word_buf,
				cmd->words.d[i], GQESO_SURROUND_WITH_QUOTES_IF_SPACES);

		// If you have an error word to print. Else it's we're doing
		// the-whole-line-is-an-err. This feels a little hacky.
		if (err_word_idx != TE_ERR_IS_WHOLE_LINE) {
			if (i < err_word_idx)
				pr->start_col += word_len;
			else if (i == err_word_idx)
				pr->bad_word_len = word_len;
		}

		fputs (word_buf, f);

		if (i != end_idx) {
			fputc (' ', f);
			if (err_word_idx != TE_ERR_IS_WHOLE_LINE && i < err_word_idx)
				pr->start_col++;
		}
	}

	int r = ftell (f);
	fclose (f);
	pr->line[sizeof pr->line - 1] = 0;
	if (err_word_idx == TE_ERR_IS_WHOLE_LINE)
		pr->bad_word_len = r - 1;
	return r;
}

int get_statement_words_len (tsk_cmd *cmd, int start_idx) {
	int r = 0, i = start_idx;
	while (i < cmd->words.n && strcmp (cmd->words.d[r], ";"))
		i++;
	r = i - start_idx;
	return r;
}

// Sets cmd's err_info. It takes only a cmd_words_idx. It's used in
// action as well as cmd. Action pieces store their cmd_words_idxes.
// It's called "get_cmd_err_info" because read notefile also sets an
// err info, but not using this function.
struct err_info get_cmd_err_info (tsk_cmd *cmd,

		// Can be TE_ERR_IS_WHOLE_LINE, or off the end of the cmd
		// array.
		int err_word_idx, int statement_start_idx) {

	// If you're over the end of the array the err is the whole
	// line. I think this is fine. Obviously I only say that when I'm
	// not sure.
	if (err_word_idx >= cmd->words.n)
		err_word_idx = TE_ERR_IS_WHOLE_LINE;

	struct err_info r = {};

	int statement_words_len = get_statement_words_len (cmd, statement_start_idx);

	get_err_line_info (cmd, &r, statement_start_idx, statement_words_len, err_word_idx);
	return r;
}

#undef add_text

