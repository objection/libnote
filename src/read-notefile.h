#include "tsk-types.h"
#include "tsk-item.h"

enum read_flags {
	RF_EXPECT_NOTEFILE_ID = 1 << 0,

	// This is actually not used?
	RF_RETAIN_NUMBER = 1 << 1,

	// Read notefile adds the new notefile to . But you
	// don't want to do that with change_in_editor. Or maybe you do, I
	// dunno. No, you don't.
	RF_DONT_ADD_NOTEFILE = 1 << 2,
};

struct rn_err_info {
	struct err_info err_info;
	char msg[BUFSIZ];
	bool err_reading_file;
};

int read_notefile (tsk_ctx *ctx, tsk_notefile * const notefile,
		enum read_flags flags, struct rn_err_info *_rn_err_info, tsk_items *out)
	TSK_HIDE;
