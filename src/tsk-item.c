#define _GNU_SOURCE
#include <assert.h>
#include "config.h"
#include "useful.h"
#include "notefile.h"
#include "view.h"
#include "tsk-item.h"
#include "misc.h"

char *CMD_SOURCE_STRS[2] = {"cmd", "autocmd"};

#define x(a, b) b,
char *ACTION_ID_STRS[N_ACTION_IDS] = {ACTION_IDS};
#undef x

#define x(a, b) {.long_name = b},
tsk_keyword ITEM_TYPE_KEYWORDS[N_IT] = {ITS};
#undef x

#define x(a, b, c, d) {.long_name = b, .short_name = c},
tsk_keyword PROP_KEYWORDS[N_PROPS] = {PROPS};
#undef x

#define x(a, b, c, d) d,
enum prop_id PROP_DEPS[N_PROPS] = {PROPS};
#undef x

struct peristent_mods PERSISTENT_MODS;

// Can't include lib/rab/rab.h in tsk-item.c so use a pointer.
static rab_getter __TSK_KEYWORD_LONG_NAME_GETTER = {
	.func = keyword_full_name_getter_fn,
	.type_size = sizeof (tsk_keyword),
};
rab_getter *TSK_KEYWORD_LONG_NAME_GETTER = &__TSK_KEYWORD_LONG_NAME_GETTER;

// This is for passing to the time_string functions, which expect
// an array of ts_constants. It's also used to initialise new notes'
// times.
struct tsk_times TIMES = {
	.d = {
		[TIME_ON]              = {.name = "on", .type = TS_CONSTANT_TYPE_TIME},
		[TIME_POSTPONE]        = {.name = "postpone", .type = TS_CONSTANT_TYPE_TIME},
		[TIME_BEFORE]          = {.name = "before", .type = TS_CONSTANT_TYPE_TIME},
		[TIME_ENDING]          = {.name = "ending", .type = TS_CONSTANT_TYPE_TIME},
		[TIME_INTERVAL]        = {.name = "interval", .type = TS_CONSTANT_TYPE_DURATION},
		[TIME_STOP_REPEATING]  = {.name = "stop-repeating", .type = TS_CONSTANT_TYPE_TIME},
		[TIME_EXCEPT]          = {.name = "except", .type = TS_CONSTANT_TYPE_TIME_RANGE},
		[TIME_EXCEPT_INTERVAL] = {.name = "except-interval", .type = TS_CONSTANT_TYPE_DURATION},
		[TIME_CREATION]        = {.name = "creation", .type = TS_CONSTANT_TYPE_TIME},
		[TIME_NOW]             = {.name = "now", .type = TS_CONSTANT_TYPE_TIME},
		[TIME_REMINDER]        = {.name = "reminder", .type = TS_CONSTANT_TYPE_DURATION},
		[TIME_LAST_REMINDED]   = {.name = "last-reminded", .type = TS_CONSTANT_TYPE_TIME},
	},
};

struct tsk_item tsk_make_item (enum item_type item_type, struct ts_time creation_date) {
	struct tsk_item r = {.type = item_type, .has = (1 << IP_NEW) | (1 << IP_CHANGED)};
	if (item_type == IT_NOTE) {

		// Do allocate this every time. Well, you don't have to. But
		// props get made pretty regularly -- they're made for tags,
		// annotations.
		assert (r.props = u_malloc (sizeof *r.props));
		arr_init (r.props);
		arr_reserve (r.props, 1);
		r.times.creation.time = creation_date,
		r.effort = -1;
		r.priority = -1;

	}

	// We don't allocate "froms" -- not for any particular reason.
	$fori (i, N_TIMES)
		r.times.d[i].type = TIMES.d[i].type;
	return r;
}

struct tsk_item tsk_make_item_with_body (char *str, enum item_type item_type,
		struct ts_time creation_date) {
	struct tsk_item r = tsk_make_item (item_type, creation_date);
	r.body = strdup (str);
	return r;
}

// Finds an item. I'd just use something like lfind now.
// FIXME: get rid of this; use arr_find.
struct tsk_item *tsk_find_item (struct tsk_items *items, struct tsk_item *new) {
	if (!items)
		return 0;
	arr_each (items, _) {
		if (_ != new) {

			// If _ and new the same type and blank, it's a match.
			// FIXME: shouldn't it be an error?
			if (!_->body && !new->body && _->type == new->type)
				return _;

			// They both have a body and they strmatch and their types
			// are the same, return it.
			if (_->body && new->body
					&& $strmatch (_->body, new->body)
					&& _->type == new->type)
				return _;
		}
	}
	return 0;
}

int tsk_cmp_item_autocmds_first (const void *a, const void *b) {
	struct tsk_item *aa = (struct tsk_item *) a;
	struct tsk_item *bb = (struct tsk_item *) b;
	if (aa->type == IT_AUTOCMD && bb->type != IT_AUTOCMD)
		return -1;
	else if (aa->type != IT_AUTOCMD
			&& bb->type == IT_AUTOCMD)
		return 1;
	else if (aa->type == IT_AUTOCMD
			&& bb->type == IT_AUTOCMD)
		return 0;
	else return 0;
}

#if  0
int tsk_cmp_item (const void *a, const void *b) {
	const struct tsk_item *aa = a;
	const struct tsk_item *bb = b;
	if ((aa->type == IT_NOTE)
			&& (bb->type == IT_NOTE)) {
		if (aa->has & (1 << NP_ON) && !(bb->has & (1 << NP_ON))) {
			return -1;
		}
		if (!(aa->has & (1 << NP_ON)) && bb->has & (1 << NP_ON)) {
			return 1;
		}
		if (aa->has & (1 << NP_ON) && bb->has & (1 << NP_ON)) {
			return aa->times.on.time.ts
				- bb->times.on.time.ts;
		}
#if 0
		if (aa->has & (1 << NP_BEFORE) && !(bb->has & (1 << NP_BEFORE))) {
			return -1;
		}
		if (!(aa->has & (1 << NP_BEFORE)) && bb->has & (1 << NP_BEFORE)) {
			return 1;
		}
		if (aa->has & (1 << NP_BEFORE) && bb->has & (1 << NP_BEFORE)) {
			return aa->times.before.time.ts
				- bb->times.before.time.ts;
		}
#endif
	} else return 0;

#if 0
	else if (aa->type == IT_NOTE) {
		return 1;
	} else if (bb->type == IT_NOTE)
		return -1;
	else if (aa->type == IT_AUTOCMD
			&& bb->type != IT_AUTOCMD)
		return -1;
	else if (aa->type != IT_AUTOCMD
			&& bb->type == IT_AUTOCMD)
		return 1;
	else if (aa->type != IT_AUTOCMD
			&& bb->type == IT_AUTOCMD)
		return 0;

#endif
	return 0;
}
#endif

int tsk_cmp_item_start_date (const void *a, const void *b) {
	struct tsk_item *aa = (struct tsk_item *) a;
	struct tsk_item *bb = (struct tsk_item *) b;
	if ((aa->type == IT_NOTE) && (bb->type == IT_NOTE))
		return aa->times.creation.time.ts - bb->times.creation.time.ts;
	else
		return tsk_cmp_item_autocmds_first (a, b);
	return 0;
}

// Note we don't deal with statements. Why are statements items
// anyway?
struct tsk_item tsk_item_dup (struct tsk_item *orig, bool dup_props) {
	struct tsk_item r = *orig;

	if (orig->body)
		r.body = strdup (orig->body);

	if (orig->name)
		r.name = strdup (orig->name);

	if (orig->notefile_idxes.n) {
		r.notefile_idxes = (struct tsk_ints) {};
		arr_pusharr (&r.notefile_idxes, orig->notefile_idxes.d, orig->notefile_idxes.n);
	}

	if ((orig->type == IT_AUTOCMD || orig->type == IT_NOTE) && orig->froms) {
		r.froms = u_calloc (sizeof *r.froms);
		arr_each (orig->froms, _)
			arr_add (r.froms, tsk_item_dup (_, 0));
	}

	if (r.type != IT_NOTE)
		r.props = 0;
	else if (dup_props) {
		r.props = 0;
		r.props = u_calloc (sizeof *r.props);

		arr_reserve (r.props, orig->props->n);

		arr_each (orig->props, _)
				arr_add (r.props, tsk_item_dup (_, 0));

#if 0
		if (orig->n_intervals.n) {
			memcpy (r.n_intervals.d, orig->n_intervals.d,
				sizeof *orig->n_intervals.d * orig->n_intervals.n);
			r.n_intervals.n = orig->n_intervals.n;
			r.n_intervals.a = orig->n_intervals.a;
		}
#endif
	}

	return r;
}

void tsk_free_note (struct tsk_item *item) {
	if (item->props) {
		if (item->props->d) {
			arr_each (item->props, _)
				tsk_free_item (_);
		}
		free (item->props->d);
		free (item->props);
	}
	item->note = (typeof (item->note)) {};
}

void tsk_free_action (struct action *action) {
	arr_each (&action->pieces, _)
		$free_if (_->str);
	$free_if (action->pieces.d);
	*action = (typeof (*action)) {};
}

// Exported
void tsk_free_cmd (struct tsk_cmd *cmd) {
	if (cmd->text)
		free (cmd->text);
	arr_each (&cmd->statements, _)
		tsk_free_statement (_);
	arr_deinit (&cmd->statements);
	*cmd = (typeof (*cmd)) {};
}

void tsk_free_items (struct tsk_items *items) {
	arr_each (items, _)
		tsk_free_item (_);
	free (items->d);
	*items = (typeof (*items)) {};
}

void tsk_free_item (struct tsk_item *item) {

	$free_if (item->body);
	if (item->type == IT_NOTE)
		tsk_free_note (item);
	if (item->froms) {
		tsk_free_items (item->froms);
		free (item->froms);
	}
	$free_if (item->notefile_idxes.d);
	*item = (typeof (*item)) {};
}

void tsk_free_statement (struct tsk_statement *statement) {
	view_free (statement->view);
	free (statement->view);
	tsk_free_action (&statement->action);
	$free_if (statement->notefiles_referenced.d);
	*statement = (typeof (*statement)) {};
}

enum prop_id item_type_to_prop (enum item_type type) {
	switch (type) {
	case IT_ANNOTATION: return NP_ANNOTATION;
	case IT_ATTACHMENT:	return NP_ATTACHMENT;
	case IT_LINK: 		return NP_LINK;
	case IT_TAG: 		return NP_TAG;
	default: return -1;
	}
	return -1;
}

// Returns -1 to mean the note is *not* missing a dep. Pretty weird.
int note_is_missing_this_prop_dep (struct tsk_item *item, enum prop_id checking) {

	enum prop_id deps = PROP_DEPS[checking];

	// If this note doesn't have the property we're checking, we
	// return OK (-1).
	if (!(item->has & (1 << checking)) || !deps)
		return -1;

	// Need to loop here as far as I know so we can print the missing
	// dependency.
	$fori (i, N_PROPS) {
		int dep_bit_set = deps & (1 << i);
		if (dep_bit_set && !(item->has & (1 << i)))
			return i;
	}
	return -1;
}

void *prop_id_to_member (struct tsk_item *item, enum prop_id prop_id) {
	switch (prop_id) {
	case IP_SUBOPTS:         assert (0);
	case IP_NAME:            return &item->name;
	case IP_COPY:            return &item->has;
	case IP_BODY:            return &item->body;
	case IP_NOTEFILE_IDS:    return &item->notefile_idxes;
	case IP_FROMS:           return item->froms;
	case IP_TYPE:            return &item->type;
	case IP_REMOVE:          return &item->has;
	case IP_HAS:             return &item->has;
	case IP_LOCK:            return &item->has;
	case IP_NEW:             return &item->has;
	case IP_CHANGED:         return &item->has;
	case NP_DONE:            return &item->has;
	case IP_SUPER_LOCK:      return &item->has;
	case NP_INTERVAL:        return &item->times.interval;
	case NP_STOP_REPEATING:  return &item->times.stop_repeating;
	case NP_POSTPONE:        return &item->times.postpone;
	case NP_EXCEPT:          return &item->times.except;
	case NP_EXCEPT_INTERVAL: return &item->times.except_interval;
	case NP_ENDING:          return &item->times.ending;
	case NP_REMINDER:        return &item->times.reminder;
	case NP_LAST_REMINDED:   return &item->times.last_reminded;
	case NP_BEFORE:          return &item->times.before;
	case NP_ON:              return &item->times.on;
	case NP_PRIORITY:        return &item->priority;
	case NP_EFFORT:          return &item->effort;
	case NP_ANNOTATION:      return item->props;
	case NP_ATTACHMENT:      return item->props;
	case NP_LINK:            return item->props;
	case NP_TAG:             return item->props;
	case N_PROPS:            assert (0);
	}
	assert (0);
}

static inline bool prop_has (enum prop_id prop_id, enum prop_id item_has) {
	return item_has & (1 << prop_id);
}

static void stringify_ts_constant_prop (tsk_ctx *, size_t n_buf, char *buf,
		struct ts_constant *val, enum prop_id prop_id, enum prop_id item_has, bool) {

	if (!prop_has (prop_id, item_has))
		return;
	switch (val->type) {
		case TS_CONSTANT_TYPE_TIME:
			ts_str_from_ts (buf, n_buf, val->time.ts,

					// Don't use the _NO_T version of this. Since I'm
					// wrapping everything, the using that means the
					// string could get splitted. This difficulty is
					// probably why people don't wrap cli programs.
					TSK_ISO_DATE);
			break;
		case TS_CONSTANT_TYPE_DURATION:
			ts_str_from_dur (buf, n_buf, &val->dur, &TIMES.now.time, 0);
			break;
		case TS_CONSTANT_TYPE_TIME_RANGE:
			ts_str_from_time_range (buf, n_buf, &val->time_range,
					TSK_ISO_DATE_NO_T, 0);
			break;
	}
}

static void stringify_int_prop (tsk_ctx *, size_t n_buf, char *buf, int *val,
		enum prop_id prop_id, enum prop_id item_has, bool) {
	if (!prop_has (prop_id, item_has))
		return;
	snprintf (buf, n_buf, "%d", *val);
}

static void stringify_item_type_prop (tsk_ctx *, size_t n_buf, char *buf,
		enum item_type *item_type, enum prop_id prop_id, enum prop_id item_has,
		bool) {
	if (!prop_has (prop_id, item_has))
		return;
	strncpy (buf, ITEM_TYPE_KEYWORDS[*item_type].long_name, n_buf);
}

static void stringify_has_prop (size_t n_buf, char *buf, enum prop_id *,
		enum prop_id prop_id, enum prop_id item_has) {
	if (item_has & (1 << prop_id))
		strncpy (buf, "set", n_buf);
	else
		*buf = 0;
}

static void stringify_str_prop (tsk_ctx *ctx, size_t n_buf, char *buf, char **val,
		enum prop_id prop_id, enum prop_id item_has, bool quote_if_string) {
	if (!prop_has (prop_id, item_has))
		return;

	int len = strlen (*val);
	assert (len < n_buf);
	if (!quote_if_string)
		memcpy (buf, val, len);
	else {
		get_display_str (ctx, n_buf, buf, &(dso) {.no_wrap = 1,
				.gqesm_mode = quote_if_string ? GQESO_SURROUND_WITH_QUOTES_ALWAYS : 0},
				*val);
	}
}

static void stringify_indexed_strs_prop (tsk_ctx *ctx, size_t n_buf, char *buf,
		struct tsk_ints *val, enum prop_id prop_id, enum prop_id item_has, bool) {
	if (!prop_has (prop_id, item_has))
		return;
	assert (val->n <= n_buf);
	get_comma_list_str_of_items_notefiles (ctx, n_buf, buf, val, 1);
}

static void stringify_prop_prop (tsk_ctx *ctx, size_t n_buf, char *buf,
		struct ts_constant *, enum prop_id prop_id, enum prop_id item_has, bool) {

	if (!prop_has (prop_id, item_has))
		return;
	FILE *f = fmemopen (buf, n_buf, "w");
	assert (f);
	char quote_buf[BUFSIZ];
	int n_written = 0;
	arr_each (&ctx->items, _) {
		if (n_written)
			fprintf (f, ", ");
		n_written++;
		if (item_type_to_prop (_->type) == prop_id) {
			bool quote = strpbrk (_->body, " \t");
			if (quote) {
				get_quote_escaped_str (sizeof quote_buf, quote_buf, _->body,
						GQESO_SURROUND_WITH_QUOTES_IF_SPACES);
				fprintf (f, "\"%s\"", quote_buf);
			} else
				fprintf (f, "%s", _->body);
		}
	}
	fclose (f);
	buf[n_buf - 1] = 0;
}

int stringify_prop_val (tsk_ctx *ctx, size_t n_buf, char *buf, enum prop_id prop_id,
		void *val, enum prop_id item_has, bool quote_if_string) {

#define stringify_it(_func) stringify_##_func##_prop (ctx, n_buf, buf, val, prop_id, \
		item_has, quote_if_string)
	switch (prop_id) {
		case IP_SUBOPTS:         assert (0);
		case IP_NAME:
		case IP_BODY:            stringify_it (str); break;
		case IP_NOTEFILE_IDS:    stringify_it (indexed_strs); break;
		case IP_FROMS:           assert (!"Should deal with froms"); break;
		case IP_TYPE:            stringify_it (item_type); break;
		case IP_REMOVE:
		case IP_HAS:
		case IP_LOCK:
		case IP_NEW:
		case IP_CHANGED:
		case NP_DONE:
		case IP_COPY:
		case IP_SUPER_LOCK:      stringify_has_prop (n_buf, buf, 0, prop_id, item_has); break;
		case NP_INTERVAL:
		case NP_STOP_REPEATING:
		case NP_POSTPONE:
		case NP_EXCEPT:
		case NP_EXCEPT_INTERVAL:
		case NP_ENDING:
		case NP_REMINDER:
		case NP_LAST_REMINDED:
		case NP_BEFORE:
		case NP_ON:              stringify_it (ts_constant); break;
		case NP_PRIORITY:
		case NP_EFFORT:          stringify_it (int); break;
		case NP_ANNOTATION:
		case NP_ATTACHMENT:
		case NP_LINK:
		case NP_TAG:             stringify_it (prop); break;
		case N_PROPS:            assert (0);

	}
#undef stringify_it
	return 0;
}
static int count_item_props (struct tsk_items *it, enum item_type item_type) {
	int r = 0;
	arr_each (it, _) {
		if (_->type == item_type)
			r++;
	}
	return r;
}

static int cmp_arr_prop (struct tsk_items *a, struct tsk_items *b,
		enum prop_id *prop_id) {
	enum item_type item_type = prop_id_to_item_type (*prop_id);
	int a_n = count_item_props (a, item_type);
	int b_n = count_item_props (b, item_type);

	int diff = a_n - b_n;
	if (diff)
		return diff;

	$fori (i, a->n) {
		auto a_item = &a->d[i];
		auto b_item = &b->d[i];
		diff = a_item->type - b_item->type;
		if (diff)
			return diff;
		int a_len = strlen (a_item->body);
		int b_len = strlen (b_item->body);
		diff = a_len - b_len;
		if (diff)
			return diff;
		diff = memcmp (a_item->body, b_item->body, a_len);
		if (diff)
			return diff;
		diff = (a_item->has & (1 << IP_REMOVE)) -
			(b_item->has & (1 << IP_REMOVE));
		if (diff)
			return diff;

	}
	return 0;
}

static int cmp_bitfield_bit_prop (int *a, int *b, int *bit_index) {
	return *a & *bit_index - *b & *bit_index;
}

static int cmp_ts_constant_prop (struct ts_constant *a, struct ts_constant *b, void *) {
	return memcmp (a, b, sizeof *a);
}

static int cmp_str_prop (char **a, char **b, void *) {
	if (!(*a && *b))

		// Is this OK? I think so. It'll get truncated to an int.
		return *a - *b;
	int len_a = strlen (*a);
	int len_diff = len_a - strlen (*b);
	if (len_diff)
		return len_diff;
	return memcmp (*a, *b, sizeof **a * len_a);
}

static int cmp_tsk_ints_prop (struct tsk_ints *a, struct tsk_ints *b, void *) {
	int n_diff = a->n - b->n;
	if (n_diff)
		return n_diff;
	return memcmp (a->d, b->d, sizeof *a->d * a->n);
}

static int cmp_int_prop (int *a, int *b, void *) {
	return *a - *b;
}

#if 0
static int cmp_str_index_prop (struct tsk_ints *a, struct tsk_ints *b,
		struct tsk_strs *strs) {

	int a_sorted[a->n];
	int b_sorted[b->n];
	memcpy (a_sorted, a->d, a->n * sizeof *a_sorted);
	memcpy (b_sorted, b->d, b->n * sizeof *b_sorted);

	// Should just need to qsort. It shouldn't be possible for these
	// arrays to have dups.
	qsort (a_sorted, a->n, sizeof *a_sorted, ncmp_ints);
	qsort (b_sorted, b->n, sizeof *b_sorted, ncmp_ints);

}
#endif

int cmp_prop (void *a, void *b, enum prop_id prop_id) {
	int r;
	switch (prop_id) {
		case IP_SUBOPTS:         assert (0);
		case IP_NAME:
		case IP_BODY:            r = cmp_str_prop (a, b, 0); break;
		case IP_NOTEFILE_IDS:    r = cmp_tsk_ints_prop (a, b, 0); break;
		case IP_FROMS:           assert (!"Should deal with froms"); break;
		case IP_TYPE:            r = cmp_int_prop (a, b, 0); break;
		case IP_REMOVE:
		case IP_HAS:             assert (0);
		case IP_LOCK:
		case IP_NEW:
		case IP_CHANGED:
		case IP_COPY:
		case NP_DONE:
		case IP_SUPER_LOCK:      r = cmp_bitfield_bit_prop (a, b,
										 (enum prop_id []) {1 << prop_id}); break;
		case NP_INTERVAL:
		case NP_STOP_REPEATING:
		case NP_POSTPONE:
		case NP_EXCEPT:
		case NP_EXCEPT_INTERVAL:
		case NP_ENDING:
		case NP_REMINDER:
		case NP_LAST_REMINDED:
		case NP_BEFORE:
		case NP_ON:              r = cmp_ts_constant_prop (a, b, 0); break;
		case NP_PRIORITY:
		case NP_EFFORT:          r = cmp_int_prop (a, b, 0); break;
		case NP_ANNOTATION:
		case NP_ATTACHMENT:
		case NP_LINK:
		case NP_TAG:             r = cmp_arr_prop (a, b, &prop_id); break;
		case N_PROPS:            assert (0);

	}
	return r;
#undef cmp_it
}

bool item_was_loaded_by_at_least_one_list (tsk_ctx *ctx, struct tsk_item *item) {
	arr_each (&item->notefile_idxes, _) {
		if (ctx->notefiles.d[*_].flags & TSK_FILE_FLAG_WAS_LOADED_BY_LIST)
			return 1;
	}
	return 0;
}

bool item_belongs_to_at_least_this_notefile (struct tsk_item *item, int notefile_idx) {
	arr_each (&item->notefile_idxes, _) {
		if (*_ == notefile_idx)
			return 1;
	}
	return 0;
}

// These are necessary because they're used when stringifying
void add_necessary_hases (struct tsk_item *item) {
	item->has |= (1 << IP_NOTEFILE_IDS);
	item->has |= (1 << IP_TYPE);
	item->has |= (1 << IP_HAS);
}

int cmp_item_by_type (const void *a, const void *b) {
	return ((struct tsk_item *) a)->type - ((struct tsk_item *) b)->type;
}

char **keyword_full_name_getter_fn (const void *it, void *) {
	return &((tsk_keyword *) it)->long_name;
}

