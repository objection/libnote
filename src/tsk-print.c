#define _GNU_SOURCE
#include <string.h>
#include <assert.h>
#include "lib/useful/useful.h"
#include "tsk-types.h"
#include "config.h"
#include "tsk-action.h"
#include "misc.h"
#include "notefile.h"
#include "tsk-print.h"
#include "tsk-item.h"
#include "wrap.h"

#define $add_colour(_colour) \
	do { \
		if (ctx->opts->flags & TSK_FLAG_USE_COLOUR) { \
			ctx->pc->colour_index = _colour ; \
			ctx->pc->colour (ctx->pc); \
		} \
	} while (0)

static inline struct ts_time day_start (time_t plus) {
	struct tm beginning_of_day_tm = {.tm_year = -1, .tm_mday = -1, .tm_mon = -1};
	struct ts_time r = ts_time_with_units_set (TIMES.now.time.ts + plus, beginning_of_day_tm);
	assert (r.ts != -1);
	return r;
}

static char *get_time_bit (char *r, struct ts_time *ts_time) {

	char *p = r;
	time_t ts = ts_time->ts;

	char iso_date[32];
	ts_str_from_time (iso_date, sizeof iso_date, ts_time, "%y-%m-%dT%H:%M:%S");
	char daystr[64 * 2], day[64];

	// Really, the beginning of the day of a fortnight from now.
	struct ts_time fortnight_from_now = day_start (TS_SECS_FROM_DAYS (14));

	// Really, the beginning of yesterday.
	struct ts_time yesterday = day_start (-TS_SECS_FROM_DAYS (1));

	// If the date is between (the beginning of) yesterday and the
	// beginning of the day 14 days from now.
	if (ts < fortnight_from_now.ts && ts >= yesterday.ts) {
		struct tm tm = ts_tm_from_ts (ts);
		strftime (day, u_len (day), "%A", &tm);

		// Note (perf): Occurs to me I really don't need to keep
		// calling ts_ts_with_units_set. All I need is the
		// start of today and addition/subtraction. Having said this,
		// ts_ts_with_units_set, should deal properly with
		// daylight savings, etc. I haven't proved that.
		if (ts >= day_start (TS_SECS_FROM_DAYS (7)).ts)
			sprintf (daystr, "next %s", day);
		else if (ts >= day_start (TS_SECS_FROM_DAYS (2)).ts)
			sprintf (daystr, "%s", day);
		else if (ts >= day_start (TS_SECS_FROM_DAYS (1)).ts)
			sprintf (daystr, "tomorrow");
		else if (ts >= day_start (TS_SECS_FROM_DAYS (0)).ts)
			sprintf (daystr, "today");
		else if (ts >= day_start (-TS_SECS_FROM_DAYS (1)).ts)
			sprintf (daystr, "yesterday");
		else
			assert (!"Impossible");
		p += sprintf (p, "%s, ", daystr);
	} else

		// Else, just print the date
		p += sprintf (p, "%.8s ", iso_date);

		// Could indicate if it's happened or yet to happen, here.
		// But I could also print "happened" instead of "on" to mean
		// it's passed. So, I'm undecided.
	p += sprintf (p, "%s", iso_date + 9);
	assert (p < r + 32);
	return r;
#undef $day_start
}

static void print_arr_props (tsk_ctx *ctx, struct tsk_item *item, tsk_view *props_view,
		bool props_were_specified, enum prop_id print_what) {
	bool wrote_something = 0;
	int end;
	if (props_were_specified) {
		if (props_view && props_view->n)
			end = props_view->n;
		else
			return;
	} else
		end = item->props->n;
	if (!end)
		return;
	int line_length = 0, line = 1;
	enum item_type props_type = -1;

	$fori (i, end) {
		auto prop =
			props_were_specified ?
				item->props->d + props_view->d[i].item_idx :
				item->props->d + i;

		if (!(print_what & (1 << item_type_to_prop (prop->type))))
			continue;

		// This is confusing, but it's saying the first thing you
		// do is print, eg, "annotation", else print a ", ".
		if (prop->type != props_type) {
			line_length = 0;
			if (wrote_something && i != (props_were_specified ? props_view->d[0].item_idx : 0))
				ctx->pc->print (ctx->pc, "\n");
			line_length += ctx->pc->print (ctx->pc, "        %ss: ",
					ITEM_TYPE_KEYWORDS[prop->type].long_name);
			wrote_something = 1;
			props_type = prop->type;

		} else if (!ctx->pc->lines || (line < ctx->pc->lines ||
				strlen (prop->body) + line_length + 2 < ctx->pc->cols))
			line_length += ctx->pc->print (ctx->pc, ", ");

		// The 2 is the lenth of ", ".
		if (strlen (prop->body) + line_length + 2 > ctx->pc->cols) {

			if (ctx->pc->lines && line >= ctx->pc->lines)
				break;
			line_length = 0;

			// NOTE: -1 because of '\n'.
			line_length += -1 + ctx->pc->print (ctx->pc, "\n            ");
			line++;
		}
		line_length += strpbrk (prop->body, "\t ")?
			ctx->pc->print (ctx->pc, "\"%s\"", prop->body):
			ctx->pc->print (ctx->pc, "%s", prop->body);
	}

	if (wrote_something)
		ctx->pc->print (ctx->pc, "\n");
}

// This prints a certain number of lines. It doesn't print a number
// representing the number of lines in the selection.
static void print_n_lines (tsk_ctx *ctx, char *buf) {
	char *p = buf;
	if (ctx->pc->lines == 0)
		ctx->pc->print (ctx->pc, "%s\n", buf);
	else $fori (i, ctx->pc->lines) {
		while (*p && *p != '\n')
			ctx->pc->print (ctx->pc, "%c", *p++);
		ctx->pc->print (ctx->pc, "\n"), p++;
		if (*p == '\0')
			break;
	}
}

static void print_short (tsk_ctx *ctx, struct tsk_item *item, tsk_view *view) {

	char scratch[TSK_N_SCRATCH];
	int i = 0;
	bool has_list = view && view->n;
	struct tsk_item *props = 0;
	int types[N_IT] = {};

	// You go through with them
	for (; i < (has_list ? view->n : item->props->n); i++) {
		props = has_list ?
				item->props->d + view->d[i].item_idx :
				item->props->d + i;
		types[props->type] = 1;
	}
	char *p = scratch;
	bool got = 0;
	for (int *type = types; type < types + N_IT; type++) {
		if (*type) {
			got = 1;
			p += sprintf (p, "%s", ITEM_TYPE_KEYWORDS[type - types].long_name);
			if (!tsk_all_arr_is (type + 1, N_IT - (type + 1 - types), 0))
				p += sprintf (p, ", ");
		}
	}
	char scratch_2[TSK_N_SCRATCH];
	wrap (scratch_2, TSK_N_SCRATCH,
			&(wrap_opts) {.initial_tabs = 2, .second_line_indent_incr = 1},
			"%s%s", got? "has ": "",
			scratch);
	if (got)
		ctx->pc->print (ctx->pc, "%s\n", scratch_2);
}

int tsk_print_fmt (tsk_ctx *ctx, tsk_cmd *cmd, int cursor, struct action_piece action_piece,
		struct tsk_items *items, struct tsk_statement *statement) {

	auto item = items->d + cursor;
	char buf[BUFSIZ];
	FILE *f = fmemopen (buf, sizeof buf, "w");
	assert (f);
	for (char *s = action_piece.str; *s; s++) {
		if (*s == '%' && (*(s - 1) != '\\' || s == action_piece.str)) {
			s++;
			switch (*s) {
			case 'b': fputs (item->body, f); break;
			case 'n': fprintf (f, "%d", cursor); break;
			default:  return action_err (ctx, cmd, cursor, action_piece.cmd_words_idx, 0,
							  statement,
							  "Bad format str");
			}
		} else
			fputc (*s, f);
	}
	fclose (f);

	// I don't know why it returns here. It's probably a mistake,
	// but this function doesn't work and, I think, isn't called.
	// Sat 06 Apr 2024 07:26:33 BST: Actually it is, but I only
	// vaguely remember what it does.
	return 0;
	struct wrap_opts wrap_opts = {.second_line_indent_incr = 1};

	// FIXME: this is to stop clang saying VLA has zero size. It's
	// obviously a bad way of doing things. Probably won't make a
	// difference, though.
	char out_buf[BUFSIZ];
	wrap (out_buf, sizeof out_buf, &wrap_opts, "%s", buf);
	print_n_lines (ctx, out_buf);
	return 0;
}

static void print_because_of (tsk_ctx *ctx, tsk_cmd *cmd) {
	char display_str[BUFSIZ];
	get_display_strf (ctx, sizeof display_str, display_str, 0,
			" (because of %s \"%s\"):", CMD_SOURCE_STRS[cmd->source],
			cmd->text);
	ctx->pc->print (ctx->pc, "%s\n", display_str);
}

static void print_header (tsk_ctx *ctx, struct tsk_item *item, int view_cursor) {

	char notefiles_str[32];
	get_comma_list_str_of_items_notefiles (ctx, 32, notefiles_str, &item->notefile_idxes,
			0);
	ctx->pc->print (ctx->pc, "#%zu %s y%s%s%s%s%s",
			view_cursor,
			notefiles_str,
			ITEM_TYPE_KEYWORDS[item->type].long_name,
			(item->has & (1 << IP_LOCK)) ?
				" 🔒" :
				"",
			(item->has & (1 << IP_SUPER_LOCK)) ?
				" S🔒" :
				"",
			(item->has & (1 << NP_DONE)) ?
				" ✓" :
				"",
			(item->has & (1 << IP_REMOVE)) ?
				" ❌" :
				"");

}

static int print_time (tsk_ctx *ctx, struct tsk_item *item, int ts_constant_cursor,
		enum prop_id print_what) {

	auto ts_constants = item->times.d;
	char out_buf[TSK_N_SCRATCH];
	char scratch[TSK_N_SCRATCH];
	struct ts_time *on_time, *postpone_time;

	auto has = time_id_to_prop (ts_constant_cursor);
	struct ts_constant *_ = ts_constants + ts_constant_cursor;
	if (has == -1 || !_->name || !print_what & (1 << has))
		return 0;

	ctx->pc->print (ctx->pc, "        %s", _->name);
	if (_->type == TS_CONSTANT_TYPE_TIME && _->time.ts < TIMES.now.time.ts &&

			// Doesn't make sense to add "passed" to
			// last-reminded. It should always be passed.
			ts_constant_cursor != NP_LAST_REMINDED)
		ctx->pc->print (ctx->pc, " (passed)");
	ctx->pc->print (ctx->pc, ": ");

	// Note that I'm using the name of the _ to check if
	// the thing is there. I don't like it. We have "prop".
	if (_->name == TIMES.d[TIME_ON].name)
		on_time = &_->time;

	else if (_->name == TIMES.d[TIME_POSTPONE].name)
		postpone_time = &_->time;

	if (_->type == TS_CONSTANT_TYPE_TIME) {

		ctx->pc->print (ctx->pc, "%s\n", get_time_bit (scratch, &_->time));

		if (_->time.ts < TIMES.now.time.ts && _->name == TIMES.d[TIME_ON].name
				&& item->has & (1 << NP_INTERVAL)) {

			struct ts_time next_on = ts_time_from_ts (_->time.ts +
					ts_secs_from_dur (item->times.interval.dur, on_time));
			ctx->pc->print (ctx->pc, "        next: %s\n", get_time_bit (scratch, &next_on));
		}

	} else if (_->type == TS_CONSTANT_TYPE_DURATION) {

		ts_str_from_dur (out_buf, sizeof out_buf, &_->dur,

				// Presumably I could just do pointer cmp here
				_->name == TIMES.d[TIME_INTERVAL].name
				? on_time
				: _->name == TIMES.d[TIME_EXCEPT].name
				? postpone_time:
				0, 1);
		ctx->pc->print (ctx->pc, "%s\n", out_buf);

	} else {
		ts_str_from_time (out_buf, sizeof out_buf, &_->time_range.s,
				TSK_ISO_DATE);
		ctx->pc->print (ctx->pc, "%s..", out_buf);
		ts_str_from_time (out_buf, sizeof out_buf, &_->time_range.e,
				TSK_ISO_DATE);
		ctx->pc->print (ctx->pc, "%s\n", out_buf);
	}
	return 0;
}

static void print_times (tsk_ctx *ctx, struct tsk_item *item, enum prop_id print_what) {

	// Attrocious, confusing code ahoy.
	$fori (i, N_TIMES) {
		int time_id_prop = time_id_to_prop (i);
		if (time_id_prop ==

				// "creation" is the only relevant one that can be -1.
				// This means we can print it. Shouldn't be like that
				// but I'm in a hurry.
				-1)
			continue;
		if (!(print_what & (1 << time_id_prop)))
			continue;
		if (print_what == (unsigned) -1 || (time_id_prop != -1 &&
					item->has & (1 << time_id_prop)))
			print_time (ctx, item, i, print_what);
	}
}

int tsk_print (tsk_ctx *ctx, tsk_cmd *cmd, view_node view_node, int view_cursor,
		bool props_were_specified, enum prop_id print_what) {

	if (!print_what)
		return 0;

	auto item = ctx->items.d + view_node.item_idx;

	char out_buf[TSK_N_SCRATCH], scratch[TSK_N_SCRATCH];
	*out_buf = *scratch = 0;

	$add_colour (TSK_COLOUR_PRINT_HEADER);

	print_header (ctx, item, view_cursor);

	if (cmd->source == CMD_SOURCE_AUTOCMD)
		print_because_of (ctx, cmd);
	else
		ctx->pc->print (ctx->pc, "\n");

	$add_colour (TSK_COLOUR_OFF);

	if (print_what & (1 << IP_BODY)) {
		/* if (item->has & (1 << IP_REMOVE)) */
		/* 	strcpy (out_buf, "    (Empty body, removed)"); */
		/* else */
		wrap (out_buf, TSK_N_SCRATCH,
				&(wrap_opts) {
				.initial_tabs = 1,
				.second_line_indent_incr = 0
				}, "%s", item->body);
		print_n_lines (ctx, out_buf);
	}

	if (item->type != IT_NOTE) {
		for (size_t i = 0; i < ctx->opts->nnewlines_between_notes; i++)
			ctx->pc->print (ctx->pc, "\n");
		goto out;
	}

	if (print_what &  (1 << IP_NAME) && item->name)
		ctx->pc->print (ctx->pc, "        name: %s\n", item->name);

	// It's only notes from here.

	if (print_what & (1 << NP_PRIORITY) && item->priority != -1)
		ctx->pc->print (ctx->pc, "        priority: %d\n", item->priority);
	if (print_what & (1 << NP_EFFORT) && item->effort != -1)
		ctx->pc->print (ctx->pc, "        effort: %d\n", item->effort);
	if (ctx->opts->flags & TSK_FLAG_PRINT_SHORT) {
		print_short (ctx, item, view_node.next);
		goto out;
	}
	print_arr_props (ctx, item, view_node.next, props_were_specified, print_what);

	print_times (ctx, item, print_what);

	for (size_t i = 0; i < ctx->opts->nnewlines_between_notes; i++)
		ctx->pc->print (ctx->pc, "\n");

out:;
	fflush (stdout);
	return 0;
}
