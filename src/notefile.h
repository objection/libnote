#pragma once

#include "tsk-types.h"
#include "tsk-item.h"
extern struct rab_getter NOTEFILE_GETTER;

char *tsk_get_notefile_in_dir (tsk_ctx *ctx, char *dir)
	TSK_HIDE;
char **get_notefile_path_member (const void *elem, void *)
	TSK_HIDE;
int get_comma_list_str_of_items_notefiles (tsk_ctx *ctx, int n_pr, char *pr,
		struct tsk_ints *notefile_idxes, bool insert_space_after_comma)
	TSK_HIDE;
tsk_notefile create_notefile_struct (tsk_ctx *ctx, char *path_or_basename,
		enum notefile_flags flags)
	TSK_HIDE;
int free_notefile (tsk_notefile *notefile)
	TSK_HIDE;
int cmp_notefiles_by_path (const void *a, const void *b)
	TSK_HIDE;
int cmp_notefiles_not_not_sfiles_by_path (const void *_a, const void *_b)
	TSK_HIDE;
