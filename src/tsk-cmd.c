#define _GNU_SOURCE

#include "config.h"
#include "lib/n-fs/n-fs.h"
#include "misc.h"
#include "ncmp.h"
#include "notefile.h"
#include "read-notefile.h"
#include "split.h"
#include "str-to-int-arr.h"
#include "tsk-cmd.h"
#include "tsk-err.h"
#include "tsk-print.h"
#include "tsk-types.h"
#include "useful.h"
#include "view.h"
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <glob.h>
#include <search.h>
#include <stdarg.h>
#include "tsk-cmd-keywords.h"

enum { KEYWORD_MAX_LEN = 64 };

enum filter_loop_flags {
	FILTER_LOOP_COMPILE_REGEX = 1 << 0,
};

enum file_filter_mode {
	FFM_LOAD,
	FFM_CREATE,
	FFM_MATCH,
};

// NOTE: Using a fixed buffer like this isn't good enough for listing
// files. You could have more than TSK_MAX_COMPLETION_MATCHES matches in that
// case.
int __COMPLETION_MATCHES_IDXES[TSK_MAX_COMPLETION_MATCHES];

#define ERR_NO_END_AFTER_OPERATOR "You can't end a statement after an operator"

thread_local static regex_t PREG;

// Toplevel statement -- ie, not one produced while parsing brackets
// or props. I can see that this might blow up in my face. It suggests
// that there should be some kind of "inner statement" which we use
// when we're recursing.
thread_local static struct tsk_statement *TOPLEVEL_STATEMENT;

/* #define x(a, b, c, d, e, f, g) {.long_name = a, .short_name = b}, */
/* struct tsk_cmd_keyword CMD_HELP_KEYWORDS[N_CTK] = {CTKS}; */
/* #undef x */

char *CMD_TOKEN_TYPE_STRS[] = {
	[CT_TYPE_FILTER]   = "filter",
	[CT_TYPE_ACTION]   = "action",
	[CT_TYPE_OPERATOR] = "operator",
	[CT_TYPE_MOD]      = "mod",
	[CT_TYPE_SPECIAL]  = "special",
};

enum statement_type {
	STATEMENT_TYPE_NOTE,
	STATEMENT_TYPE_PROP,
};

typedef union match_arg match_arg;
union match_arg {
	const size_t _size_t;
	const char *_str;
	const enum item_type _item_type;
	const enum prop_id _prop_id;
	const struct s2ia_info *_s2ia_info_p;
	const struct ts_time *_ts_time_p;
	const enum time_id _time_id;
	const enum statement_type _statement_type;
};

static struct cmd_token T;

// I'm storing this entirely so I can properly complete words
// with equalses in them, eg, "Sfile=no". If you just output
// "notes.nxt" nothing will happen in Zsh. You need to output
// Sfile=notes.nxt. I'm sure there's another way to do this.
struct cmd_token PREV_T;

// I think (some of these)  globals are reasonable. A C file is like a
// class. And in classes, you have variables global to the class,
// right? I think so. There's no point hauling around all these
// variables. It just makes the parameters go on and on and on.
//
// Having said that, I think they're a sign of dodgy design.

// This means the list a list was actually written (in cli cmd or
// autocmd), like "note n..". It's used to in sort_out_action figure
// out if we the action should be A_ADD.
static bool NON_NOTEFILE_LOADING_FILTER_WAS_GIVEN;

static int parse_statement (tsk_ctx *, tsk_cmd *, int *,
		enum statement_type, const tsk_view *, tsk_completions *, struct tsk_statement *);

static inline bool should_print_completion (tsk_cmd *cmd, tsk_completions *completions) {

	// Note that it's only ever the last word that's completed. This
	// means that you shouldn't pass the whole cmdline to --complete
	// but the part up to the cursor (LBUFFER in Zsh). This is a good
	// way of doing things, simple.
	return completions && T.cmd_words_idx == cmd->words.n - 1;
}

static char **get_cmd_keywords_long_name (const void *it, void *) {
	return &((tsk_cmd_keyword *) it)->keyword.long_name;
}

rab_getter __TSK_CMD_KEYWORD_LONG_NAME_GETTER = {
	.func = get_cmd_keywords_long_name,
	.type_size = sizeof (struct tsk_cmd_keyword),
};
rab_getter *TSK_CMD_KEYWORD_LONG_NAME_GETTER = &__TSK_CMD_KEYWORD_LONG_NAME_GETTER;

// This, for now, sorts based on ons, then priority.
//
// Ons should end up at the top of the list, then priorities.
//
// Later, I suppose I can have mutiple cmp functions. --sort=prio
// --descending-sort=prio, that sort of thing. Or the user could
// provide one. This, obviously, means scripting. At some point I
// might stick a scripting language in here.
int cmp_view_node (const void *_a, const void *_b, void *arg) {
	struct tsk_items *items = arg;
	const view_node *a = _a;
	const view_node *b = _b;
	struct tsk_item *a_item = items->d + a->item_idx;
	struct tsk_item *b_item = items->d + b->item_idx;
	if ((a_item->type == IT_NOTE) && (b_item->type == IT_NOTE)) {
		if (a_item->has & (1 << NP_ON) && !(b_item->has & (1 << NP_ON)))
			return -1;
		if (!(a_item->has & (1 << NP_ON)) && b_item->has & (1 << NP_ON))
			return 1;
		if (a_item->has & (1 << NP_ON) && b_item->has & (1 << NP_ON))
			return a_item->times.on.time.ts - b_item->times.on.time.ts;
		if (a_item->has & (1 << NP_PRIORITY) &&
				!(b_item->has & (1 << NP_PRIORITY)))
			return -1;
		if (!(a_item->has & (1 << NP_PRIORITY)) &&
				b_item->has & (1 << NP_PRIORITY))
			return 1;
		if (a_item->has & (1 << NP_PRIORITY) && b_item->has & (1 << NP_PRIORITY))
			return a_item->priority - b_item->priority;
	}
	return 0;
}

static char *dup_token () {
	return strndup (T.s, T.e - T.s);
}

static int set_cmd_err_info (tsk_cmd *cmd, struct err_info *rn_err_info) {
	auto err_info = get_cmd_err_info (cmd, T.cmd_words_idx,
			TOPLEVEL_STATEMENT->cmd_words_idx);
	arr_add (&cmd->err_infos, err_info);
	if (rn_err_info)
		arr_add (&cmd->err_infos, *rn_err_info);
	return 0;
}

static void cmd_err (tsk_ctx *ctx, tsk_cmd *cmd, tsk_completions *completions,

		// This t_s and t_e is token_s and token_e. It can't be a
		// token because I've foolishly exposed the internals of this
		// lib. They're actually ignored except when we get her
		// through the match func directly.
		struct err_info *rn_err_info,
		char *fmt, ...) {
	if (completions)
		return;
	va_list ap;
	va_start (ap);
	assert (cmd);
	set_cmd_err_info (cmd, rn_err_info);
	char buf[BUFSIZ];

	// Don't wrap, by the way. We were wrapping here, which was
	// wrapping twice, which made things fucked.
	vsnprintf (buf, sizeof buf, fmt, ap);
	ctx->pc->is_error = 1;
	tsk_warn (ctx, cmd, "%s", buf);
	ctx->pc->is_error = 0;
	va_end (ap);
}

static int s2ia_str_to_relative_int_arr_wrapper (tsk_ctx *ctx, tsk_cmd *cmd,
		tsk_view *view, tsk_view last_view, tsk_completions *completions) {
	size_t n_int_list;
	int *int_list = 0;
	struct tsk_ints last_view_ints = {};
	$fori (i, last_view.n)
		arr_add (&last_view_ints, last_view.d[i].item_idx);
	struct s2ia_info s2ia_info = s2ia_str_to_relative_int_arr (&int_list, &n_int_list,
			T.s, T.e, last_view_ints.d, last_view_ints.n, 0);
	if (s2ia_info.status) {
		if (s2ia_info.status == S2IA_ERROR_MINUS_NUM_TAKEN_BELOW_ZERO ||
				s2ia_info.status == S2IA_ERROR_NUM_TOO_BIG)
			cmd_err (ctx, cmd, completions, nullptr, "%s. There are only %zu item%s in the view",
					s2ia_error_strs[s2ia_info.status], last_view.n, $plural (last_view.n));
		else
			cmd_err (ctx, cmd, completions, nullptr, "%s", s2ia_error_strs[s2ia_info.status]);
		return 1;
	}
	view->n = 0;
	$fori (i, n_int_list)
		arr_add (view, (view_node) {.item_idx = int_list[i]});
	$free_if (last_view_ints.d);
	$free_if (int_list);
	return 0;
}

static int parse_cmds_get_int (tsk_ctx *ctx, tsk_cmd *cmd, int *r, tsk_completions *completions) {
	if ($get_int (r, T.s, T.e)) {
		cmd_err (ctx, cmd, completions, nullptr, "Not an int");
		return 1;
	}
	return 0;
}

//  This function should be more generic and be in misc.c.
static int compile_regex (tsk_ctx *ctx, tsk_cmd *cmd, tsk_completions *completions) {
	char regerror_buf[0xff] = {};
	int r = 0;

	char buf[BUFSIZ];
	assert (*T.e == 0 && (T.e - T.s) - 1 < sizeof buf);
	memcpy (buf, T.s, sizeof *buf * ((T.e - T.s) + 1));
	int ret;
	if (ret = regcomp (&PREG, buf, REG_EXTENDED | REG_NOSUB)) {
		regerror (ret, &PREG, regerror_buf, 0xff);
		cmd_err (ctx, cmd, completions, nullptr, "Bad regex: %s", regerror_buf);
		r = 1;
	}
	return r;
}

// Returns 0 when there's a match. Also, it updates t if it finds a
// match.
void match_longest_keyword (tsk_ctx *, char *word, const tsk_cmd_keyword *cmd_keywords,
		rab_completion_matches *out) {

	if (*word == '=')
		return;

	// Copy the token into this buf so we can pass it to
	// rab_get_completion. You might prefer to temporarily put a 0
	// at T->e, but that won't work since we get the strings from
	// argv. Since I remember going through this before it might be
	// good actually to copy everything from argv.
	char buf[BUFSIZ] = {};

	// This, here is really bad. We're really looking to get a
	// long_name keyword, so something like "effort". Outside of this
	// function if we don't match a long name we start looking for
	// short names. That works most of the time but it will fail if
	// the user (me) has passed a short option with an argument
	// that looks like a long option. Like, say there was the long
	// keyword "friend" and the short keyword "f" and you' passed
	// "riend" as the argument to "f". We'd match the keyword
	// "friend". This would happen even if you typed "fr". That'd be
	// interpreted as "friend", not "f" with the argument "r". The
	// short option thing needs to go. There's no solution to this.
	// You could make it so we match short options first, but then
	// everything, just about, would be intepreted as a short option.
	// Since we allow you to abbreviate option names we should get rid
	// of short options. The only reason I don't want to is I'm
	// attached to writing "o1d". I'd end up having to write "on=1d"
	// or "on 1d".
	char *word_end = word;
	while (*word_end && *word_end != '=')
		word_end++;

	assert (word_end - word < BUFSIZ);
	strncpy (buf, word, word_end - word);

	int rc = rab_get_completion (buf, N_CTK, cmd_keywords,
			TSK_CMD_KEYWORD_LONG_NAME_GETTER, TSK_MAX_COMPLETION_MATCHES, out);
	if (rc)
		abort ();
}

static void get_keyword_matches (tsk_ctx *ctx, char *word,

		// This is the out param. You're expected to have alloced the
		// idxes member. The reason this is so painful is we're using
		// rab_get_completion. Using library functions is just
		// painful.
		rab_completion_matches *matches,

		// You can pass null to this and _has_attached_arg.
		bool *_is_long,

		// Doesn't get set if it's a long option we've matched.
		bool *_has_attached_arg) {

	bool is_long = false, has_attached_arg = false;

	// You can't just memset this since the idxes member points to
	// __COMPLETION_MATCHES_IDXES. This is a little flimsy. If I
	// decide to make rab_get_completion zero its matches param this
	// will fail and I'll get really confused.

	// This matches full commands -- including mods, though only
	// because long mod commands are stored with the ":" at the start,
	// eg, ":confirm".
	match_longest_keyword (ctx, word, CMD_KEYWORDS, matches);
	if (matches->count > 0) {
		is_long = true;
		has_attached_arg = !!strchr (word, '=');
		goto out;
	}

	// Check to see if it's a mod.
	if (*word == ':') {

		$fori (i, N_CTK) {
			auto info = CMD_KEYWORDS + i;
			if (info->keyword.short_name == word[1] && info->type == CT_TYPE_MOD) {
				matches->idxes[0] = i;
				matches->count = 1;
				matches->len = 1;
				if (word[matches->len])
					has_attached_arg = true;
				goto out;
			}
		}
	} else {

		$fori (i, N_CTK) {
			auto info = CMD_KEYWORDS + i;
			if (info->keyword.short_name == *word && info->type != CT_TYPE_MOD) {
				matches->idxes[0] = i;
				matches->count = 1;
				matches->len = 1;
				if (word[matches->len])
					has_attached_arg = true;
				goto out;
			}
		}
	}
out:
	if (_is_long)
		*_is_long = is_long;
	if (_has_attached_arg)
		*_has_attached_arg = has_attached_arg;

}

static void add_completion_match (tsk_completions *completions,
		tsk_cmd_keyword *keyword, bool is_arg, const char *fmt, ...) {
	if (keyword->dont_offer_as_completion)
		return;
	assert (completions->count < u_len (completions->matches));
	va_list ap;
	va_start (ap);
	char *str = 0;

	// Can't really get away without strduping -- unless I want to
	// use a 2D buffer, and such a buffer would have to be big, so
	// it'd be dumb.
	vasprintf (&str, fmt, ap);
	completions->matches[completions->count++] = (tsk_completion_match) {
		.str = str,
		.keyword = keyword,
	};

	// NOTE this is some serious laziness, here. Completions can only
	// presumably match for one type of thing, so "is_arg" is in the
	// completions struct, not the matches one. But we set matches
	// here, not the completions. So let's just keep setting is_arg
	// over and over.
	completions->is_arg = is_arg;

	va_end (ap);
}

static void get_long_keyword_completion_word (int match_len,
		const tsk_cmd_keyword *cmd_keyword, int n_completion_buf,
		char completion_buf[n_completion_buf]) {
	if (cmd_keyword->keyword.args_descr &&
			match_len == strlen (cmd_keyword->keyword.long_name))
		snprintf (completion_buf, n_completion_buf, "%s=",
				cmd_keyword->keyword.long_name);
	else
		strncpy (completion_buf, cmd_keyword->keyword.long_name, n_completion_buf);
}

// Returns 1 for error.
static void do_keyword (tsk_ctx *ctx, tsk_cmd *cmd, tsk_completions *completions) {

	rab_completion_matches matches = {.idxes = __COMPLETION_MATCHES_IDXES};
	get_keyword_matches (ctx,

			// This is turned into a "word" inside a function this
			// function calls. It'd be best to do that here, so do
			// that if you care.
			T.s, &matches, &T.is_long, &T.has_attached_arg);

	T.e = T.s + matches.len;
	if (T.is_long && *T.e == '=')
		T.has_attached_arg = true;

	// Completion stuff.
	if (should_print_completion (cmd, completions)) {
		char completion_buf[256];
		if (matches.idxes[0] == CTKS_COMPLETE_ALL) {
			assert (matches.count == 1);
			for (int i = 0; i < N_CTK; i++) {
				auto cmd_keyword = CMD_KEYWORDS + i;

				if (cmd_keyword->keyword.long_name) {
					get_long_keyword_completion_word (matches.len, cmd_keyword,
							u_len (completion_buf), completion_buf);
					add_completion_match (completions, cmd_keyword,
							false, completion_buf);
				}

				else if (cmd_keyword->keyword.short_name)
					add_completion_match (completions, cmd_keyword, false,
							(char []) {cmd_keyword->keyword.short_name, 0});
			}

		} else if (

				// If the tsk_cmd_keyword is long and you've got an attached arg
				// we're on values, ie, we're on maybe
				// sfile=<here>
				!((T.is_long && T.has_attached_arg) || *T.s == '=')) {

			// completions has a "pos_in_word" member, which we're
			// ignoring for now.

			for (int i = 0; i < matches.count; i++) {
				auto cmd_keyword = CMD_KEYWORDS + matches.idxes[i];
				if (T.is_long && cmd_keyword->keyword.long_name) {
					get_long_keyword_completion_word (matches.len, cmd_keyword,
							u_len (completion_buf), completion_buf);
					add_completion_match (completions, cmd_keyword,
							false, completion_buf);
				}

				// So we do actually match short matches. This means
				// that if you type "note /<tab>" Zsh will insert the
				// space even though there can be only one match. This
				// surprised me and I started making it so short
				// matches don't get echoed as matches. The problem is
				// doing that will ring the terminal bell, which isn't
				// a good look. Just know that
				// 		note / this-is-the-search
				// is valid in the shell. Obviously.
				// "this-is-the-search" is the argument to "/".
				else if (cmd_keyword->keyword.short_name)
					add_completion_match (completions, cmd_keyword, false,
							(char []) {cmd_keyword->keyword.short_name, 0});
			}
		}
	}

	T.kind = matches.idxes[0];

	bool is_error = false;
	if (matches.count == 0) {
		cmd_err (ctx, cmd, completions, nullptr, "No keyword matches \"%s\"", T.s);
		is_error = true;
	} else if (matches.count > 1) {
		cmd_err (ctx, cmd, completions, nullptr, "Too many keywords match \"%s\"", T.s);
		is_error = true;
	}

	if (is_error) {
		while (*T.e != '\0')
			T.e++;
		T.kind = CTK_ERROR;
		return;
	}

}

static void get_token (tsk_ctx *ctx, tsk_cmd *cmd, enum cmd_token_kind fake_token,
		tsk_completions *completions) {

	PREV_T = T;
	if (ctx->opts->flags & TSK_FLAG_IMPLICIT_LIST && fake_token != -1) {
		T.kind = fake_token;
		return;
	}

	T.has_attached_arg = 0;
	T.kind = CTK_NOT_SET;
	T.is_long = 0;

	// Advance to the next word in the words array.
	if (!*T.e) {
		T.cmd_words_idx++;
		if (T.cmd_words_idx == cmd->words.n) {
			T.kind = CTK_END;
			return;
		}
		T.s = cmd->words.d[T.cmd_words_idx];
	} else

		// Advance to the next token within this word, eg the equals
		// in "this=that".
		T.s = T.e;

	T.e = T.s + 1;

	switch (*T.s) {
		case ':':
			if (*T.e == 0) {
				T.kind = CTK_PROP;
				break;
			}
			[[fallthrough]];
		default:
			do_keyword (ctx, cmd, completions);
			break;
	}
}

static int match_pattern (char *str) {
	int r = 0;

	if (0 == regexec (&PREG, str, 0, 0, 0))
		r = 1;
	return r;
}

static int match_body (tsk_ctx *, tsk_cmd *, struct tsk_item *item, char *, char *,
		tsk_completions *, match_arg *) {
	return match_pattern (item->body);
}

static int match_name (tsk_ctx *, tsk_cmd *, struct tsk_item *item, char *, char *,
		tsk_completions *, match_arg *) {
	if (!item->name)
		return 0;
	return match_pattern (item->name);
}

static bool is_prop (enum item_type type) {
	if (type == IT_ANNOTATION
			|| type == IT_ATTACHMENT
			|| type == IT_LINK
			|| type == IT_TAG) {
		return 1;
	}
	return 0;
}

static bool is_normal_item (enum item_type type) {
	if (type == IT_AUTOCMD
			|| type == IT_NOTE
			|| type == IT_ANNOTATION
			|| type == IT_ATTACHMENT
			|| type == IT_LINK
			|| type == IT_TAG) {
		return 1;
	}
	return 0;
}

// This function's nearly identical to get_item_flag_list
// You could make it more generic by using ints and do an if for
// == or &.
static int match_item_type (tsk_ctx *ctx, tsk_cmd *cmd, struct tsk_item *item, char *s, char *e,
		tsk_completions *completions, match_arg *args) {

	auto statement_type = args[0]._statement_type;

	int r = -1;
	char err_buf[TSK_MAX_ERR_BUF];
	int wanted[N_IT];
	size_t n_wanted = get_comma_list_indexes (s, e, N_IT, ITEM_TYPE_KEYWORDS,
			TSK_KEYWORD_LONG_NAME_GETTER, err_buf, N_IT, wanted);

	if (n_wanted == -1) {
		cmd_err (ctx, cmd, completions, nullptr, "Couldn't match item type: %s", err_buf);
		goto out;
	}

	// If we get a wanted.n of 0 there's a bug in
	// get_comma_list_indexes; see its comments.
	assert (n_wanted);

	// I'll put in this array. Right now there not matching multiple
	// types. You can't do tag=t,a, etc. But I'll probably want to.
	// So, n_wanted is for now 1
	u_each (_, n_wanted, wanted) {
		if (statement_type == STATEMENT_TYPE_PROP && !is_prop (*_)) {
			cmd_err (ctx, cmd, completions, nullptr, "Props can't be type %s",
					ITEM_TYPE_KEYWORDS[*_].long_name);
			goto out;

		} else if (statement_type == STATEMENT_TYPE_NOTE && !is_normal_item (*_)) {
			cmd_err (ctx, cmd, completions, nullptr, "Items can't be type %s",
					ITEM_TYPE_KEYWORDS[*_].long_name);
			goto out;
		}
	}

	r = 0;
	if (lfind (&item->type, wanted, &n_wanted, sizeof item->type, ncmp_ints))
		r = 1;
out:
	return r;
}

static int match_ts_constant (tsk_ctx *ctx, tsk_cmd *cmd, struct tsk_item *item, char *s,
		char *, tsk_completions *completions, match_arg *args) {

	enum time_id time_id = args[0]._time_id;
	enum prop_id prop_id = time_id_to_prop (time_id);
	if (
			// time_id_to_prop returns -1 for creation and now. can't
			// match now because there's no keyword for, but you can
			// match creation, and so in both cases we can continue.
			prop_id != -1 &&

			!(item->has & (1 << prop_id)))
		return 0;
	struct ts_ts_range range;
	auto ts_constant = &item->times.d[time_id];

	switch (item->times.d[time_id].type) {

	case TS_CONSTANT_TYPE_TIME:
		range = ts_maths_ts_range_from_str (s, 0, &TIMES.now.time, item->times.d, N_TIMES,
				0);
		if (ts_errno) {
			cmd_err (ctx, cmd, completions, nullptr, "Bad time range for %s: %s",
					TIMES.d[time_id].name, ts_strerror (ts_errno));
			return -1;
		}
		if (ts_constant->time.ts >= range.s
				&& item->times.d[time_id].time.ts < range.e)
			return 1;
		break;

	case TS_CONSTANT_TYPE_DURATION:
		range = ts_maths_ts_range_from_str (s, 0, &TIMES.now.time, item->times.d, N_TIMES,
				0);
		range.e -= TIMES.now.time.ts;
		range.s -= TIMES.now.time.ts;
		if (range.s - range.e > ts_constant->dur.secs)
			return 1;
		break;
	case TS_CONSTANT_TYPE_TIME_RANGE:
		range = ts_maths_ts_range_from_str (s, 0, &TIMES.now.time, item->times.d, N_TIMES,
				0);

		// If your range overlaps the time's range it's a match. I
		// think it makes sense. If you were to look up excepts (the
		// only range times right now) you'd want to know if an except
		// was happeing in this time range.
		if (range.s > item->times.d[time_id].time_range.s.ts ||
				range.s < item->times.d[time_id].time_range.e.ts)
			return 1;
		break;

	}
	return 0;
}

static int match_has (tsk_ctx *, tsk_cmd *, struct tsk_item *item, char *, char *,
		tsk_completions *, match_arg *args) {

	// No point returning 0 if, eg, the item is an autocmd and the
	// user's matching a note prop since autocmds can't have those.
	return item->has & (1 << args[0]._prop_id);
}

static int match_props (tsk_ctx *, tsk_cmd *, struct tsk_item *item, char *, char *,
		tsk_completions *, match_arg *args) {

	int r = 0;

	// This function matches the props in notes, and also "froms",
	// which are in the items, hence we get props by doing the
	// void * offset trick. Might as well make use of this
	// function. Also, in the future there might be more non-note
	// props. "froms" are items, you see. Though to be honest a bool
	// would have been fine. You could have used item_type, too.
	struct tsk_items *props = *((struct tsk_items **) ((void *) item + args[1]._size_t));
	enum item_type type = args[0]._item_type;

	if (!props)
		return 0;

	if (item->type != IT_NOTE)
		goto out; // r is already 0

	arr_each (props, _) {
		if (_->type == type)
			if (!regexec (&PREG, _->body, 0, 0, 0)) {
				r = 1;
				break;
			}
	}
out:
	return r;
}

static struct ts_ts_range get_event_range (struct tsk_item *item,
		int n_intervals, struct ts_time on) {

	auto times = &item->times;

	auto adjusted_on = n_intervals == 0 && item->has & (1 << NP_POSTPONE)
		? times->postpone.time
		: on;

	struct ts_ts_range r = {
		.s = n_intervals == 0 && item->has & (1 << NP_REMINDER)
			? adjusted_on.ts - ts_secs_from_dur (times->reminder.dur,
					&adjusted_on)
			: adjusted_on.ts,
	};
	if (item->has & (1 << NP_ENDING))

		// Subtract ending from original on to get this iteration's
		// on. And then add it to on, not adjusted on so it doesn't
		// move up if you've done a reminder. Ditto postpone? Maybe
		// ending should move in that case.
		r.e = on.ts + times->ending.time.ts - times->on.time.ts;
	else {
		if (item->has & (1 << NP_INTERVAL))
			r.e = r.s + ts_secs_from_dur (times->interval.dur, &adjusted_on);
		else

			// This is dumb code, obviously. I'm making the end
			// "infinity" in a lazy way. And I'm half-assing figuring
			// out time_t's size.
			r.e = sizeof (time_t) == 4 ? INT32_MAX
				: sizeof (time_t) == 8 ? INT64_MAX
				:                        TS_SECS_FROM_YEARS (100);
	}
	return r;
}

bool is_within_except (struct tsk_item *item, time_t stop,
		struct ts_ts_range *event_range) {
	auto times = &item->times;

	// This is pretty inefficient. Not that it matters.
	for (struct ts_time_range except = times->except.time_range;
			except.s.ts < stop;

			// It shouldn't matter if item except-interval doesn't
			// have an except-interval.
			except = ts_range_plus_dur (&except, &times->except_interval.dur)) {

		if (event_range->s > except.s.ts && event_range->e < except.e.ts)
			return 1;
		if (!(item->has & (1 << NP_EXCEPT_INTERVAL)))
			break;
	}
	return 0;
}

int tsk_n_ons_within_time_period (struct tsk_item *item,
		struct ts_ts_range range) {

	if (item->type != IT_NOTE || !(item->has & (1 << NP_ON)))
		return 0;

	auto times = &item->times;

	// The time to stop at in the loop below. If a item doesn't have
	// interval, stop isn't set and the loop is breaked.
	time_t stop = ({
		if (item->has & (1 << NP_INTERVAL) && item->has & (1 << NP_STOP_REPEATING))
			stop = u_min (times->stop_repeating.time.ts, range.e);
		else
			stop = range.e;
		stop;
	});


	// Need seperate variables for interval and except_interval
	// because they change now, because I'm considering months.
	// Shouldn't need to calculate interval.total_secs here because it
	// should be done in read-notefile and change doesn't do anything
	// with lists, right?
	/* struct ts_time_range except = times.except.time_range; */
	/* struct ts_time ending = times.ending.time; */

	int n_matches = 0;
	int n_intervals = 0;

	struct ts_time on = times->on.time;
	do {

		// Don't try working this out in advance. Its range can be
		// different on the first iteration.
		auto event_range = get_event_range (item, n_intervals, on);
		bool matches = 0;
		if (range.s <= event_range.e && range.e >= event_range.s)  {

			matches = 1;
		// 2022-10-25T02:12:48+01:00: commented out this line
		// basically removes "except" from the program. I really don't
		// care about except. One day we'd want something really
		// sophisticated. Really the thing to do would be to study the
		// systemd source.
#if 0
			if (item->has & NP_EXCEPT && is_within_except (item, stop, &event_range))
				matches = 0;
#endif
		}

		if (matches)
			n_matches++;

		if (event_range.s >= stop)
			break;

		on = ts_time_from_ts (on.ts + ts_secs_from_dur (times->interval.dur, &on));

		// If doesn't have interval, just break.
		if (!(item->has & (1 << NP_INTERVAL)))
			break;
		n_intervals++;
	} while (1);

	return n_matches;
}

static int match_on (tsk_ctx *ctx, tsk_cmd *cmd, struct tsk_item *item, char *s, char *,
		tsk_completions *completions, match_arg *args) {

	if (item->type != IT_NOTE || !(item->has & (1 << NP_ON)))
		return 0;

	// Leave this as a pointer. It gets passed to ts_ functions as a
	// pointer.
	struct ts_time *now = (struct ts_time *) args[0]._ts_time_p;
	ts_maths_ts_range_from_str (s, 0, now, TIMES.d, N_TIMES, 1);
	if (ts_errno) {
		cmd_err (ctx, cmd, completions, nullptr, "Bad time str: %s", ts_strerror (ts_errno));
		return -1;
	}

	struct ts_ts_range range;
#if 0
	if (!note->n_intervals.n)
		note->n_intervals = tsk_strs_create (8);
#endif

	// This has to be calculated now, and not before before each note
	// has its own .times.
	range = ts_maths_ts_range_from_str (s, 0, now, item->times.d, N_TIMES, 0);

	if (ts_errno) {
		cmd_err (ctx, cmd, completions, nullptr, "Bad on time range: %s",
				ts_strerror (ts_errno));
		return -1;
	}

	return tsk_n_ons_within_time_period (item, range);
}

static int get_value_token (tsk_ctx *ctx, tsk_cmd *cmd, tsk_completions *completions) {

	/* fprintf (stderr, "Token = \"%s\"\n", T.s); */
	// If the value is attached
	if (*T.e) {
		assert (*T.e == '=' || T.e - T.s == 1 || *T.s == ':' && T.e - T.s == 2);
		T.s = T.e;
		T.e = strchr (T.e, 0);
	} else {
		T.cmd_words_idx++;
		if (T.cmd_words_idx == cmd->words.n) {
			cmd_err (ctx, cmd, completions, nullptr, "Missing arg");
			return 1;
		}

		T.s = cmd->words.d[T.cmd_words_idx];
		T.e = strchr (T.s, 0);
	}

	/* fprintf (stderr, "Now token = \"%s\"\n", T.s); */
	// If we've encountered the special "complete" token we'll set the
	// token's kind. We won't bother when it's not.
	if (*T.s == 0x1)
		T.kind = CTKS_COMPLETE_ALL;

	return 0;
}

static int get_optional_arg (tsk_ctx *ctx, tsk_cmd *cmd, char **pr,
		tsk_completions *completions) {
	if (!T.has_attached_arg)
		return 0;

	if (T.is_long) {
		get_token (ctx, cmd, -1, completions);
		assert (T.kind == CTK_EQUALS);
	}
	if (get_value_token (ctx, cmd, completions))
		return 1;
	if (pr)
		*pr = dup_token ();
	return 0;
}

static int get_non_optional_arg (tsk_ctx *ctx, tsk_cmd *cmd, char **pr, tsk_completions *completions) {
	if (T.is_long && T.has_attached_arg) {
		get_token (ctx, cmd, -1, completions);
		assert (T.kind == CTK_EQUALS);
	}
	if (get_value_token (ctx, cmd, completions))
		return 1;
	if (T.kind == CTK_END) {
		cmd_err (ctx, cmd, completions, nullptr, "Missing arg");
		return 1;
	}
	if (pr)
		*pr = dup_token ();
	return 0;
}

// Returns -1 for a proper failure. 1 means that it's an optional arg
// and you didn't get one. I don't like this solution. You could use
// the has_attached_arg to figure out if you got one but whatever.
static int get_and_dup_arg_if_needed (tsk_ctx *ctx, tsk_cmd *cmd, char **pr,
		tsk_completions *completions) {
	auto keyword = &CMD_KEYWORDS[T.kind].keyword;

	// If the action doesn't take an arg.
	if (!keyword->args_descr)
		return 0;

	int r;

	// Is an optional arg. Looks like we try to get the value and
	// if move we accept that.
	if (keyword->args_are_optional)
		r = get_optional_arg (ctx, cmd, pr, completions);
	else
		r = get_non_optional_arg (ctx, cmd, pr, completions);
	return r;
}

// Returns 1 for err, for no err. It doesn't consider if it
// matched or not.
static int filter_loop (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		match_arg *args,
		enum filter_loop_flags flags,

		// We can't use completion_fn to mean we're completions because
		// not all filters have completions functions. Recall that
		// "completions" disables error messages in cmd_err.
		tsk_completions *completions,
		void (*completion_fn) (tsk_ctx *, tsk_cmd *cmd, tsk_completions *),
		int (*match) (tsk_ctx *, tsk_cmd *, struct tsk_item *, char *, char *,
			tsk_completions *, match_arg *)) {

	int r = 0;
	if (get_and_dup_arg_if_needed (ctx, cmd, 0, completions))
		$get_out (r);

	// This is your completions function, and it's meant to just go by
	// what the user (me) has typed. However, you could put this at
	// the end of item loop and match against what the user has typed
	// and what they've matched. So, in "note n-2.. has=<tab>", you
	// could produce only the props that the matching notes actually
	// ahve. I really don't want to do that; it's a lot of work and it
	// might be confusing. The user (me) would press tab expecting to
	// match properties, but they wouldn't find half them since most
	// notes don't have many properties. On the other hand it would be
	// a very quick way to find out what stuff notes have. Very
	// dynamic. NOTE: we'll probably do this.
	if (completions && completion_fn)
		completion_fn (ctx, cmd, completions);
	if ((flags & FILTER_LOOP_COMPILE_REGEX) && compile_regex (ctx, cmd, completions))
		$get_out (r);
	pr->n = 0;
	int ret;
	arr_each (&ctx->items, _) {

		if ((ret = match (ctx, cmd, _, T.s, T.e, completions, args)) >= 1)
			arr_add (pr, (view_node) {.item_idx = _ - ctx->items.d});

			// This else if might not be the greatest. I know that
			// cmd_err sets t.kind to CTK_ERROR.
		else if (ret == -1)
			$get_out (r);
	}
out:
	if (flags & FILTER_LOOP_COMPILE_REGEX) {
		regfree (&PREG);
		memset (&PREG, 0, sizeof PREG);
	}
	return r;
}

static void simple_complete (tsk_cmd *cmd, int n_objs, void *objs, size_t type_size,
		bool is_arg, char **(*get_fn) (const void *, void *),
		tsk_completions *completions) {
	if (T.cmd_words_idx != cmd->words.n - 1)
		return;
	struct rab_getter getter = {
		.func = get_fn,
		.type_size = type_size,
	};
	if (T.kind == CTKS_COMPLETE_ALL) {
		for (int i = 0; i < n_objs; i++) {
			void *obj = objs + i * type_size;
			char *str = get_fn ? *get_fn (obj, nullptr) : *(char **) obj;

			auto info = CMD_KEYWORDS + i;
			if (PREV_T.is_long && PREV_T.has_attached_arg)
				add_completion_match (completions, info, is_arg, "%.*s=%s",
						(int) (PREV_T.e - PREV_T.s), PREV_T.s, str);
			else
				add_completion_match (completions, info, is_arg, "%s", str);
		}
	} else {
		rab_completion_matches matches = {.idxes = __COMPLETION_MATCHES_IDXES};
		int rc = rab_get_completion (T.s, n_objs, objs, get_fn ? &getter : nullptr,
				TSK_MAX_COMPLETION_MATCHES, &matches);
		if (rc)
			abort ();
		for (int i = 0; i < matches.count; i++) {
			void *obj = objs + matches.idxes[i] * type_size;
			char *str = get_fn ? *get_fn (obj, nullptr) : *(char **) obj;

			auto info = CMD_KEYWORDS + i;
			if (PREV_T.is_long && PREV_T.has_attached_arg)
				add_completion_match (completions, info, is_arg, "%.*s=%s",
						(int) (PREV_T.e - PREV_T.s), PREV_T.s, str);
			else
				add_completion_match (completions, info, is_arg, "%s", str);
		}
	}
}

static void complete_keywords (tsk_ctx *, tsk_cmd *cmd, tsk_completions *completions) {
	simple_complete (cmd, N_CTK, CMD_KEYWORDS, sizeof *CMD_KEYWORDS, true,
			TSK_CMD_KEYWORD_LONG_NAME_GETTER->func, completions);
}

static void complete_future_time_str (tsk_ctx *, tsk_cmd *cmd,
		tsk_completions *completions) {

	// If you wanted you could add a couple of non-relative ones.
	// You'd get the time now and calculate some.
	char *strs[] = {"1h", "4h", "8h", "1d", "1w", "1M", "1Y"};
	simple_complete (cmd, u_len (strs), strs, sizeof *strs, true, nullptr, completions);
}

static void complete_ts_dur (tsk_ctx *, tsk_cmd *cmd, tsk_completions *completions) {

	char *strs[] = {"1h", "1d", "1w", "1M", "1Y"};
	simple_complete (cmd, u_len (strs), strs, sizeof *strs, true, nullptr, completions);
}

static void complete_reminder (tsk_ctx *, tsk_cmd *cmd, tsk_completions *completions) {

	char *strs[] = {"$on - 1h", "$on - 2h", "$on -3h", "$on - 1d"};
	simple_complete (cmd, u_len (strs), strs, sizeof *strs, true, nullptr, completions);
}

static void complete_negative_ts_time (tsk_ctx *, tsk_cmd *cmd,
		tsk_completions *completions) {

	// If you wanted you could add a couple of non-relative ones.
	// You'd get the time now and calculate some.
	char *strs[] = {"-1h", "-4h", "-8h", "-1d", "-1w", "-1M", "-1Y"};
	simple_complete (cmd, u_len (strs), strs, sizeof *strs, true, nullptr, completions);
}

static void complete_prop (tsk_ctx *, tsk_cmd *cmd, tsk_completions *completions) {
	simple_complete (cmd, N_PROPS, PROP_KEYWORDS, sizeof *PROP_KEYWORDS,
			true, keyword_full_name_getter_fn, completions);
}

static void complete_effort_etc_action (tsk_ctx *, tsk_cmd *cmd,
		tsk_completions *completions) {
	char *numbers[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
	simple_complete (cmd, u_len (numbers), numbers, sizeof *numbers, true, nullptr,
			completions);
}

static void complete_effort_etc_filter (tsk_ctx *, tsk_cmd *cmd,
		tsk_completions *completions) {

	char *strs[] = { "0..5", "5..10"};
	simple_complete (cmd, u_len (strs), strs, sizeof *strs, true, nullptr,
			completions);
}

static void complete_item_type (tsk_ctx *, tsk_cmd *cmd, tsk_completions *completions) {
	simple_complete (cmd, N_IT, ITEM_TYPE_KEYWORDS, sizeof *ITEM_TYPE_KEYWORDS,
			true, keyword_full_name_getter_fn, completions);
}

static void complete_ts_range (tsk_ctx *, tsk_cmd *cmd, tsk_completions *completions) {
	char *strs[] = {"0d..1d", "0d..1w", "0d..1M", "Od..1Y"};
	simple_complete (cmd, u_len (strs), strs, sizeof *strs, true,
			keyword_full_name_getter_fn, completions);
}

static void complete_creation_range (tsk_ctx *, tsk_cmd *cmd,
		tsk_completions *completions) {
	char *strs[] = {
		"-1w..0d",
		"-1M..0d",
		"-1y..0d",
		"-2y..0d",
		"-3y..0d",
		"-4y..0d",
		"-5y..0d",
		"-6y..0d",
	};
	simple_complete (cmd, u_len (strs), strs, sizeof *strs, true,
			keyword_full_name_getter_fn, completions);
}


static int eval_note_prop (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *view,
		enum statement_type statement_type, enum item_type type,
		size_t props_offset, tsk_completions *completions) {

	int r = 1;
	enum filter_loop_flags flags = FILTER_LOOP_COMPILE_REGEX;

	if (statement_type == STATEMENT_TYPE_NOTE)
		r = filter_loop (ctx, cmd, view,
				(match_arg []) {
					{type},
					{props_offset}
				},
				flags, completions, complete_prop, match_props);
	else
		cmd_err (ctx, cmd, completions, nullptr, "\
Array-type props can't have %ss; maybe use \"/your-tag-or-whatever\" instead",
ITEM_TYPE_KEYWORDS[type].long_name);
	return r;
}

// For effort and priority.
static int match_int (tsk_ctx *ctx, tsk_cmd *cmd, struct tsk_item *item, char *s, char *e,
		tsk_completions *completions, match_arg *args) {

	// So it doesn't fire on autocmds. This is obviously fine for
	// now. It's called "match_int", but it's just effort and
	// priority, which are part of note.
	if (item->type != IT_NOTE)
		return 0;
	struct tsk_ints arr = {};
	auto s2ia_info = s2ia_str_to_int_arr (&arr.d, &arr.n, s, e, 10,
			S2IA_FLAG_ALLOW_OUT_OF_RANGE);
	size_t member_offset = args[0]._size_t;
	if (s2ia_info.status) {
		cmd_err (ctx, cmd, completions, nullptr, "Problem with list at index whtaever: %s",
				s2ia_error_strs[s2ia_info.status]);
		return -1;
	}
	char *base = (char *) item;
	int *val = (int *) (base + member_offset);
	if (*val != -1 && arr_find (&arr, val, ncmp_ints))
		return 1;
	free (arr.d);
	return 0;
}

static int get_prev_view (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *view, tsk_completions *completions) {

	if (get_and_dup_arg_if_needed (ctx, cmd, 0, completions))
		return 1;
	int i;

	if (parse_cmds_get_int (ctx, cmd, &i, completions))
		return 1;

	if (i >= cmd->statements.n) {
		cmd_err (ctx, cmd, completions, nullptr, "\
There are only %zu previous statements. Statements are zero-indexed",
				cmd->statements.n);
		return 1;
	}
	view_copy (view, (arr_last (&cmd->statements)->view));

	return 0;
}

static int match_comma_list_against_has (tsk_ctx *ctx, tsk_cmd *cmd, struct tsk_item *item, char *s,
		char *e, tsk_completions *completions, match_arg *) {

	char err_buf[TSK_MAX_ERR_BUF];
	int want;
	int rc = get_bitfield_from_comma_list (s, e, N_PROPS, PROP_KEYWORDS,
			TSK_KEYWORD_LONG_NAME_GETTER, err_buf, &want);
	if (rc) {
		cmd_err (ctx, cmd, completions, nullptr, "\"has\": %s", err_buf);
		return -1;
	}
	return (item->has & want) == want;
}

static int match_notefile (tsk_ctx *ctx, tsk_cmd *, struct tsk_item *item, char *, char *,
		tsk_completions *, match_arg *args) {
	char *notefile_pattern = *((char **)  args);
	arr_each (&item->notefile_idxes, _) {
		auto notefile = &ctx->notefiles.d[*_];
		if (!(notefile->flags & TSK_FILE_FLAG_IS_SHORTCUT) &&
				!fnmatch (notefile_pattern, ctx->notefiles.d[*_].path, FNM_PATHNAME | FNM_EXTMATCH)) {
			return 1;
		}
	}
	return 0;
}

static int match_shortcut_notefile (tsk_ctx *ctx, tsk_cmd *, struct tsk_item *item, char *, char *,
		tsk_completions *, match_arg *) {

	arr_each (&item->notefile_idxes, _) {
		auto notefile = &ctx->notefiles.d[*_];

		if ((notefile->flags & TSK_FILE_FLAG_IS_SHORTCUT)) {
			char *basename = strrchr (ctx->notefiles.d[*_].path, '/');
			assert (basename);
			if (!regexec (&PREG, basename, 0, 0, 0))
				return 1;
		}
	}
	return 0;
}

static int load_notefile (tsk_ctx *ctx, tsk_cmd *cmd, char *path_or_basename,
		enum notefile_flags notefile_flags, bool add_to_notefiles_referenced,
		bool create, tsk_completions *completions) {

	struct rn_err_info rn_err_info;
	int r = -1;

	if (add_to_notefiles_referenced)
		assert (TOPLEVEL_STATEMENT);

	// We update the notefiles referenced here. Maybe sfile, etc would
	// be better, but here we only have to do it once. We don't bother
	// checking to see that we've already got it because doing that in
	// C is so such a pain -- loop, and it doesn't matter here.
	int notefile_referenced = -1;

	auto notefile = create_notefile_struct (ctx, path_or_basename, notefile_flags);
	auto notefile_match = arr_find (&ctx->notefiles, &notefile,
			cmp_notefiles_by_path);
	if (notefile_match) {
		notefile_referenced = notefile_match - ctx->notefiles.d;
		r = 0;
		goto out;
	}
	if (n_fs_fexists (notefile.path)) {
		r = read_notefile (ctx, &notefile, 0, &rn_err_info, &ctx->items);
		if (r) {
			cmd_err (ctx, cmd, completions, rn_err_info.err_reading_file ? 0 :
					&rn_err_info.err_info,
					"%s", rn_err_info.msg);
			goto out;
		}
		notefile_referenced = ctx->notefiles.n - 1;

	} else if (!create) {
		cmd_err (ctx, cmd, completions, nullptr, "%s\"%s\" doesn't exist",
				notefile.flags & TSK_FILE_FLAG_IS_SHORTCUT ? "Shortcut notefile " : "",
				notefile.path);
		goto out;
	}
	else {
		arr_add (&ctx->notefiles, notefile);
		notefile_referenced = ctx->notefiles.n - 1;
		r = 0;
	}


out:
	if (r || notefile_match)
		free_notefile (&notefile);
	if (add_to_notefiles_referenced)
		arr_add (&TOPLEVEL_STATEMENT->notefiles_referenced, notefile_referenced);
	return r;
}

static void complete_sfiles (tsk_ctx *ctx, tsk_cmd *cmd, tsk_completions *completions) {
	simple_complete (cmd, ctx->sfiles.n, ctx->sfiles.d, sizeof *ctx->sfiles.d, true, nullptr,
			completions);
}

static int eval_sfile_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *view,
		enum file_filter_mode file_filter_mode, tsk_completions *completions) {

	// This function should really just match notefiles and then call
	// eval_file_filter. But I can't be bothered.

	if (file_filter_mode == FFM_MATCH) {
		return filter_loop (ctx, cmd, view, 0, FILTER_LOOP_COMPILE_REGEX,
				completions, complete_sfiles, match_shortcut_notefile);
	}

	int r = 1;
	struct cmd_token saved_t = T;
	struct cmd_token saved_prev_t = PREV_T;
	if (!ctx->sfiles.n) {
		cmd_err (ctx, cmd, completions, nullptr, "You have no shortcut notefiles");
		return r;
	}
	if (get_and_dup_arg_if_needed (ctx, cmd, 0, completions))
		return r;

	bool create = file_filter_mode = FFM_CREATE;
	struct tsk_ints matches;

	if (completions)
		complete_sfiles (ctx, cmd, completions);

	char err_buf[TSK_MAX_ERR_BUF];

	// Use completions when you can be bothered. There's also the
	// possibility of globs, and globs and regexes will both work now
	// and not be confusing because we have sfilecreate.
	int rc = regmatch_arr (&matches, T.s, ctx->sfiles.n, ctx->sfiles.d, err_buf);
	if (rc) {
		cmd_err (ctx, cmd, completions, nullptr, "%s", err_buf);
		return r;
	}

	if (!matches.n) {
		if (create)  {
			if (load_notefile (ctx, cmd, T.s, TSK_FILE_FLAG_IS_SHORTCUT | TSK_FILE_FLAG_WAS_LOADED_BY_LIST,
						true, create, completions))
				goto out;

			// When creating continue to the filter_loop even though
			// we know that only this note can have that notefile.
			// Just because it's safer.
		} else {
			cmd_err (ctx, cmd, completions, nullptr, "\
No sfiles match \"%s\". Use \"sfilecreate\" if you want to create that notefile", T.s);
			goto out;
		}
	} else {
		// If so, and it's not already in the notefiles, add it to the
		// notefile list and load the notefile's items.
		$fori (i, matches.n) {
			char **sfile = &ctx->sfiles.d[matches.d[i]];

			if (load_notefile (ctx, cmd, *sfile, TSK_FILE_FLAG_IS_SHORTCUT | TSK_FILE_FLAG_WAS_LOADED_BY_LIST,
						true, create, completions))
				goto out;
		}
	}

	T = saved_t;
	PREV_T = saved_prev_t;
	r = filter_loop (ctx, cmd, view, 0, FILTER_LOOP_COMPILE_REGEX,

			completions,

			// Already have dealt with completions.
			nullptr,
			match_shortcut_notefile);
out:
	if (matches.d)
		free (matches.d);
	return r;
}

static void complete_str (tsk_ctx *, tsk_cmd *cmd, tsk_completions *completions) {

	// Doing this works in the tui but not in Zsh. Zsh puts a
	// backslash in front of it. I've solved this issue by just not
	// bothering with this completion in the CLI.
	char *strs[] = {"\""};
	simple_complete (cmd, u_len (strs), strs, sizeof *strs, true, nullptr, completions);
}

static void complete_paths (tsk_ctx *, tsk_cmd *cmd, tsk_completions *completions) {
	char pattern[PATH_MAX];
	if (T.kind == CTKS_COMPLETE_ALL)
		snprintf (pattern, sizeof pattern, "*");
	else
		snprintf (pattern, sizeof pattern, "%s*", T.s);
	glob_t glb;

	// We don't care about glob's return value since this is to be
	// used by Zsh ultimately. Though I suppose we might want to do
	// something about it in the TUI.
	glob (pattern,

			// There's a GLOB_MARK function that adds slashes to the
			// end of directories. It's a good idea because it's what
			// Zsh does by default. But me doing it doesn't work like
			// it does in Zsh. In Zsh if you type a slash after one
			// of those slashes have been added you'll just overwrite
			// the slash. Here, we'll add it. I know that Zsh has ways
			// to configure that stuff. But I'm not going to think
			// about it now.
			0, nullptr, &glb);

	// It's a bit pointless to go through simple_complete since we
	// already have all the matches but I'm doing it any way.

	if (glb.gl_pathc)
		simple_complete (cmd, glb.gl_pathc, glb.gl_pathv, sizeof *glb.gl_pathv, true,
				nullptr, completions);
}

// NOTE: it occurs to me this is dodgy design. We'll see when I've
// done it. This does two things: loads notefiles if they're not
// already loaded, and adds them to your list.
//
// It deals with both shortcut and non-shortcut notefiles. Seems
// reasonable.

static int eval_file_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *view,
		enum file_filter_mode file_filter_mode, tsk_completions *completions) {

	char *notefile_pattern;
	if (file_filter_mode == FFM_MATCH)
		return filter_loop (ctx, cmd, view, (match_arg []) {{._str = T.s}},
			FILTER_LOOP_COMPILE_REGEX, completions, complete_paths, match_notefile);

	auto saved_t = T;
	auto saved_prev_t = PREV_T;

	if (get_and_dup_arg_if_needed (ctx, cmd, 0, completions))
		return 1;

	// It's easier to do the completions here rather than in
	// filter_loop as we do elsewhere.
	if (completions)
		complete_paths (ctx, cmd, completions);

	if (n_fs_fexists (T.s)) {

		// We return 1 only if there's a "real" error -- like, stat
		// fails. Trying to read a non-existent file isn't a failure,
		// because if it was, you couldn't do "note f /tmp/new-file
		// ,'hi'".
		int rc = load_notefile (ctx, cmd, T.s, TSK_FILE_FLAG_WAS_LOADED_BY_LIST,
				true, file_filter_mode, completions);
		if (rc < 0)
			return 1;
	} else if (file_filter_mode == FFM_CREATE) {

		// Note that we add the notefile even if the file doesn't
		// exist. This is so we don't fail if you try to edit a
		// non-existent notefile. Yeah, it doesn't feel right.
		arr_add (&ctx->notefiles, create_notefile_struct (ctx, T.s,
					TSK_FILE_FLAG_WAS_LOADED_BY_LIST));
		arr_add (&TOPLEVEL_STATEMENT->notefiles_referenced, ctx->notefiles.n - 1);
		return 0;
	} else  {
		cmd_err (ctx, cmd, completions, nullptr, "\
				No files match \"%s\". Use \"filecreate\" if you want to create that notefile", T.s);
		return 1;
	}

	notefile_pattern = T.s;
	T = saved_t;
	PREV_T = saved_prev_t;
	int r = filter_loop (ctx, cmd, view, (match_arg []) {{._str = notefile_pattern}},
			FILTER_LOOP_COMPILE_REGEX,

			completions,

			// Already have dealt with completions.
			nullptr, match_notefile);
	return r;
}

// Add the items whose notefiles were referenced by this statement to
// view. Ie, in "note swhatever \; shello-there", the
// "hello-there" ones would get added. 2023-06-29T22:15:33+01:00:
// I don't understand at all what I meant there. Surely the swhatever
// one would get added, too?
static int add_all_items_whose_notefiles_were_referenced_by_this_statement_to_view (tsk_ctx *ctx,
		tsk_view *view) {
	auto notefiles_referenced = &TOPLEVEL_STATEMENT->notefiles_referenced;

	arr_each (&ctx->items, item) {
		$fori (j, notefiles_referenced->n) {
			if (item->notefile_idxes.d[0] == notefiles_referenced->d[j]) {
				arr_add (view, (view_node) {.item_idx = item - ctx->items.d});
				break;
			}
		}
	}
	return 0;
}

static void insert_word_into_cmd_words (tsk_cmd *cmd, char *word) {

	int token_len = T.e - T.s;

	// We insert a token into the stream. I don't love doing this but
	// I'm committed to it. Inserting the token means we get logical
	// error messages -- even if it might surprise the user (me) to
	// see tokens appear in their error messages. I'm committed to
	// this path.
	arr_insert (&cmd->words, TOPLEVEL_STATEMENT->cmd_words_idx, strdup (word));

	// These cmd_words_idx and cmd_words_end things, they're the
	// indexes into the cmd.words array where the statements start and
	// end. A 2D array would have been more comprehensible. I think
	// this will break on more than one statement. I'm not checking
	// right now.
	if (T.cmd_words_idx >= TOPLEVEL_STATEMENT->cmd_words_end &&
			T.cmd_words_idx < cmd->words.n - 1) {
		T.cmd_words_idx++;
		T.s = cmd->words.d[T.cmd_words_idx];
		T.e = T.s + token_len;
	}
}

// When deciding whether you're adding a note or changing one, Note's
// rule is this: if you make a list non-notefile-generating list,
// you're adding a note. So if you do "note snotes ,'hello'", you're
// adding one. But if you did "note snotes n-1 ,'hello'", you're
// changing one. If you do just "note n-1" you get to this function,
// which loads the two default notefiles, and so your "n-1" refers to
// those.
static int load_default_notefiles (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *view,
		tsk_completions *completions) {
	int r = 1;

	int rc = load_notefile (ctx, cmd, "notes.nxt",
			TSK_FILE_FLAG_IS_SHORTCUT | TSK_FILE_FLAG_WAS_LOADED_BY_LIST, true, true, completions);
	if (rc)
		goto out;

	insert_word_into_cmd_words (cmd, "snotes.nxt");

	free (cmd->text);
	cmd->text = u_malloc (BUFSIZ);
	join_and_quote (BUFSIZ, cmd->text, cmd->words.n, cmd->words.d);

	if (!ctx->notefiles.n)
		cmd_err (ctx, cmd, completions, nullptr, "\
You started your expression with a non-note-loading list, and none of the default \
ones, initial.nxt and notes.nxt, exist in %s",
				ctx->opts->data_dir);

	r = 0;
out:
	add_all_items_whose_notefiles_were_referenced_by_this_statement_to_view (ctx,
			view);
	return r;
}

static int eval_parent (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr, tsk_completions *completions) {
	char cwd[PATH_MAX];
	getcwd (cwd, PATH_MAX);
	auto notefile = tsk_get_notefile_in_dir (ctx, cwd);
	if (!notefile)
		do {
			*strrchr (cwd, '/') = '\0';
			if (*cwd == '\0') {

				// This tsk_err is fine. The parent-dir option is
				// passed to tsk err, so there's no confusion, no
				// need to clarify where the error has come from.
				// And there's no error indicator to print,
				// nothing to indicate.
				cmd_err (ctx, cmd, completions, nullptr, "No notefiles were found");
				return 0;
			}
			if (notefile = tsk_get_notefile_in_dir (ctx, cwd))
				break;

		} while (true);
	int r = load_notefile (ctx, cmd, notefile, 0, true, false, completions);
	if (r)
		return r;
	return filter_loop (ctx, cmd, pr, 0, FILTER_LOOP_COMPILE_REGEX, completions, nullptr,
			match_notefile);
}

static bool is_notefileloading_filter (enum cmd_token_kind kind) {
	return kind == CTK_FILE || kind == CTK_SFILE || kind == CTK_PARENT ||
		kind == CTK_FILE_CREATE || kind == CTK_SFILE_CREATE;
}

static int eval_changed_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr, tsk_completions *completions) {
	match_arg args[] = {
		{._prop_id = IP_CHANGED},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, nullptr, match_has);
}

static int eval_remove_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr, tsk_completions *completions) {
	match_arg args[] = {
		{._prop_id = IP_REMOVE},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, nullptr, match_has);
}

static int eval_name_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr, tsk_completions *completions) {
	return filter_loop (ctx, cmd, pr, 0, FILTER_LOOP_COMPILE_REGEX, completions, nullptr,
			match_name);
}

static int eval_new_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr, tsk_completions *completions) {
	match_arg args[] = {
		{._prop_id = IP_NEW},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, nullptr, match_has);
}

static int eval_copy_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr, tsk_completions *completions) {
	match_arg args[] = {
		{._prop_id = IP_COPY},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, nullptr, match_has);
}

static int eval_locked_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr, tsk_completions *completions) {
	match_arg args[] = {
		{._prop_id = IP_LOCK},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, nullptr, match_has);
}

static int eval_super_locked_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr, tsk_completions *completions) {
	match_arg args[] = {
		{._prop_id = IP_SUPER_LOCK},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, nullptr, match_has);
}

static int eval_effort_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		tsk_completions *completions) {
	match_arg args[] = {
		{._size_t = offsetof (struct tsk_item, effort)},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, complete_effort_etc_filter,
			match_int);
}

static int eval_priority_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		tsk_completions *completions) {
	match_arg args[] = {
		{._size_t      = offsetof (struct tsk_item, priority)},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, complete_effort_etc_filter,
			match_int);
}

static int eval_link_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		enum statement_type statement_type,
		tsk_completions *completions) {
	return eval_note_prop (ctx, cmd, pr, statement_type, IT_LINK,
			offsetof (struct tsk_item, props), completions);
}

static int eval_tag_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		enum statement_type statement_type,
		tsk_completions *completions) {
	return eval_note_prop (ctx, cmd, pr, statement_type, IT_TAG,
			offsetof (struct tsk_item, props), completions);
}

static int eval_on_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		tsk_completions *completions) {
	match_arg args[] = {
		{._ts_time_p = &TIMES.now.time},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, complete_ts_range, match_on);
}

static int eval_interval_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		tsk_completions *completions) {
	match_arg args[] = {
		{._time_id = TIME_INTERVAL},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, complete_ts_range,
			match_ts_constant);
}

static int eval_last_reminded_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		tsk_completions *completions) {
	match_arg args[] = {
		{._time_id = TIME_LAST_REMINDED},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, complete_ts_range, match_ts_constant);
}

static int eval_grep_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		tsk_completions *completions) {
	return filter_loop (ctx, cmd, pr, nullptr, FILTER_LOOP_COMPILE_REGEX, completions, nullptr,
			match_body);
}

static int eval_postpone_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		tsk_completions *completions) {
	match_arg args[] = {
		{._time_id = TIME_POSTPONE},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, complete_ts_range,
			match_ts_constant);
}

static int eval_stop_repeating_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		tsk_completions *completions) { match_arg args[] = {
		{._time_id = TIME_STOP_REPEATING},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, complete_ts_range,
			match_ts_constant);
}

static int eval_relative_range_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		tsk_view last_view,
		tsk_completions *completions) {
	int r = 1;
	if (ctx->opts->flags & TSK_FLAG_SORT)
		qsort_r (last_view.d, last_view.n, sizeof *last_view.d, cmp_view_node,
				&ctx->items);
	r = get_and_dup_arg_if_needed (ctx, cmd, nullptr, completions);
	if (r)
		return r;

	r =  s2ia_str_to_relative_int_arr_wrapper (ctx, cmd, pr, last_view, completions);
	return r;
}

static int eval_from_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		enum statement_type statement_type,
		tsk_completions *completions) {
	return eval_note_prop (ctx, cmd, pr, statement_type, IT_FROM,
			offsetof (struct tsk_item, froms), completions);
}

static int eval_annotation_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		enum statement_type statement_type,
		tsk_completions *completions) {
	return eval_note_prop (ctx, cmd, pr, statement_type, IT_ANNOTATION,
			offsetof (struct tsk_item, props), completions);
}

static int eval_attachment_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		enum statement_type statement_type,
		tsk_completions *completions) {
	return eval_note_prop (ctx, cmd, pr, statement_type, IT_ATTACHMENT,
			offsetof (struct tsk_item, props), completions);
}

static int eval_type_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		enum statement_type statement_type,
		tsk_completions *completions) {
	match_arg args[] = {
		{._statement_type = statement_type},
	};
	return filter_loop (ctx, cmd, pr, args, FILTER_LOOP_COMPILE_REGEX, completions,
			complete_item_type, match_item_type);
}

static int eval_before_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		tsk_completions *completions) {
	match_arg args[] = {
		{._time_id = TIME_BEFORE},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, complete_ts_range,
			match_ts_constant);
}

static int eval_reminder_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		tsk_completions *completions) {
	match_arg args[] = {
		{._time_id = TIME_REMINDER},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, complete_ts_range,
			match_ts_constant);
}

static int eval_creation_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		tsk_completions *completions) {
	match_arg args[] = {
		{._time_id = TIME_CREATION},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, complete_creation_range,
			match_ts_constant);
}

static int eval_done_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		tsk_completions *completions) {
	match_arg args[] = {
		{._prop_id = NP_DONE},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, nullptr, match_has);
}

static int eval_has (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		tsk_completions *completions) {

	return filter_loop (ctx, cmd, pr, nullptr, 0, completions, complete_prop,
			match_comma_list_against_has);
}

static int eval_except_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		tsk_completions *completions) {
	match_arg args[] = {
		{._time_id = TIME_EXCEPT},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions,
			complete_ts_range, match_ts_constant);
}


static int eval_except_interval_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr,
		tsk_completions *completions) {
	match_arg args[] = {
		{._time_id = TIME_EXCEPT_INTERVAL},
	};
	return filter_loop (ctx, cmd, pr, args, 0, completions, complete_ts_range,
			match_ts_constant);
}

static int eval_filter (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *pr, tsk_view _last_view,
		enum statement_type statement_type, tsk_completions *completions) {

	// Save yourself some heartache and use a new variable. Don't
	// mutate last_view. I think that you *could* use a pointer and
	// mutate it. It would probably be fine. But ... It's hard to
	// understand.
	tsk_view last_view = {};
	arr_each (&_last_view, _)
		arr_add (&last_view, *_);
	pr->n = 0;

	int r = -50;

	if (!is_notefileloading_filter (T.kind)) {

		if (!NON_NOTEFILE_LOADING_FILTER_WAS_GIVEN && !last_view.n) {

			// The user has passed something like /hello as the first
			// item. This means she hasn't started with a notefile,
			// *and* hasn't passed a view to parse_statement (ie, is
			// an autocmd) meaning we use the default notefile,
			// notes.nxt.
			load_default_notefiles (ctx, cmd, &last_view, completions);
		}
		NON_NOTEFILE_LOADING_FILTER_WAS_GIVEN = 1;
	}

	switch (T.kind) {
	case CTK_ANNOTATION:      r = eval_annotation_filter (ctx, cmd, pr, statement_type, completions); break;
	case CTK_ATTACHMENT:      r = eval_attachment_filter (ctx, cmd, pr, statement_type, completions); break;
	case CTK_BEFORE:          r = eval_before_filter (ctx, cmd, pr, completions); break;
	case CTK_CHANGED:         r = eval_changed_filter (ctx, cmd, pr, completions); break;
	case CTK_COPY:            r = eval_copy_filter (ctx, cmd, pr, completions); break;
	case CTK_CREATION:        r = eval_creation_filter (ctx, cmd, pr, completions); break;
	case CTK_DONE:            r = eval_done_filter (ctx, cmd, pr, completions); break;
	case CTK_EFFORT:          r = eval_effort_filter (ctx, cmd, pr, completions); break;
	case CTK_EXCEPT:          r = eval_except_filter (ctx, cmd, pr, completions); break;
	case CTK_EXCEPT_INTERVAL: r = eval_except_interval_filter (ctx, cmd, pr, completions); break;
	case CTK_FILE:            r = eval_file_filter (ctx, cmd, pr, FFM_LOAD, completions); break;
	case CTK_FILE_CREATE:     r = eval_file_filter (ctx, cmd, pr, FFM_CREATE, completions); break;
	case CTK_FROM:            r = eval_from_filter (ctx, cmd, pr, statement_type, completions); break;
	case CTK_GREP:            r = eval_grep_filter (ctx, cmd, pr, completions); break;
	case CTK_HAS:             r = eval_has (ctx, cmd, pr, completions); break;
	case CTK_IMPLICIT_LIST:   r = view_copy (pr, arr_last (&cmd->statements)->view); break;
	case CTK_INTERVAL:        r = eval_interval_filter (ctx, cmd, pr, completions); break;
	case CTK_INT_LIST:        r = s2ia_str_to_relative_int_arr_wrapper (ctx, cmd, pr, last_view, completions); break;
	case CTK_LAST_REMINDED:   r = eval_last_reminded_filter (ctx, cmd, pr, completions); break;
	case CTK_LINK:            r = eval_link_filter (ctx, cmd, pr, statement_type, completions); break;
	case CTK_LOCKED:          r = eval_locked_filter (ctx, cmd, pr, completions); break;
	case CTK_MATCH_FILE:      r = eval_file_filter (ctx, cmd, pr, FFM_MATCH, completions); break;
	case CTK_MATCH_SFILE:     r = eval_sfile_filter (ctx, cmd, pr, FFM_MATCH, completions); break;
	case CTK_NAME:            r = eval_name_filter (ctx, cmd, pr, completions); break;
	case CTK_NEW:             r = eval_new_filter (ctx, cmd, pr, completions); break;
	case CTK_ON:              r = eval_on_filter (ctx, cmd, pr, completions); break;
	case CTK_PARENT:          r = eval_parent (ctx, cmd, pr, completions); break;
	case CTK_POSTPONE:        r = eval_postpone_filter (ctx, cmd, pr, completions); break;
	case CTK_PREV_LIST:       r = get_prev_view (ctx, cmd, pr, completions); break;
	case CTK_PRIORITY:        r = eval_priority_filter (ctx, cmd, pr, completions); break;
	case CTK_RELATIVE_RANGE:  r = eval_relative_range_filter (ctx, cmd, pr, last_view, completions); break;
	case CTK_REMINDER:        r = eval_before_filter (ctx, cmd, pr, completions); break;
	case CTK_REMOVE:          r = eval_remove_filter (ctx, cmd, pr, completions); break;
	case CTK_SFILE:           r = eval_sfile_filter (ctx, cmd, pr, FFM_LOAD, completions); break;
	case CTK_SFILE_CREATE:    r = eval_sfile_filter (ctx, cmd, pr, FFM_CREATE, completions); break;
	case CTK_STOP_REPEATING:  r = eval_stop_repeating_filter (ctx, cmd, pr, completions); break;
	case CTK_SUPER_LOCKED:    r = eval_super_locked_filter (ctx, cmd, pr, completions); break;
	case CTK_TAG:             r = eval_tag_filter (ctx, cmd, pr, statement_type, completions); break;
	case CTK_TYPE:            r = eval_type_filter (ctx, cmd, pr, statement_type, completions); break;
	default:
								cmd_err (ctx, cmd, completions, nullptr, "Bad token");
	}
	assert (r != -50 && "You've forgotten to deal with a T.kind");
	$free_if (last_view.d);

	return r;
}

// For action errors like ,"this" M/tmp/that. The problem there is
// M changes the action, and you need to highlight the M, not the
// /tmp/that.
static int check_and_set_action (tsk_ctx *ctx, tsk_cmd *cmd, struct action *action, enum action_id id,
		tsk_completions *completions) {
	if (action->id != A_NOT_SET && action->id != id) {
		cmd_err (ctx, cmd, completions, nullptr, "\
The cmd tries to change the action (from %s to %s)", ACTION_ID_STRS[action->id],
ACTION_ID_STRS[id]);
		return 1;
	}
	action->id= id;
	return 0;
}

static int add_action (tsk_ctx *ctx, tsk_cmd *cmd, struct action *action,tsk_completions *completions,
		enum action_id id, enum prop_id prop_id,
		void (*complete_fn) (tsk_ctx *, tsk_cmd *, tsk_completions *)) {

	if (check_and_set_action (ctx, cmd, action, id, completions))
		return 1;

	struct action_piece action_piece = {
		.prop = prop_id,
		.cmd_words_idx = T.cmd_words_idx,
	};

	if (get_and_dup_arg_if_needed (ctx, cmd, &action_piece.str, completions))
		return 1;

	arr_add (&action->pieces, action_piece);
	if (completions && complete_fn)
		complete_fn (ctx, cmd, completions);
	return 0;
}

static int parse_action (tsk_ctx *ctx, tsk_cmd *cmd, tsk_completions *completions) {

	auto action = &TOPLEVEL_STATEMENT->action;

	int r = -1;
#define ADD_ACTION(...) add_action (ctx, cmd, action, completions, __VA_ARGS__)
	switch (T.kind) {
	case CTKA_ADD:             r = ADD_ACTION (A_ADD,            -1,                 nullptr); break;
	case CTKA_LIST_SHORTCUTS:  r = ADD_ACTION (A_LIST_SHORTCUTS, -1,                 nullptr); break;
	case CTKA_ANNOTATION:      r = ADD_ACTION (A_CHANGE,         NP_ANNOTATION,      complete_str); break;
	case CTKA_ATTACHMENT:      r = ADD_ACTION (A_CHANGE,         NP_ATTACHMENT,      complete_str); break;
	case CTKA_BEFORE:          r = ADD_ACTION (A_CHANGE,         NP_BEFORE,          complete_future_time_str); break;
	case CTKA_BODY:            r = ADD_ACTION (A_CHANGE,         IP_BODY,            complete_str); break;
	case CTKA_COPY:            r = ADD_ACTION (A_COPY,           -1,                 complete_paths); break;
	case CTKA_COPY_CREATE:     r = ADD_ACTION (A_COPY_CREATE,    -1,                 complete_paths); break;
	case CTKA_COUNT:           r = ADD_ACTION (A_COUNT,          -1,                 nullptr); break;
	case CTKA_CMD:             r = ADD_ACTION (A_CMD,            -1,                 nullptr); break;
	case CTKA_DONE:            r = ADD_ACTION (A_CHANGE,         NP_DONE,            nullptr); break;
	case CTKA_EFFORT:          r = ADD_ACTION (A_CHANGE,         NP_EFFORT,          complete_effort_etc_filter); break;
	case CTKA_ENDING:          r = ADD_ACTION (A_CHANGE,         NP_ENDING,          complete_future_time_str); break;
	case CTKA_EXCEPT:          r = ADD_ACTION (A_CHANGE,         NP_EXCEPT,          complete_ts_range); break;
	case CTKA_EXCEPT_INTERVAL: r = ADD_ACTION (A_CHANGE,         NP_EXCEPT_INTERVAL, complete_ts_dur); break;
	case CTKA_FILE:            r = ADD_ACTION (A_FILE,           -1,                 complete_paths); break;
	case CTKA_FILE_CREATE:     r = ADD_ACTION (A_FILE_CREATE,           -1,          complete_paths); break;
	case CTKA_FMT:             r = ADD_ACTION (A_FMT,            -1,                 complete_str); break;
	case CTKA_HELP:            r = ADD_ACTION (A_HELP,           -1,                 complete_keywords); break;
	case CTKA_INTERVAL:        r = ADD_ACTION (A_CHANGE,         NP_INTERVAL,        complete_ts_dur); break;
	case CTKA_LAST_REMINDED:   r = ADD_ACTION (A_CHANGE,         NP_LAST_REMINDED,   complete_negative_ts_time); break;
	case CTKA_LINK:            r = ADD_ACTION (A_CHANGE,         NP_LINK,            complete_str); break;
	case CTKA_LOCK:            r = ADD_ACTION (A_CHANGE,         IP_LOCK,            nullptr); break;
	case CTKA_NAME:            r = ADD_ACTION (A_CHANGE,         IP_NAME,            complete_str); break;
	case CTKA_NOP:             r = ADD_ACTION (A_NOP,            -1,                 nullptr); break;
	case CTKA_ON:              r = ADD_ACTION (A_CHANGE,         NP_ON,              complete_future_time_str); break;
	case CTKA_OPEN:            r = ADD_ACTION (A_OPEN,           -1,                 nullptr); break;
	case CTKA_POSTPONE:        r = ADD_ACTION (A_CHANGE,         NP_POSTPONE,        complete_future_time_str); break;
	case CTKA_PRINT_TAGS:      r = ADD_ACTION (A_PRINT_TAGS,     -1,                 nullptr); break;
	case CTKA_PRIORITY:        r = ADD_ACTION (A_CHANGE,         NP_PRIORITY,        complete_effort_etc_action); break;
	case CTKA_QUIT:            r = ADD_ACTION (A_QUIT,           -1,                 nullptr); break;
	case CTKA_REMIND:          r = ADD_ACTION (A_REMIND,         -1,                 nullptr); break;
	case CTKA_REMINDER:        r = ADD_ACTION (A_CHANGE,         NP_REMINDER,        complete_reminder); break;
	case CTKA_REMOVE:          r = ADD_ACTION (A_REMOVE,         -1,                 nullptr); break;
	case CTKA_SCOPY:           r = ADD_ACTION (A_SCOPY,          -1,                 complete_sfiles); break;
	case CTKA_SFILE:           r = ADD_ACTION (A_SFILE,          -1,                 complete_sfiles); break;
	case CTKA_SFILE_CREATE:    r = ADD_ACTION (A_SFILE_CREATE,   -1,                 complete_sfiles); break;
	case CTKA_STOP_REPEATING:  r = ADD_ACTION (A_CHANGE,         NP_STOP_REPEATING,  complete_ts_dur); break;
	case CTKA_SUPER_LOCK:      r = ADD_ACTION (A_CHANGE,         IP_SUPER_LOCK,      nullptr); break;
	case CTKA_TAG:             r = ADD_ACTION (A_CHANGE,         NP_TAG,             complete_str); break;
	case CTKA_ITEM_TYPE:       r = ADD_ACTION (A_CHANGE,         IP_TYPE,            complete_item_type); break;
	case CTKA_UNSET:           r = ADD_ACTION (A_UNSET,          -1,                 complete_prop); break;
	case CTKA_UPDATE:          r = ADD_ACTION (A_UPDATE,         -1,                 nullptr); break;
	case CTKA_WRITE:           r = ADD_ACTION (A_WRITE,          -1,                 nullptr); break;
	case CTKA_WRITE_QUIT:      r = ADD_ACTION (A_WRITE_QUIT,     -1,                 nullptr); break;
#undef ADD_ACTION
	default:                   assert ("!Invalid action passed to parse_action; programming error");
	}

	assert (r != -1);
	return r;
}

static bool this_action_id_updates_notes (enum action_id action_id) {

	bool r = 0;

	switch (action_id) {
		case A_PRINT:
		case A_HELP:
		case A_NOP:
		case A_COUNT:
		case A_QUIT:
		case A_OPEN:
		case A_FMT:
		case A_PRINT_TAGS:
			break;
		default:
			r = 1;
			break;
	}
	return r;
}

static int sort_out_action (struct tsk_statement *statement) {
	auto action = &statement->action;
	if (!action->id) {
		if (action->edit_in_editor.val.yes)

			// This might get set to A_ADD directly below.
			action->id = A_CHANGE;
		else
			action->id = A_PRINT;
	}

	if (statement->action.id == A_CHANGE && !NON_NOTEFILE_LOADING_FILTER_WAS_GIVEN)
		statement->action.id = A_ADD;

	return 0;
}

static int parse_mod (tsk_ctx *ctx, tsk_cmd *cmd, tsk_completions *completions) {
	int r = 1;

	auto action = &TOPLEVEL_STATEMENT->action;

	switch (T.kind) {
	case CTK_MOD_MSG:
		r = get_and_dup_arg_if_needed (ctx, cmd, &action->msg, completions);
		if (r == -1)
			break;
		r = 0;
		break;
	case CTK_MOD_NORMAL:
		action->mods.confirmation_mode = CM_NORMAL;
		r = 0;
		break;
	case CTK_MOD_CONFIRM:
		action->mods.confirmation_mode = CM_CONFIRM;
		r = 0;
		break;
	case CTK_MOD_EDIT:
		action->edit_in_editor.val.yes = 1;
		arr_add (&action->edit_in_editor.pieces,
				(struct mod_piece) {.cmd_words_idx = T.cmd_words_idx});
		r = 0;
		break;
	case CTK_MOD_PRINT:
		char *print_what_str = 0;
		r = get_and_dup_arg_if_needed (ctx, cmd, &print_what_str, completions);
		if (r == -1)
			break;
		arr_add (&action->mods.print_what.pieces,
				((struct mod_piece) {.cmd_words_idx = T.cmd_words_idx,
				 .str = print_what_str}));
		r = 0;
		break;
	case CTK_MOD_FORCE:
		action->mods.confirmation_mode = CM_FORCE;
		r = 0;
		break;
	default:
		assert (0);
		break;
	}
	return r;
}

struct tsk_cmd make_cmd (size_t argc, char **argv, enum cmd_source cmd_source,
		const tsk_view *view) {
	struct tsk_cmd r = {};

	r.source = cmd_source;

	if (!argc && r.source != CMD_SOURCE_AUTOCMD &&
			(!view || !view->n))
		arr_add (&r.words, strdup ("snotes.nxt"));
	else {
		u_each (_, argc, argv)
			arr_add (&r.words, strdup (*_));
	}
	r.text = u_malloc (BUFSIZ);
	join_and_quote (BUFSIZ, r.text, r.words.n, r.words.d);

	return r;
}

// Returns a null one if passed null cmd. Fully copies the view --
// including subnotes, etc -- for now. I think I'll want to not do
// that. I don't think it'd be useful.
tsk_view *make_view_comprising_all_views_in_cmd (struct tsk_cmd *cmd) {
	if (!cmd)
		return nullptr;

	tsk_view *r = u_calloc (sizeof *r);
	arr_each (&cmd->statements, statement)
		view_copy (r, statement->view);
	return r;
}

static int get_token_deal_with_action (tsk_ctx *ctx, tsk_cmd *cmd, enum cmd_token_kind fake_token,
		tsk_completions *completions) {

	for (;;) {
		get_token (ctx, cmd, fake_token, completions);

		auto type = CMD_KEYWORDS[T.kind].type;

		// Add to the "filter words" array. When you're calling the
		// library again you could do with this.
		if (type == CT_TYPE_FILTER || type == CT_TYPE_OPERATOR) {

			if (type == CT_TYPE_FILTER &&
					is_notefileloading_filter (T.kind) &&
					T.cmd_words_idx == 0)
				cmd->flags |= TCF_NEW_VIEW_DISCARDED_OLD_VIEW;
			arr_add (&cmd->view_words, strdup (T.s));
			break;
		}

		if (T.kind == CTK_EQUALS) {
			cmd_err (ctx, cmd, completions, nullptr, "Equalses can only go after keywords that take values");
			return 1;
		} if (type == CT_TYPE_ACTION) {
			if (parse_action (ctx, cmd, completions))
				return 1;
		} else if (type == CT_TYPE_MOD) {
			if (parse_mod (ctx, cmd, completions))
				return 1;
		} else
			break;
	}
	if (T.kind == CTK_ERROR)
		return 1;
	return 0;
}

static int parse_brackets (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view **pr, enum statement_type statement_type,
		int *depth, tsk_completions *completions) {
	struct tsk_statement tmp = {};
	(*depth)++;
	parse_statement (ctx, cmd, depth, statement_type, nullptr, completions, &tmp);
	(*depth)--;

	if (tmp.flags & SF_FAILED)
		return 1;
	/* view_free (*pr); */
	*pr = tmp.view;

	// Since you're allowed to put an action wherever you want,
	// you might have made one in this parse_statement, so copy it
	// over.
	if (tmp.action.id)
		TOPLEVEL_STATEMENT->action = tmp.action;
	return 0;
}

static int parse_not (tsk_ctx *ctx, tsk_cmd *cmd, tsk_completions *completions) {
	if (get_token_deal_with_action (ctx, cmd, -1, completions))
		return 1;
	if (T.kind == CTK_END) {
		cmd_err (ctx, cmd, completions, nullptr, ERR_NO_END_AFTER_OPERATOR);
		return 1;
	}
	return 0;
}

static int get_evaled_list (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view **RESULT, tsk_view _last_view, bool was_not,
		enum statement_type statement_type, int *depth, tsk_completions *completions) {

	int r = 1;

	// Make a new last_view. We do not under any circumstance want to
	// mutate last_view. It's too confusing.
	tsk_view last_view = {};
	arr_each (&_last_view, _)

		// We're copying the pointers and all. It should be allowed.
		// last_view should never have children.
		arr_add (&last_view, *_);

	if (T.kind == CTK_OPEN_BRACKET) {
		if (parse_brackets (ctx, cmd, RESULT, statement_type, depth, completions))
			goto out;

	} else {
		if (T.kind == CTK_NOT) {
			was_not = 1;
			if (parse_not (ctx, cmd, completions))
				goto out;
		}
		if (CMD_KEYWORDS[T.kind].type == CT_TYPE_OPERATOR) {
			cmd_err (ctx, cmd, completions, nullptr, "Expected list, \"(\" or \";\"");
			goto out;
		} if (CMD_KEYWORDS[T.kind].type == CT_TYPE_FILTER) {
			if (eval_filter (ctx, cmd, *RESULT, last_view, statement_type, completions))
				goto out;
		}
	}
	if (was_not)
		view_set_diff (*RESULT, &last_view);
	r = 0;
out:
	$free_if (last_view.d);
	return r;
}

static int parse_prop (tsk_ctx *ctx, tsk_cmd *cmd, tsk_view *r_view, tsk_completions *completions) {

	$fori (i, r_view->n) {
		auto item = ctx->items.d + r_view->d[i].item_idx;
		if (!item->props)
			continue;
		int depth = -1;
		auto saved_t = T;
		auto real_items = ctx->items;
		ctx->items = *item->props;
		struct tsk_statement new_statement = {};
		parse_statement (ctx, cmd, &depth, STATEMENT_TYPE_PROP, nullptr, completions,
				&new_statement);
		ctx->items = real_items;
		if (new_statement.flags & SF_FAILED)
			return 1;
		r_view->props_were_specified |= 1;

		// Because actions can be set anywhere, it might have been
		// set while parsing the prop, so if it has been, set
		// statement->action to the new one.
		if (new_statement.action.id)
			TOPLEVEL_STATEMENT->action = new_statement.action;
		if (!new_statement.view->n) {
			tsk_free_statement (&new_statement);
			cmd_err (ctx, cmd, completions, nullptr, "\
Your prop bit matched nothing. If you were trying to add a prop you need \
to do that from the level above; something like \"note Tag=this\"");
			return 1;
		}
		r_view->d[i].next = new_statement.view;
		if (i != r_view->n - 1)
			T = saved_t;
	}
	if (!r_view->props_were_specified) {

		// FIXME: this error shouldn't be here.
		//
		// It no match should be an error condition, and
		// parse_cmd should check for it, too.
		//
		// Either that or this error should be here. Right now
		// it happens in note, I think.
		TOPLEVEL_STATEMENT->flags |= SF_FAILED;
		cmd_err (ctx, cmd, completions, nullptr, "No props matched");
		return 1;
	}
	return 0;
}

static int check_notefiles (tsk_ctx *ctx, tsk_cmd *cmd, struct tsk_statement *statement,
		tsk_completions *completions) {

	// We'd prefer to make sure you've actually loaded some
	// notefiles right at the start, not here, but we can't
	// know until we get here, can't know until the full
	// statement is parsed.
	if (statement->action.id != A_ADD && !ctx->notefiles.n) {

		// t.s, etc, are ignored when not calling cmd_err from outside
		// cmd.c, ie, when the match funcs are called directly from
		// the TUI.
		cmd_err (ctx, cmd, completions, nullptr, "You've not loaded any notefiles.");
		return 1;
	}

	// Now we check to see if you've tried to reference a file
	// that doesn't exist.
	arr_each (&statement->notefiles_referenced, _) {
		auto notefile = ctx->notefiles.d + *_;

		// We do this check so that autocmds don't check
		// if files exist. If they did, you'd get spammed
		// when adding a note to a new notefile ("note f
		// /tmp/new-file ,'hello, there'").
		if (!(notefile->flags & TSK_FILE_FLAG_CHECKED)) {
			notefile->flags |= TSK_FILE_FLAG_CHECKED;
			if (!n_fs_fexists (notefile->path)) {

				// It's only an error to be referencing a file
				// that doesn't exist if you're not adding an
				// item. It's lame to do it in this loop.  But
				// it's fine.
				if (statement->action.id != A_ADD) {
					cmd->flags |= TCF_FAILED;
					errno = 0;
					if (!completions) {
						tsk_warn (ctx, cmd, "\
%sotefile %s doesn't exist. It needs to exist if you're not adding an item (even \
with \"filecreate\", etc",
notefile->flags & TSK_FILE_FLAG_IS_SHORTCUT ? "Shortcut n" : "N",
notefile->path);
					}
				}
			}
		}
	}
	return 0;
}

static int add_item (tsk_ctx *ctx, tsk_view *new_view, int notefile_idx) {

	arr_add (&ctx->items, (struct tsk_item) {});
	auto new_item = arr_last (&ctx->items);
	arr_add (&new_item->notefile_idxes, notefile_idx);
	arr_add (new_view, (view_node) {.item_idx = ctx->items.n - 1});
	return 0;
}

static int add_items (tsk_ctx *ctx, struct tsk_statement *statement) {
	auto view = statement->view;

	tsk_view *new_view = u_calloc (sizeof *new_view);


	// We add items to all notefiles referenced. We don't actually
	// look at statement->notefiles_referenced to do this -- not
	// necessary. We loop through the view and add the notes if the
	// view's item's original notefile idx hasn't been seen. The
	// reason we ignore notefiles referenced using that wouldn't give
	// you the right answer for weird expression like this:
	//
	// 		note \( sons and snotes \) or swhatever ,"Add this"
	//
	// It would add the items to sons and snotes even though
	// the expression didn't produce any notes that had belonged to
	// sons and snotes.

	arr_each (&statement->notefiles_referenced, notefile_referenced) {

		// This is a bit shit. This means that if a notefile has not
		// items we add the new item to it. This means that you would
		// add an item with the following expression, when to be
		// totally correct according to my rules it shouldn't:
		//
		// 		note f./empty-file and
		// 				f./file-that-is-not-empty ,"Adding"
		//
		// That expression evaluates to zero, so I think it should
		// error. Because this next *is* an error:
		//
		// 		note f./exists and f./also-exists ,"Adding"
		//
		// But without allowing this, I can't see how you could ever
		// add an item -- according to these rules of mine.
		if (!ctx->notefiles.d[*notefile_referenced].n_items_first_loaded_from_this_notefile)
			add_item (ctx, new_view, *notefile_referenced);
	}

	bool seen[ctx->notefiles.n];
	memset (seen, 0, sizeof seen);

	arr_each (view, view_node) {
		auto item = ctx->items.d + view_node->item_idx;
		if (!seen[item->notefile_idxes.d[0]]) {
			add_item (ctx, new_view, item->notefile_idxes.d[0]);
			seen[item->notefile_idxes.d[0]] = 1;
		}
	}
	if (view->n)
		view_free (view);
	statement->view = new_view;
	return 0;
}

static int add_used_notefiles_to_cmd_words (tsk_ctx *ctx, tsk_cmd *cmd,
		const tsk_view *prev_view) {

	return 0;

	// This updates the cmd.words array so that it's a valid command.
	//
	// However, it doesn't really work. It'll work OK in some cases
	// but not in all of them. What I want is to maintain the tokens
	// that comprise your command as your filter them down in the TUI.
	// I want the "view names" to look like this:
	//
	// 		snotes
	//      snotes /whatever
	//      snotes /whatever n-1
	//
	// Without this function these will look like:
	//
	// 		snotes
	//      /whatever
	//      n-1
	//
	// With this function you'll get this:
	//
	// 		snotes
	//      snotes /whatever
	//      snotes n-1
	//
	// And that's just wrong. The final result isn't, as the name
	// says, the last note in the "notes" notefile
	int *seen = nullptr;
	int r_n_seen = 0, a = 0;

	for (int i = 0; i < prev_view->n; i++) {
		int match_idx = -1;
		for (int j = 0; j < r_n_seen; j++) {
			if (!strcmp (ctx->notefiles.d[prev_view->d[i].item_idx].path,
						ctx->notefiles.d[seen[j]].path)) {
				match_idx = i;
				break;
			}
		}
		if (match_idx == -1) {
			tsk_item *item = ctx->items.d + prev_view->d[i].item_idx;
			tsk_notefile *notefile = &ctx->notefiles.d[item->notefile_idxes.d[0]];
			char *word = nullptr;
			char *the_basename = basename (notefile->path);
#define doit() do { \
	if (notefile->flags & TSK_FILE_FLAG_IS_SHORTCUT) \
		asprintf (&word, "s%s", the_basename); \
	else \
		asprintf (&word, "f%s", notefile->path); \
} while (0)
			if (!r_n_seen) {
				doit ();
				insert_word_into_cmd_words (cmd, word);
			} else {
				doit ();
				insert_word_into_cmd_words (cmd, word);
				asprintf (&word, "or");
				insert_word_into_cmd_words (cmd, word);
			}
			u_add (seen, r_n_seen, a, 32, i);
		}
	}
	return r_n_seen;
}

// This function is messy. It's pretty tricky to write.
static int finalise_statement (tsk_ctx *ctx, tsk_cmd *cmd, const tsk_view *prev_view,
		struct tsk_statement *statement, tsk_completions *completions) {

	auto action = &statement->action;
	if (statement->flags & SF_FAILED || sort_out_action (statement))
		return 1;


	// You can add like "note ,'this'", and when you do, you have
	// no items and so should load the default ones. You can't end
	// up with no items if you changed. In "note n-1 ,'hi'", the
	// default items would get loaded in eval_filter. We can't
	// really do this step earlier, can't fold it into
	// eval_filter, because we don't know if we've note parsed
	// notefiles until we get here.
	if (!statement->notefiles_referenced.n) {
		int n_added_words;
		if (prev_view)
			n_added_words = add_used_notefiles_to_cmd_words (ctx, cmd, prev_view);
		else if (!NON_NOTEFILE_LOADING_FILTER_WAS_GIVEN) {

			int n_notefiles_before = ctx->notefiles.n;
			load_default_notefiles (ctx, cmd, statement->view, completions);

			n_added_words = ctx->notefiles.n - n_notefiles_before;

		}
		// We add to the each action piece's cmd words idx
		// because load_default_notefiles adds notefiles
		// into the cmd->words array.
		arr_each (&action->pieces, _) {
			_->cmd_words_idx += n_added_words;
		}
	}

	// This is half-creating the new item. The other half is done in
	// init_added_item. Don't ask questions. This is the way I'm doing
	// it.
	if (action->id == A_ADD)
		add_items (ctx, statement);

	if (check_notefiles (ctx, cmd, statement, completions))
		return 1;

	if (!ctx->items.n && statement->action.id != A_COUNT) {

		char *msg = rab_get_comma_list (ctx->notefiles.n, ctx->notefiles.d,
				&(rab_pclo) {.and_str = "or", .getter = &NOTEFILE_GETTER});
		cmd_err (ctx, cmd, completions, nullptr, "There are no items in %s", msg);
		free (msg);
		return 1;
	}

	if (this_action_id_updates_notes (statement->action.id))
		statement->flags |= SF_ACTION_MODIFIES_NOTES;

	return 0;
}

static bool is_unmatched_brackets (tsk_ctx *ctx, tsk_cmd *cmd, int *depth,
		tsk_completions *completions) {
	if (depth && *depth > 0) {
		cmd_err (ctx, cmd, completions, nullptr, "Unmatched brackets");
		return 1;
	}
	return 0;
}

// A statement is a list ( n-1 or { /this/ and /that }) and an action,
// ended perhaps by a ;. An tsk_do_action (eg Remove, Done) can
// anywhere in there. So even though this parse_statement returns an
// item, the action part is ignored. The action stuff goes in struct
// action *action here and it copied at the end. I think this is fine.
static int parse_statement (tsk_ctx *ctx, tsk_cmd *cmd, int *depth,
		enum statement_type statement_type, const tsk_view *prev_cmds_view,
		tsk_completions *completions, struct tsk_statement *out) {

	// NOTE: I'm assuming you've initially pass depth = -1.
	//
	// And not incrementing depth if PROP. Not sure what the right
	// thing to do is yet. In "note prop yt n-1", that's not really
	// the same as "note \( n-1 or n1 \). In that case you're really
	// doing real recursion. This is literally recursion, but ...

	*out = (struct tsk_statement) {.view = u_calloc (sizeof *out->view)};

	if (prev_cmds_view && prev_cmds_view->n)
		view_copy (out->view, prev_cmds_view);

	// This is just for the int array but it has to be a list because
	// this function is called again if there's an open bracket and
	// it returns an item. Any actions found in the recursion are
	// parsed as normal, ie, you don't have to make sure you put your
	// actions outside {}s.
	struct tsk_statement list_statement = {
		.view = u_calloc (sizeof *list_statement.view)
	};

	// But I'll take a pointer to it.
	auto this_list = list_statement.view;

	if (get_token_deal_with_action (ctx, cmd, cmd->statements.n ? CTK_IMPLICIT_LIST : -1,
			completions))
		goto fail;

	if (T.kind == CTK_OPEN_BRACKET) {
		if (parse_brackets (ctx, cmd, &out->view, statement_type, depth, completions))
			goto out;
	} else if (T.kind == CTK_END || T.kind == CTK_SEMICOLON || T.kind == CTK_PROP) {

		//  note : -- that's a failure. Maybe not? Maybe you should
		//  match everything? I can't remember how it works. I'm going
		//  to do this to fix a crash.
		if (T.kind == CTK_PROP)
			goto fail;
		if (is_unmatched_brackets (ctx, cmd, depth, completions))
			goto fail;
		else
			goto out;

		// Probably could put a pointer to the matching cmd_keyword in
		// the cmd_token.
	} else if (CMD_KEYWORDS[T.kind].type != CT_TYPE_FILTER && T.kind != CTK_NOT) {
		cmd_err (ctx, cmd, completions, nullptr, "\
Expected a list or \"not\", not \"%s\", which is a %s",
CMD_KEYWORDS[T.kind].keyword.long_name,
CMD_TOKEN_TYPE_STRS[CMD_KEYWORDS[T.kind].type]);
		goto fail;
	}

	// We need a new object for the "last list" (or whatever). We
	// can't just pass r.view, even by value, because r.view gets
	// mutated. We need to use a different object, so we'll use
	// this_list and set its n to 0 after.
	view_copy (this_list, out->view);
	if (get_evaled_list (ctx, cmd, &out->view, *this_list, 0, statement_type, depth,
				completions))
		goto fail;
	this_list->n = 0;

	if (CMD_KEYWORDS[T.kind].type == CT_TYPE_ACTION)
		goto out;

	for (;;) {

		if (get_token_deal_with_action (ctx, cmd, -1, completions))
			goto fail;

		if (T.kind == CTK_PROP) {
			if (parse_prop (ctx, cmd, out->view, completions))
				goto fail;
			goto out;
		}

		if (T.kind == CTK_END || T.kind == CTK_SEMICOLON) {
			if (is_unmatched_brackets (ctx, cmd, depth, completions))
				goto fail;
			else
				goto out;
		} else if (T.kind == CTK_CLOSE_BRACKET)
			goto out;

		bool was_not = 0;

		// Get the operator
		enum cmd_token_kind operator;
		if (CMD_KEYWORDS[T.kind].type == CT_TYPE_OPERATOR) {
			if (T.kind == CTK_NOT) {
				operator = CTK_AND;
				was_not = 1;
			} else
				operator = T.kind;

			if (get_token_deal_with_action (ctx, cmd, -1, completions))
				goto fail;

			if (T.kind == CTK_END || T.kind == CTK_SEMICOLON
					|| T.kind == CTK_PROP) {
				cmd_err (ctx, cmd, completions, nullptr, ERR_NO_END_AFTER_OPERATOR);
				goto fail;
			}

			if (T.kind == CTK_CLOSE_BRACKET) {
				cmd_err (ctx, cmd, completions, nullptr, "\
1: Expected not a close bracket list. Clean up this code; just keep compressing until it's clear");
				goto fail;
			}

		} else if (CMD_KEYWORDS[T.kind].type != CT_TYPE_OPERATOR)
			operator = CTK_AND;

		// Get the list.
		if (get_evaled_list (ctx, cmd, &this_list, *out->view, was_not, statement_type, depth,
					completions))
			goto fail;

		if (CMD_KEYWORDS[T.kind].type == CT_TYPE_ACTION)
			goto out;

		switch (operator) {
			case CTK_AND: view_and (out->view, this_list); break;
			case CTK_OR:  arr_insert_mul (out->view, out->view->n, this_list->d,
								  this_list->n); break;
			default:      assert (0);
		}
	}

fail:
	out->flags |= SF_FAILED;
out:
	view_free (this_list);
	free (this_list);

	return 0;
#undef GET_STATEMENT_TOKEN
}

void set_now () {

	TIMES.now = (struct ts_constant) {
		.time = ts_time_from_ts (time (nullptr)),
		.type = TS_CONSTANT_TYPE_TIME
	};

}

int tsk_update_ctx (tsk_ctx ctx[static 1], bool initialise,
		tsk_print_ctx *pc, tsk_options *opts) {

	if (initialise)
		*ctx = (tsk_ctx) {};

	if (pc)
		ctx->pc = pc;
	if (opts)
		ctx->opts = opts;

	if (!ctx->opts->print_what)
		ctx->opts->print_what = (unsigned) -1;

	if (!ctx->opts->truncate_len)
		ctx->opts->truncate_len = TSK_TRUNCATE_LEN;

	if (!ctx->opts->cwd)
		ctx->opts->cwd = getcwd (0, 0);

	set_now ();

	if (!initialise)
		return 0;

	// We can't really load read_notefile with load_notefile because
	// it calls cmd_err, and that requires a cmd to exist. You could
	// create a dummy cmd, that's no good, because tsk_msg_ap prints
	// "From cmd XXXX". I'm not going to add more flags to it. It'd be
	// awful.
	auto initial_notefile = create_notefile_struct (ctx, "initial.nxt",
			TSK_FILE_FLAG_IS_SHORTCUT | TSK_FILE_FLAG_WAS_LOADED_BY_LIST);
	if (n_fs_fexists (initial_notefile.path)) {
		struct rn_err_info rn_err_info;
		int rc = read_notefile (ctx, &initial_notefile, 0, &rn_err_info,
				&ctx->items);
		if (rc) {
			tsk_warn_no_cmd (ctx, "Failed to parse the default notefile %s: %s",
					initial_notefile.path, rn_err_info.msg);
			return 1;
		}
	} else
		free_notefile (&initial_notefile);

	return 0;
}

static int set_sfiles (tsk_ctx *ctx, tsk_cmd *cmd) {

	// The sfiles are stored as just the basenames.
	char *saved_dir = n_fs_chdir (ctx->opts->data_dir);
	if (!saved_dir) {
		tsk_warn (ctx, cmd, "Couldn't chdir to %s", ctx->opts->data_dir);
		return 1;
	}
	tsk_glob_path (ctx, cmd, "*.nxt", ".", &ctx->sfiles);

	// And we just get rid of the "./", else we'll end up with
	// paths like this: "/home/you/.local/note/./initial.nxt. This
	// wouldn't be a problem, but we use regexes to match shortcut
	// notefiles. And I *do* want that, basically so you can match
	// notefiles with partial matches: "sno" might match "notes".
	// With fnmatch, with globs you'd need to append an asterisk.

	arr_each (&ctx->sfiles, _) {
		int len = strlen (*_ + 2);
		memmove (*_, *_ + 2, len);
		(*_)[len] = 0;
	}

	chdir (saved_dir);
	return 0;
}

static struct tsk_cmd really_parse_cmd (tsk_ctx *ctx,
		char **cmd_words, size_t n_cmd_words, enum cmd_source cmd_source,
		struct tsk_cmd *prev_cmd, tsk_completions *completions) {

	// I'm setting the now here and in tsk_parse_cmd. You don't need
	// or I think want to set it in every API function. But you do
	// want to set it
	set_now ();

	struct tsk_cmd r = {.flags = TCF_FAILED};

	if (!ctx->sfiles.n &&

			// I dunno if it's sensible to set these here, but I also
			// don't care. It's definitely not horrific.
			set_sfiles (ctx, &r))
		return r;

	const tsk_view *prev_view = make_view_comprising_all_views_in_cmd (prev_cmd);
	r = make_cmd (n_cmd_words, cmd_words, cmd_source, prev_view);
	if (r.statements.n)
		return r;

	if (!ctx->items.d)
		ctx->items = (struct tsk_items) {};

	T = (struct cmd_token) {.e = *r.words.d};

	// parse_statement increments depth at its top, so pass -1 here.
	int depth = 0;
	for (;;) {

		// This is a dirty global.
		NON_NOTEFILE_LOADING_FILTER_WAS_GIVEN = 0;

		// We use this for inserting the "snotes" into the cmd.words
		// array and also for printing errors. Ditto cmd_words_end
		// below.
		while (T.kind == CTK_SEMICOLON)
			get_token (ctx, &r, -1, completions);
		T.e = r.words.d[T.cmd_words_idx];
		int cmd_words_idx = T.cmd_words_idx;

		struct tsk_statement statement = {};

		// We take a pointer to statement, and do it before
		// parse_statement, since we don't want to take pointers to
		// the statements created when we parse brackets or props.
		TOPLEVEL_STATEMENT = &statement;

		parse_statement (ctx, &r, &depth, STATEMENT_TYPE_NOTE, prev_view, completions,
				&statement);
		statement.cmd_words_idx = cmd_words_idx;
		statement.cmd_words_end = T.cmd_words_idx;

		if (!statement.flags & SF_FAILED && finalise_statement (ctx, &r, prev_view,
					&statement, completions))
			TOPLEVEL_STATEMENT->flags |= SF_FAILED;

		// Note we add the statement even if it fails.
		arr_add (&r.statements, statement);
		if (statement.flags & SF_FAILED) {
			r.flags |= TCF_FAILED;
			break;
		}

		if (T.kind == CTK_END || statement.action.id == A_QUIT)
			break;
	}

	// Remove dups.
	arr_each (&r.statements, _)
		view_remove_dups (_->view);

	ts_errno = 0;
	errno = 0;

	return r;
}

// This returns a command, but it also pushes to the item array. Feels
// a little nasty. But maybe not. Maybe it's perfectly acceptable.
//
// Don't be tempted to remove _cmd_arr since we're passing no. no
// has the command array when statement comes from cmdline but if it's
// an autocmd, it comes from the file.

// Exported
struct tsk_cmd tsk_parse_cmd (tsk_ctx *ctx,
		char **cmd_words, size_t n_cmd_words, enum cmd_source cmd_source,
		struct tsk_cmd *prev_cmd) {
	return really_parse_cmd (ctx, cmd_words, n_cmd_words, cmd_source, prev_cmd,
			nullptr);
}

static char **get_argv_from_string_for_completion (tsk_ctx *ctx, char *_cmd_str,
		ssize_t *n_out) {

	char *cmd_str = (char [BUFSIZ]) {};

	char *end = strchr (_cmd_str, 0);

	// If you're pressing tab in empty space, etc:
	//
	// 		note this <HERE>
	//
	// we need to treat it a little specially, because Note obviously
	// won't parse a token that doesn't exist. So let's just add a
	// special one. Hacky, I suppose.
	if (

			// Permit you doing this: --complete="" for no particular
			// reason.
			end == _cmd_str || isspace (end[-1]) || end[-1] == '=') {
		snprintf (cmd_str, BUFSIZ, "%s\x01", _cmd_str);
	} else
		cmd_str = _cmd_str;

	char **r = rab_shell_split (cmd_str, n_out);
	if (!r) {
		tsk_warn_no_cmd (ctx, "rab_shell_split failed on --complete argument \"%s\"",
				cmd_str);
		return nullptr;
	}

	return r;
}

static tsk_completions tsk_get_completions_from_argv (tsk_ctx *ctx, int argc,
		char **argv, struct tsk_cmd *prev_cmd) {

	// Return completions. I'm happy to return them because the
	// matches are a static array.
	tsk_completions r = {};
	struct tsk_items items = {};

	// We don't care about the items. The fact this means we have to
	// swap out the items member and put it back suggests maybe the
	// items shouldn't be part of the tsk_ctx struct. We'll see.
	auto saved_items = ctx->items;
	ctx->items = items;
	really_parse_cmd (ctx, argv, argc, CMD_SOURCE_FRONTEND, prev_cmd, &r);
	tsk_free_items (&ctx->items);
	ctx->items = saved_items;
	return r;
}

int tsk_get_completions_from_str (tsk_ctx *ctx, char *cmd_str, struct tsk_cmd *prev_cmd,
		tsk_completions *out) {

	memset (out, 0, sizeof *out);

	ssize_t argc;
	char **argv = get_argv_from_string_for_completion (ctx, cmd_str, &argc);
	if (!argc)
		return 1;

	*out = tsk_get_completions_from_argv (ctx, argc, argv, prev_cmd);

	for (int i = 0; i < argc; i++)
		free (argv[i]);
	free (argv);
	return 0;
}

void nuke_tsk_completion_match (tsk_completion_match *match) {
	free (match->str);
	*match = (typeof (*match)) {};
}

void tsk_nuke_completion (tsk_completions *tsk_completions) {
	for (int i = 0; i < tsk_completions->count; i++)
		nuke_tsk_completion_match (&tsk_completions->matches[i]);
	*tsk_completions = (struct tsk_completions) {};
}

