#pragma once

#include "tsk-item.h"
#include "tsk-print.h"
extern thread_local struct tsk_statement *ACTION_STATEMENT;

// The following are used with action err. Like this first one you'd
// pass instead of a valid "cursor". You'd use it when there is no
// item to print. You could put these in a struct with an enum if you
// wanted, but you'd have to nearly always pass the struct in that
// case since the cursor at least nearly always has to be passed.
// 2023-06-17T20:02:48+01:00: this seems even shitter than I thought.
enum { AE_NO_ITEM_TO_PRINT = -1 };
enum { AE_ERR_IS_WHOLE_LINE = -1 };
enum { AE_NO_CMD_WORD_TO_PRINT = -2 };

int action_err (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor, int cmd_words_idx,
		struct err_info *read_notefile_err_info, struct tsk_statement *statement,
		char *fmt, ...)
	TSK_HIDE;
int expand_action (tsk_ctx *ctx, tsk_cmd *cmd, struct action *action,
		struct tsk_statement *statement)
	TSK_HIDE;

int tsk_do_cmd (tsk_ctx *ctx, struct tsk_cmd *cmd, bool *notes_were_updated);
int tsk_run_autocmds (tsk_ctx *ctx, tsk_cmd *prev_cmd, bool *notes_were_updated);
int tsk_print_confirmation (tsk_ctx *ctx, tsk_cmd *cmd,
		struct tsk_confirmation *confirmation,
		int (*print_fn) (struct tsk_print_ctx *, char *, ...));
int tsk_get_print_what (enum prop_id *pr, char *str, char err_buf[TSK_MAX_ERR_BUF]);
