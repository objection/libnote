#define _GNU_SOURCE
#include "tsk-cmd-keywords.h"

struct tsk_cmd_keyword CMD_KEYWORDS[N_CTK] = {
	[CTK_NOT_SET] = {.type = CT_TYPE_SPECIAL},
	[CTKA_ADD] = {
		.keyword = {
			.long_name = "Add",
			.short_name = 0,
			.long_help_descr = "\
Specify you're adding a note; not necessary, because adding \
happens when you specify a body but not a list; see examples.",
		.short_help_descr = "\
Specify you're adding a note; not necessary",
			.args_descr = 0,
			.examples_str = "\
note Add ,\"Hi\"\
	Adds a note with the body \"Hi\".\n\
note ,\"Hi\"\
	Adds a note with the body \"Hi\".\n\
note smy-shortcut-notefile ,\"Hi\"\
	Adds a note to \"my-shortcut-notefile\" with the body \"Hi\".",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_ANNOTATION] = {
		.keyword = {
			.long_name = "Annotation",
			.short_name = 'A',
			.long_help_descr = "\
Add an annotation, which is just extra text that's printed after the body. \
You could use this to add, eg, a list of books you want to read over time. In \
truth I think this is just fiddly; you're better of with just notes and tags.",
			.short_help_descr = "Add an annotation",
			.args_descr = "TEXT",
			.examples_str = "\
note ,\"Books to read\" Annotation=\"Stormbringer, Moorcock\"\
	Makes a new note with the body \"Books to read\" and the annotation \"Stormbringer, \
Moorcock\"\n\
note \"/Books to read/\" Annotation=\"The Metatemporal Detective, Moorcock\"\
	Adds the annotation \"Stormbringer, Moorcock\" to the note just made.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_ATTACHMENT] = {
		.keyword = {
			.long_name = "Attachment",
			.short_name = 0,
			.long_help_descr = "\
Add an \"attachment\". This appears to to nothing.",
			.short_help_descr = "Add an attachement",
			.args_descr = "PATH",
			.examples_str = 0,
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_BEFORE] = {
		.keyword = {
			.long_name = "Before",
			.short_name = 'B',
			.long_help_descr = "\
Add a \"before\", a time before which you're meant do the thing \
talked about in the note. In truth I think this has bitrotted to \
nothing. It used to be considered when matching ons but actually \
it doesn't now. But you can use it for your own pleasure.",
			.short_help_descr = "Add a \"before\"",
			.args_descr = "TIME STR",
			.examples_str = "\
note before=2d..3d\
	Prints notes whose \"before\" is between 2 days and 3 days from now.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_BODY] = {
		.keyword = {
			.long_name = "Body",
			.short_name = ',',
			.long_help_descr = "\
Assigns the note a body. It's also the command that specifies that you're \
adding a note. You can't add a note that has no body. The short for of \
\"Body\" is \",\", because it's quick to type.",
			.short_help_descr = "Assign the note a body",
			.args_descr = "STR",
			.examples_str = "\
note ,\"This is the body of my note\"\
	Add a new note with a body\n\
note Body=\"This is the body of my note\"\
	Does the same thing but in a less cool way\n\
note n-1 ,\"This is the body of my note\"\
	Changes the body of last note in the default notefile.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_CMD] = {
		.keyword = {
			.long_name = "Cmd",
			.short_name = 0,
			.long_help_descr = "\
Does nothing yet. If it did it would be like an autocmd except you'd \
run it yourself. It'd be like a janky function.",
			.short_help_descr = "Does nothing yet",
			.args_descr = "NOTE COMMAND",
			.examples_str = nullptr,
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_COPY] = {
		.keyword = {
			.long_name = "Copy",
			.short_name = 0,
			.long_help_descr = "\
Copy the note to notefile (not a \"shortcut\" notefile: use Scopy for that).",
			.short_help_descr = "Copies a notefile",
			.args_descr = "PATH",
			.examples_str = "\
note n-1 Copy=/tmp/some-notes.nxt\
	Copies the last note to the notefile at /tmp/some-notes.nxt.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_COPY_CREATE] = {
		.keyword = {
			.long_name = "Copycreate",
			.short_name = 0,
			.long_help_descr = "\
Copy the note to notefile that already exists (not a \"shortcut\" notefile: use \
Scopy or Scopycreate for that).",
			.short_help_descr = "Copies item, creating file",
			.args_descr = "PATH",
			.examples_str = "\
note n-1 Copycreate=/tmp/some-notes.nxt\
	Copies the last note to the notefile at /tmp/some-notes.nxt whether it exists \
or not",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_COUNT] = {
		.keyword = {
			.long_name = "Count",
			.short_name = 0,
			.long_help_descr = "\
Outputs the number of items in your view.",
			.short_help_descr = "Outputs the number of items in your view",
			.args_descr = nullptr,
			.examples_str = "\
note n-1 Count\
	1\n\
note n0,1 Count\
	2\n\
note /hello Count\
	The number of notes in your default notefile that have \"hello\" \
in their bodies.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_DONE] = {
		.keyword = {
			.long_name = "Done",
			.short_name = 'D',
			.long_help_descr = "\
Mark a note as \"done\".",
			.short_help_descr = "Mark notes as done",
			.args_descr = 0,
			.examples_str = "\
note n-1 Done\
	Mark the last note as done\n\
note not done Done\
	Mark all undone notes as done.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_EFFORT] = {
		.keyword = {
			.long_name = "Effort",
			.short_name = 0,
			.long_help_descr = "\
Set how much effort you think doing the thing you're taking a note about \
will take, in a range between 0-10. \
Check \"effort\".",
			.short_help_descr = "Set how much effort you think the note will take",
			.args_descr = "INT LIST",
			.examples_str = "\
note ,\"Completely overhaul note\" Effort=10\
	Add the note \"Completely overhaul note\", with an \"Effort\" of 10.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_ENDING] = {
		.keyword = {
			.long_name = "Ending",
			.short_name = 0,
			.long_help_descr = "\
Set when a particlar event ends. This necessates \"On\".",
			.short_help_descr = "Set notes' ends",
			.args_descr = "TIME STR",
			.examples_str = "\
note ,\"Tidy house\" On=2h Ending=3h\
	A new note, an event, that begins in two hours and ends and hour after that.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_EXCEPT] = {
		.keyword = {
			.long_name = "Except",
			.short_name = 'X',
			.long_help_descr = "\
Set an exception to notes with \"ons\" and \"intervals\". Pretty sure. It's \
broken, I think.",
			.short_help_descr = "Set interval exception",
			.args_descr = "TIME STR",
			.examples_str = "\
note ,\"This\" On=1d Interval=1d Except=3d\
	Honest, I can't be bothered. This either works or it doesn't",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_EXCEPT_INTERVAL] = {
		.keyword = {
			.long_name = "Except-interval",
			.short_name = 0,
			.long_help_descr = "\
Broken; never mind it.",
			.short_help_descr = "Broken",
			.args_descr = "TIME STR",
			.examples_str = nullptr,
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_FILE] = {
		.keyword = {
			.long_name = "File",
			.short_name = 'F',
			.long_help_descr = "\
Move items in view to notefile at PATH -- if the file exists. If it doesn't \
necessarily, use Filecreate",
			.short_help_descr = "Move note",
			.args_descr = "PATH",
			.examples_str = "\
note n-1 File ~/git/note/notes.nxt\
	Moves the last note to ~/git/note/notes.nxt.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_FILE_CREATE] = {
		.keyword = {
			.long_name = "Filecreate",
			.short_name = 0,
			.long_help_descr = "\
Move items in view to PATH, creating it if necessary. Use \"File\" if you want \
to be careful you don't accidentally create a new file",
			.short_help_descr = "Move note, create file",
			.args_descr = "PATH",
			.examples_str = "\
note n-1 Filecreate ~/git/note/notes.nxt\
	Moves the last note to ~/git/note/notes.nxt, which might not exist until \
you do this.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_FMT] = {
		.keyword = {
			.long_name = "Fmt",
			.short_name = '%',
			.long_help_descr = "\
I'll come back to this.",
			.short_help_descr = "Dunno",
			.args_descr = "FMT STRING",
			.examples_str = nullptr,
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_HELP] = {
		.keyword = {
			.long_name = "Help",
			.short_name = 0,
			.long_help_descr = "\
Print a description of CMD NAME and examples if there are any. If no arg, \
print all cmds. The first line is like \"short opt | longopt\", eg \
\"d | done\". If a cmd doesn't \
have a long opt or a short opt, that part of the line will be missing.",
			.short_help_descr = "List commands",
			.args_descr = "CMD NAME",
			.examples_str = "\
note Help=Sfile\
	Prints a description of \"Sfile\"\n\
note Help=S\
	Prints a description of \"Sfile\".",
			.args_are_optional = true,
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_INTERVAL] = {
		.keyword = {
			.long_name = "Interval",
			.short_name = 'I',
			.long_help_descr = "\
Set when the note is going to repeat. Require \"on\".",
			.short_help_descr = "Set note-repeat interval",
			.args_descr = "DURATION",
			.examples_str = "\
note ,\"Say hello\" On=1h Interval=1d\
	Creates a note that's going to \"happen\" in an hour, and at the same time of the \
day forever after.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_ITEM_TYPE] = {
		.keyword = {
			.long_name = "Type",
			.short_name = 'Y',
			.long_help_descr = "\
Set/change the types of the items in your view.",
			.short_help_descr = "Set notes' types",
			.args_descr = "ITEM TYPE",
			.examples_str = "\
note type=note Type=autocmd\
	Change (for some reason, perhaps as a laugh) all of your notes into autocmds \
that wouldn't work.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_LAST_REMINDED] = {
		.keyword = {
			.long_name = "Last-reminded",
			.short_name = 0,
			.long_help_descr = "\
Set a \"last reminded\". Normally you wouldn't do this. This gets set (to now) \
by Remind. I put it here because I bet you will want to do this as some point.",
			.short_help_descr = "Set \"last reminded\"",
			.args_descr = "TIME STR",
			.examples_str = "\
note n-1 Last-reminded=-4d10\
	Set that last note's last-reminded to be four days ago.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_LINK] = {
		.keyword = {
			.long_name = "Link",
			.short_name = 0,
			.long_help_descr = "\
Create a \"link\". It's just a string that you can open with the action \"Open\". \
\"Opening\" here means run with xdg-open.",
			.short_help_descr = "Create link",
			.args_descr = "URL/PATH",
			.examples_str = "\
note ,\"Open ddg\" Link=\"https://duckduckgo.com\"\
	Make a link that opens duckduckgo using xdg-open.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_LIST_SHORTCUTS] = {
		.keyword = {
			.long_name = "List-shortcuts",
			.short_name = 0,
			.long_help_descr = "\
List the shortcut notefiles, that is notefiles in, probably ~/.local/share/note. \
Shortcut notefiles are the ones you'll probably want to use since you don't \
have to type much to use them.",
			.short_help_descr = "List shortcuts",
			.args_descr = 0,
			.examples_str = "\
note List-shortcuts\
	List the shortcut notefiles.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_LOCK] = {
		.keyword = {
			.long_name = "Locked",
			.short_name = 0,
			.long_help_descr = "\
Make a note \"locked\".",
			.short_help_descr = "Make a note locked",
			.args_descr = 0,
			.examples_str = "\
note n-1 Locked\
	Lock the last note in the selection.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_NAG] = {
		.keyword = {
			.long_name = "Nag",
			.short_name = 0,
			.long_help_descr = "\
Prints a nagging message if the selected notes don't have PROPERTY. This one \
will probably go away. Meant to be used in autocmds.",
			.short_help_descr = "Dunno, really",
			.args_descr = "PROPERTY NAME",
			.examples_str = "\
note has=on Nag=interval\
	Will nag you if notes that have \"on\" don't also have \"interval\".",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_NAME] = {
		.keyword = {
			.long_name = "Name",
			.short_name = 0,
			.long_help_descr = "\
Provide a name for the note(s). This isn't implemented yet. Later (one day) it might \
(foolishly) be used for complicted-ish \"scripting\".",
			.short_help_descr = "Not implemented",
			.args_descr = "NAME",
			.examples_str = nullptr,
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_NOP] = {
		.keyword = {
			.long_name = "Nop",
			.short_name = 0,
			.long_help_descr = "\
No operation: do nothing. Useful when your cmd consists of several statements \
and you don't want to auto-print at the end of each.",
			.short_help_descr = "Do nothing",
			.args_descr = 0,
			.examples_str = "\
note n0,1 Nop \\; l0 n0 Remove \\; l0 n1 Sfile my-other-file\
	The first statement creates a list and doesn't print it, because of \"Nop\". \
The second removes the first one in that list (got with \"l0\"); the third moves \
the second in that list to my-other-file.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_ON] = {
		.keyword = {
			.long_name = "On",
			.short_name = 'O',
			.long_help_descr = "\
Say when the notes in your view take place.",
			.short_help_descr = "Set notes' \"ons\"",
			.args_descr = "TIME STR",
			.examples_str = "\
note ,\"Visit your mother\" On=1dT10\
	Make a note about visiting your mother at 10 AM tomorrow morning.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_OPEN] = {
		.keyword = {
			.long_name = "Open",
			.short_name = 0,
			.long_help_descr = "\
Open (using xdg-open) your link (see \"Link\").",
			.short_help_descr = "Open \"Link\"-defined link",
			.args_descr = 0,
			.examples_str = "\
note n-1 Open\
	Opens the links in the last note\n\
note n-1 : ylink n0 Open\
	Opens the first link in the last note\n\
note link n-1 : ylink n0 Open\
	Opens the first link in the last note with a link.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_POSTPONE] = {
		.keyword = {
			.long_name = "Postpone",
			.short_name = 0,
			.long_help_descr = "\
Postpone your viewed notes. The difference between this and just changing \
the \"on\" is when your note repeats (check \"Interval\"). If you change the on, \
the time it repeats changes. But with postpone, that next repetition will remain \
at the original time. Note that your TIME STR is \"absolute\", so if you do \
Postpone=1d, it means postpone by one day counting from now. If you want to count \
from \"on\" or any other time thing, you can refer to them with a dollar sign \
before their names. Bear in mind the note needs an on for you to be able to postpone \
it.",
			.short_help_descr = "Push back an \"on's\" time once",
			.args_descr = "TIME STR",
			.examples_str = "\
note n-1 Postpone=1d\
	Postpone the last note to one after _now_\n\
note n-1 Postpone='$on + 1d'\
	Postpone the last note by one day, ie, its \"on\" plus one day.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_PRINT_TAGS] = {
		.keyword = {
			.long_name = "Print-tags",
			.short_name = 0,
			.long_help_descr = "\
Print just the tags the notes in your view have, in a pretty straightforward \
way.",
			.short_help_descr = "Print tags in view",
			.args_descr = 0,
			.examples_str = "\
note Tags\
	Print all your default notefile's tags\n\
note sshortcut-file n-10.. Tags\
	Print the tags in the last ten notes in the shortcut notefile \"shortcut \
file\".",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_PRIORITY] = {
		.keyword = {
			.long_name = "Priority",
			.short_name = 0,
			.long_help_descr = "\
Assign a \"priority\". Just another thing to search but (using \"priority\", \
in this case).",
			.short_help_descr = "Set notes' priorities",
			.args_descr = "INTEGER 0...10",
			.examples_str = "\
note ,\"Your new note\" Priority=5\
	Make a new note with a priority of 5.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_QUIT] = {
		.keyword = {
			.long_name = "Quit",
			.short_name = 'Q',
			.long_help_descr = "\
Quits. I think this is here for programs that use the library.",
			.short_help_descr = "Quit",
			.args_descr = 0,
			.examples_str = "\
note Quit\
	Presumably just quits the notes and then quits.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_REMIND] = {
		.keyword = {
			.long_name = "Remind",
			.short_name = 0,
			.long_help_descr = "\
\"Remind\".",
			.short_help_descr = "Remind",
			.args_descr = 0,
			.examples_str = "\
note n0...10 Remind\
	Remind about any notes that have ons within the first 11 notes.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_REMINDER] = {
		.keyword = {
			.long_name = "Reminder",
			.short_name = 'M',
			.long_help_descr = "\
Set a \"remind duration\", a time before which you will be reminded about a note \
that has an on. Right now I'm not sure what this while mean. It will \
probably mean that, effectively, a note's \"on\" is extended to plug \
the duration you give for \"reminder\". I plan to add notifications to \
Note: when I do, \"on\" plus \"reminder\" is when you'll be sent a \
notification or email or whatever. FIXME: perhaps make it do something \
with \"before\", too.",
			.short_help_descr = "Set notes' \"remind durations\"",
			.args_descr = "DURATION",
			.examples_str = "\
note ,\"New note\" On=1d Reminder=1d\
	Will remind you one day before \"New note's\" \"on\"\n\
note ,\"New note\" On=1d Reminder=-1d\
	Remind you one day after \"New note's\" \"on\".",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_REMOVE] = {
		.keyword = {
			.long_name = "Remove",
			.short_name = 'R',
			.long_help_descr = "\
Remove notes from their notefiles, ie delete them, kill them utterly.",
			.short_help_descr = "Remove notes",
			.args_descr = 0,
			.examples_str = "\
note tag=whatever Remove\
	Remove notes with the tag \"whatever\".",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_SCOPY] = {
		.keyword = {
			.long_name = "Scopy",
			.short_name = 0,
			.long_help_descr = "\
Copy notes in view to SHORTCUT NOTEFILE.",
			.short_help_descr = "Copy notes to sfile",
			.args_descr = "BASENAME",
			.examples_str = "\
note n-1 Scopy=your-other-shortcut-file.nxt\
	Copies the last note to \"Scopy=your-other-shortcut-file.nxt\", a file in your \
\"shortcut directory\".",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_SCOPY_CREATE] = {
		.keyword = {
			.long_name = "Scopycreate",
			.short_name = 0,
			.long_help_descr = "\
Copy notes in view to SHORTCUT NOTEFILE, creating the notefile if it doesn't exist.",
			.short_help_descr = "Copy to sfile, create",
			.args_descr = "BASENAME",
			.examples_str = "\
note n-1 Scopycreate=your-possibly-new-shortcut-file.nxt\
	Copies the last note to \"Scopy=your-possibly-new-shortcut-file.nxt\", a file in your \
\"shortcut directory\", which might not exist until you do it.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_SFILE] = {
		.keyword = {
			.long_name = "Sfile",
			.short_name = 'S',
			.long_help_descr = "\
Move notes in view to SHORTCUT NOTEFILE. Use \"Sfilecreate\" if you need to \
make a new one.",
			.short_help_descr = "Move notes to sfile",
			.args_descr = "BASENAME",
			.examples_str = "\
note n-1 Sfile=your-other-shortcut-file.nxt\
	Move the last note to \"your-other-shortcut-file.nxt\", a file in your \
\"shortcut directory\".",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_SFILE_CREATE] = {
		.keyword = {
			.long_name = "Sfilecreate",
			.short_name = 0,
			.long_help_descr = "\
Move notes in view to SHORTCUT NOTEFILE, creating it if necessary. Use \"Sfile\" \
mostly, so you don't accidentally create notefiles.",
			.short_help_descr = "Move notes to sfile",
			.args_descr = "BASENAME",
			.examples_str = "\
note n-1 Sfilecreate=your-probably-new-shortcut-file.nxt\
	Move the last note to \"your-probably-new-shortcut-file.nxt\", in your \
shortcut directory, creating the file if it doesn't exist",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_STOP_REPEATING] = {
		.keyword = {
			.long_name = "Stop-repeating",
			.short_name = 'H',
			.long_help_descr = "\
Specify a point after which a repeating note -- ie one that has an \"on\" and \
and \"interval\" -- should stop repeating.",
			.short_help_descr = "Set notes' \"stop-repeatings\"",
			.args_descr = "TIME STR",
			.examples_str = "\
note n-1 Stop-repeating=3d\
	Stop the last note (let's pretend it's like \"On=1d Interval=1d\") from \
repeating, after three days have passed.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_SUPER_LOCK] = {
		.keyword = {
			.long_name = "Super-locked",
			.short_name = 0,
			.long_help_descr = "\
Mark a view as \"super-locked\".",
			.short_help_descr = "Set notes' \"super-lockeds\"",
			.args_descr = 0,
			.examples_str = "\
note n-1 Super-locked\
	Makes the last note Super-locked.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_TAG] = {
		.keyword = {
			.long_name = "Tag",
			.short_name = 'T',
			.long_help_descr = "\
Give the notes a tag.",
			.short_help_descr = "Set notes' tags",
			.args_descr = "TAG NAME",
			.examples_str = "\
note ,\"Cheese\" Tag=shopping\
	Make a note with the tag \"shopping\"\n\
note n-1 Tag=\"Cheese-making\"\
	Add the tag \"Cheese-making\" to the last note.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_UNSET] = {
		.keyword = {
			.long_name = "Unset",
			.short_name = 'U',
			.long_help_descr = "\
Clear a property. This obviates some commands.",
			.short_help_descr = "Unset notes' properties",
			.args_descr = "PROPERTY NAME",
			.examples_str = "\
note n-1 Unset=on\
	Unset the \"on\" of the last note. After this the note will have no \"on\".",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_UPDATE] = {
		.keyword = {
			.long_name = "Update",
			.short_name = 0,
			.long_help_descr = "\
Force autocmds to be run and the notes to be written back to the notefiles. \
Because when printing autocmds aren't written and notes aren't written back.",
			.short_help_descr = "Run autocmds and save",
			.args_descr = 0,
			.examples_str = "\
note Update\
	Forces autocmds, writes, etc.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_WRITE] = {
		.keyword = {
			.long_name = "Write",
			.short_name = 'W',
			.long_help_descr = "\
Forces a write. I think this is here for programs that use the library.",
			.short_help_descr = "Write",
			.args_descr = 0,
			.examples_str = "\
note Write\
	Presumably just write the notes.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKA_WRITE_QUIT] = {
		.keyword = {
			.long_name = "WQ",
			.short_name = 0,
			.long_help_descr = "\
Forces a write and then quits. I think this is here for programs that use the library.",
			.short_help_descr = "Write and quit",
			.args_descr = 0,
			.examples_str = "\
note WQ\
	Presumably just write the notes and then quits.",
		},
		.type = CT_TYPE_ACTION,
	},
	[CTKS_COMPLETE_ALL] = {
		.keyword = {
			.long_name = "\x01",
			.short_name = 0,
			.long_help_descr = "\
This is a special token that means \"complete all\". It's not meant to \
by typed by you.",
			.short_help_descr = "Don't look",
			.args_descr = 0,
			.examples_str = nullptr,
		},
		.type = CT_TYPE_SPECIAL,
		.dont_offer_as_completion = true,
	},
	[CTK_AND] = {
		.keyword = {
			.long_name = "and",
			.short_name = 0,
			.long_help_descr = "\
It's \"and\", just like \"find\"s and. And just like find, you don't need to \
use it. \"and\" is the default.",
			.short_help_descr = "And operands",
			.args_descr = 0,
			.examples_str = "\
note /this/ and tthat\
	Prints a note with \"this\" in the body and the tag \"that\"\n\
note /this/ tthat\
	Same.",
		},
		.type = CT_TYPE_OPERATOR,
	},
	[CTK_ANNOTATION] = {
		.keyword = {
			.long_name = "annotation",
			.short_name = 'a',
			.long_help_descr = "\
Matches notes with annotations that match REGEX.",
			.short_help_descr = "Add annotation to notes",
			.args_descr = "REGEX",
			.examples_str = "\
note annotation=\"whatever\"\
	Matches notes with annotations with \"whatever\" in them.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_ATTACHMENT] = {
		.keyword = {
			.long_name = "attachment",
			.short_name = 0,
			.long_help_descr = "\
Matches notes with attachments that match REGEX.",
			.short_help_descr = "Match notes by annotation",
			.args_descr = "REGEX",
			.examples_str = "\
note attachment=\"my-file\"\
	Matches notes with attachments with \"my-file\" in them.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_BEFORE] = {
		.keyword = {
			.long_name = "before",
			.short_name = 'b',
			.long_help_descr = "\
Matches notes whose \"before\" are between TIME RANGE.",
			.short_help_descr = "Match notes by \"before\".",
			.args_descr = "TIME STR",
			.examples_str = "\
note before=-0d..1dT10\
	Matches notes whose befores are between now and 10 tomorrow.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_CHANGED] = {
		.keyword = {
			.long_name = "changed",
			.short_name = 0,
			.long_help_descr = "\
Matches notes that have been changed. For use in autocmds and also in \
the TUI. There, if you want to know which notes you've changed you can \
just type \"changed\". If you didn't want to see that note as changed \
after that call \"Unset changed\" on it",
			.short_help_descr = "Match changed notes",
			.args_descr = 0,
			.examples_str = "\
note changed :c\
	That'd print notes that have changed, and give you a confirmation prompt. \
It wouldn't stop the note being changed, though. Later, perhaps there will \
be a way.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_CLOSE_BRACKET] = {
		.keyword = {
			.long_name = nullptr,
			.short_name = ')',
			.long_help_descr = "\
Close bracket; for grouping expressions.",
			.short_help_descr = "Close bracket",
			.args_descr = 0,
			.examples_str = "\
note \\( n0 \\)\
	Print the first note.",
		},
		.type = CT_TYPE_SPECIAL,
	},
	[CTK_COPY] = {
		.keyword = {
			.long_name = "copy",
			.short_name = 0,
			.long_help_descr = "\
Matches notes that have been copied. Presumably there's no need for \"moved\". \
Presuambly \"new\" and \"remove\" can get you there. We'll see. I'm calling it \
\"copy\" for now even though it should be \"copied\". That's to preserve the \
conventioin where you have a lowercase filter and a corrosponding capital-case \
Action. Having said this, Copy would match notes moved by Sfile, File, and Scopy, \
too.",
			.short_help_descr = "Matches copied notes",
			.args_descr = 0,
			.examples_str = "\
note copied has=on not has=interval Interval=1d :c\
	Makes notes that have ons but don't have intervals have an interval of 1d. \
Also (\":c\") will y/n asked you if you really want to do it\n\
note Type=autocmd ,\"new has=on not has=interval Interval=1d :c\"\
	Make an autocmd with the above cmd in it.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_CREATION] = {
		.keyword = {
			.long_name = "creation",
			.short_name = 'c',
			.long_help_descr = "\
Match notes with creation dates between TIME RANGE.",
			.short_help_descr = "Match notes by \"creation\"",
			.args_descr = "TIME STR",
			.examples_str = "\
note creation=-6y..-1y\
	Matches notes whose creation dates lie between 6 years ago and one \
year ago. There's no way (yet) to specify \"from the beginning of time\".",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_DONE] = {
		.keyword = {
			.long_name = "done",
			.short_name = 'd',
			.long_help_descr = "\
Match notes that are \"done\".",
			.short_help_descr = "Match \"done\" notes",
			.args_descr = 0,
			.examples_str = "\
note done Remove\
	Remove notes that are done.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_EFFORT] = {
		.keyword = {
			.long_name = "effort",
			.short_name = 0,
			.long_help_descr = "\
Match notes whose efforts are between RANGE. But note ranges are \
exclusive, so if you want to match 10, you need to do something like 5..11.",
			.short_help_descr = "Match notes by effort",
			.args_descr = "INTEGER 0...10",
			.examples_str = "\
note effort=4..11\
	Match notes whose efforts are between 4 and 11 exclusive.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_ENDING] = {
		.keyword = {
			.long_name = "ending",
			.short_name = 0,
			.long_help_descr = "\
Matches notes whose \"endings\" are between TIME RANGE.",
			.short_help_descr = "Match notes by \"ending\"",
			.args_descr = "TIME STR",
			.examples_str = "\
note ending=-0d..1dT10\
	Matches notes whose endings are between now and 10 tomorrow.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_EQUALS] = {
		.keyword = {
			.long_name = nullptr,
			.short_name = '=',
			.long_help_descr = "\
Equals; used with long names, like \"on=1d\". They behave like they do \
in cli programs: you don't need them with options that have required \
arguments, but you do if their arguents are optional.",
			.short_help_descr = "It's just equals",
			.args_descr = 0,
			.examples_str = "\
note tthis-tag Smove=these-notes\
	Moves all notes with tags matching \"this-tag\" to shortcut notefile \
\"these-notes.nxt\"\n\
note tthis-tag Smove these-notes\
	Same. The equals isn't needed.\n\
note tthis-thag Sthese-notes\
	You don't need to use a space when using the short cmd names.",
		},
		.type = CT_TYPE_SPECIAL,
		.dont_offer_as_completion = true,
	},
	[CTK_EXCEPT] = {
		.keyword = {
			.long_name = "except",
			.short_name = 'x',
			.long_help_descr = "\
Matches notes whose \"excepts\" are between TIME RANGE.",
			.short_help_descr = "Match notes by \"except\"",
			.args_descr = "TIME STR",
			.examples_str = "\
note except=-0d..1dT10\
	Matches notes whose excepts are between now and 10 tomorrow.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_EXCEPT_INTERVAL] = {
		.keyword = {
			.long_name = "except-interval",
			.short_name = 0,
			.long_help_descr = "\
FIXME: Come back to this one. Matches notes whose \"except-intervals\" are between \
TIME RANGE.",
			.short_help_descr = "Match notes by \"except-interval\"",
			.args_descr = "DURATION",
			.examples_str = "\
note except-interval=-0d..1dT10\
	Matches notes whose except-intervals are between now and 10 tomorrow.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_FROM] = {
		.keyword = {
			.long_name = "from",
			.short_name = 0,
			.long_help_descr = "\
Regex-match \"froms\", copied notes' previous notefiles. These match by \
full path. There's no distinction between \"shortcut\" and \"normal\" \
notefiles. Perhaps there will be one day.",
			.short_help_descr = "Match notes by \"from\"",
			.args_descr = "REGEX",
			.examples_str = "\
from \"notes\\.nxt$\"\
	Print notes which were originally in a notefile matching \"notes.nxt\".",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_GREP] = {
		.keyword = {
			.long_name = "grep",
			.short_name = '/',
			.long_help_descr = "\
Match notes whose body matches REGEX.",
			.short_help_descr = "Match notes by body",
			.args_descr = "REGEX",
			.examples_str = "\
note /stuff/ Remove\
	Removes notes whose bodies contain the string \"stuff\".",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_HAS] = {
		.keyword = {
			.long_name = "has",
			.short_name = 'h',
			.long_help_descr = "\
Match notes which have the property PROPERTY NAME.",
			.short_help_descr = "Match notes by property",
			.args_descr = "PROPERTY NAME",
			.examples_str = "\
note has=on\
	Print notes that have an \"on\"\n\
note not has=on\
	Print notes that don't have an \"on\".",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_INTERVAL] = {
		.keyword = {
			.long_name = "interval",
			.short_name = 'i',
			.long_help_descr = "\
Matches notes whose \"intervals\" are between TIME RANGE.",
			.short_help_descr = "Match notes by \"interval\"",
			.args_descr = "TIME STR",
			.examples_str = "\
note interval=-0d..1dT10\
	Matches notes whose next intervals are between now and 10 tomorrow.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_LAST_REMINDED] = {
		.keyword = {
			.long_name = "last-reminded",
			.short_name = 0,
			.long_help_descr = "\
Matches notes whose \"last-remindeds\" are between TIME RANGE.",
			.short_help_descr = "Match notes by \"last-reminded\"",
			.args_descr = "TIME STR",
			.examples_str = "\
note last-reminded=-0d..1dT10\
	Matches notes whose last-remindeds are between now and 10 tomorrow.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_LINK] = {
		.keyword = {
			.long_name = "link",
			.short_name = 0,
			.long_help_descr = "\
Match notes with links matching URL OR FILE.",
			.short_help_descr = "Match notes by \"link\"",
			.args_descr = "URL/PATH",
			.examples_str = "\
note link=duckduckgo\
	Matches notes with links containing \"duckduckgo\".",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_LOCKED] = {
		.keyword = {
			.long_name = "locked",
			.short_name = 0,
			.long_help_descr = "\
Matches notes that are locked.",
			.short_help_descr = "Match locked notes",
			.args_descr = 0,
			.examples_str = "\
note locked Super-locked\
	Makes all locked notes Super-locked.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_MOD_CONFIRM] = {
		.keyword = {
			.long_name = ":confirm",
			.short_name = 'c',
			.long_help_descr = "\
Ask you to confirm *changes*. You can use this with \
you're doing something risky. It's best used for autocmds that do \
something that makes you nervous. \
Confirming happens automatically if the item you're \
dealing with is locked. Not that this is meant to confirm *changes*. \
You could use it in an autocmd to discourage the user from some action.\
Right now, though, probably the best way to do that is with Quit and :msg.",
			.short_help_descr = "Confirm changes",
			.args_descr = 0,
			.examples_str = "\
note n-1 Remove :c\
	Remove the note with a confirmation.",
		},
		.type = CT_TYPE_MOD,
	},
	[CTK_MOD_EDIT] = {
		.keyword = {
			.long_name = ":edit",
			.short_name = 'e',
			.long_help_descr = "\
Edit the view in VISUAL, if defined, else EDITOR, else it'll ask you \
to enter the name of the editor you want to use. Overrides :print.",
			.short_help_descr = "Edit in EDITOR",
			.args_descr = 0,
			.examples_str = "\
note /this :edit\
	Edit notes whose bodies match \"this\"\
note /this Remove :edit\
	Remove notes whose bodies match \"this\" then open them in the editor.",
		},
		.type = CT_TYPE_MOD,
	},
	[CTK_MOD_FORCE] = {
		.keyword = {
			.long_name = ":force",
			.short_name = 'f',
			.long_help_descr = "\
Ignore \"locked\". To ignore \"super-locked\", see \":super-force\".",
			.short_help_descr = "Ignore \"locked\"",
			.args_descr = 0,
			.examples_str = "\
note yA ,\"remove Sdeleted :force\"\
	Make an autocmd that moves deleted notes to a shortcut notefile called \
\"deleted\" -- and don't prompt if the notes are locked.",
		},
		.type = CT_TYPE_MOD,
	},
	[CTK_MOD_MSG] = {
		.keyword = {
			.long_name = ":msg",
			.short_name = 'm',
			.long_help_descr = "\
Outputs a message along with whatever action you're doing. For use in autocmds.",
			.short_help_descr = "Output message",
			.args_descr = "TEXT",
			.examples_str = "\
new sinitial yn Quit :msg=\"You've ended up with a note in initial.nxt. That's \
probably the result of a bug. In any case you don't want it there\"\
	If that was an autocmd and the user somehow put a note in initial.nxt, note would \
exit after printing that message.",
		},
		.type = CT_TYPE_MOD,
	},
	[CTK_MOD_NORMAL] = {
		.keyword = {
			.long_name = ":normal",
			.short_name = 'n',
			.long_help_descr = "\
Confirm \"normally\", that is to say when the item you're operating on is \
super-locked or locked. I'm not sure what use this will be in the CLI, \
though I suppose we'll find out, but it's necessary for the TUI, to use after \
you do --confirm or --force.",
			.short_help_descr = "Confirm \"normally\"",
			.args_descr = 0,
			.examples_str = "\
note :normal\
	Really you'd do --normal from inside the TUI.",
		},
		.type = CT_TYPE_MOD,
	},
	[CTK_MOD_PRINT] = {
		.keyword = {
			.long_name = ":print",
			.short_name = 0,
			.long_help_descr = "\
Prints properties according to SPECIFIER, a comma-separated list of \
properties to print and not print. The property names are the same \
as normal, eg \"on\" or \"o\" for \"on\". A minus before the name \
means don't print. If the first item is a \"not print\" (it's preceded \
by a minus), everything will be printed except those you say not to. If \
the first item is a \"print\" (ie, it's not preceded by a minus), nothing \
will printed except what you say to. The header will always be printed, \
unless you specify -header.",
			.short_help_descr = "Specify props to print",
			.args_descr = "PROPERTY CSV",
			.examples_str = "\
note Print=-tags\
	Print everything except tags\n\
note Print=tags\
	Print nothing except the header and the tags\n\
note Print=-props\
	Print everything except props\n\
note Print=-str-props\
	Print everything except \"str props\" -- annotations, etc.",
			.args_are_optional = true,
		},
		.type = CT_TYPE_MOD,
	},
	[CTK_NAME] = {
		.keyword = {
			.long_name = "name",
			.short_name = 0,
			.long_help_descr = "\
Matches notes whose names are NAME. Not implemented yet.",
			.short_help_descr = "Not implemented yet",
			.args_descr = "NAME",
			.examples_str = "\
note name=my-pretend-function\
	Matches that whose names are my-pretend-function. Though I should say \
\"the note whose name is\", because names will have to be unique, else there's \
no point.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_NEW] = {
		.keyword = {
			.long_name = "new",
			.short_name = 0,
			.long_help_descr = "\
Matches new notes. Only useful in autocmds. Useful if you only want to set some  \
property once.",
			.short_help_descr = "Match new notes",
			.args_descr = 0,
			.examples_str = "\
note new has=on not has=interval Interval=1d :c\
	Makes notes that have ons but don't have intervals have an interval of 1d. \
Also (\":c\") will y/n asked you if you really want to do it\n\
note Type=autocmd ,\"new has=on not has=interval Interval=1d :c\"\
	Make an autocmd with the above cmd in it.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_NONE] = {
		.keyword = {
			.long_name = "none",
			.short_name = 0,
			.long_help_descr = "\
Matches nothing.",
			.short_help_descr = "Match nothing",
			.args_descr = 0,
			.examples_str = "\
note has=on none\
	Matches nothing. Can't remember why I wanted this feature. I'm not saying it's \
a bad one, only that my past self was cleverer than my current.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_NOT] = {
		.keyword = {
			.long_name = "not",
			.short_name = 0,
			.long_help_descr = "\
Negates an \"and\" or \"or\" or \"()\".",
			.short_help_descr = "Negate expr",
			.args_descr = 0,
			.examples_str = "\
note /this/ not tthat\
	Matches notes whose bodies match \"this\" and that don't have tags with \
\"that\" in them\n\
note not n-1\
	Prints all notes except the last. note behaves as if it starts with a list \
of all possible notes. Hence you can negate them all with \"not\" at the start.",
		},
		.type = CT_TYPE_OPERATOR,
	},
	[CTK_FILE] = {
		.keyword = {
			.long_name = "file",
			.short_name = 'f',
			.long_help_descr = "\
Make a list consisting of all notes in the notefile referred to with PATH. The file \
must exist. If it might not, use \"filecreate\".",
			.short_help_descr = "Match file",
			.args_descr = "PATH",
			.examples_str = "\
note file my-notefile.nxt\
	Print all notes in my-notefile.nxt\n\
note file my-notefile.nxt n-1 F my-other-notefile.nxt\
	Move the last note in my-notefile.nxt to my-other-notefile.nxt.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_FILE_CREATE] = {
		.keyword = {
			.long_name = "filecreate",
			.short_name = 0,
			.long_help_descr = "\
Make a list consisting of all notes in the notefile referred to with PATH, creating \
it if necessary. That's a weird thing to say, of course, since if the file doesn't \
exist there will be no notes. But the file filters in this program are what \
load in notefiles, so you need to use one of them. And if you're making a new notefile \
you need to use this one.",
			.short_help_descr = "Match file, loading",
			.args_descr = "PATH",
			.examples_str = "\
note filecreate my-notefile.nxt ,\"Here's a body for you\"\
	Create \"my-notefile.nxt\" and put its first note in\n\
note filec my-notefile.nxt ,\"Here's a body for you\"\
	Create \"my-notefile.nxt\" and put its first note in",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_MATCH_FILE] = {
		.keyword = {
			.long_name = "matchfile",
			.short_name = 0,
			.long_help_descr = "\
Make a list consisting of all notes in the notefile referred to with PATH but don't \
load the notefile if it doesn't exist. You can use this in autocmds. There's no need \
to load files if it isn't necessary. Using this also will make debugging Note a bit \
easier, since the items loaded will always be the actual items you, the user (me) \
loaded. \
And I think this might be used in the TUI. You can begin a command with it, to  \
filter down views you're already looking at by file. To do that, you need this, \
because \"file\" discards the previous view.",
			.short_help_descr = "Match file",
			.args_descr = "PATH",
			.examples_str = "\
note file my-notefile.nxt matchfile notefile\
	Print all notes in my-notefile.nxt. You don't need the \"matchfile\" for this, and \
in fact almost never *need* to use matchfile. It's just a little more efficient",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_ON] = {
		.keyword = {
			.long_name = "on",
			.short_name = 'o',
			.long_help_descr = "\
Matches notes whose \"ons\" are between TIME RANGE. Actually, this does something \
a little more complex. But I can't remember what it is.",
			.short_help_descr = "Match by \"on\"",
			.args_descr = "TIME STR",
			.examples_str = "\
note on=-0d..1dT10\
	Matches notes whose ons are between now and 10 tomorrow.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_OPEN_BRACKET] = {
		.keyword = {
			.long_name = nullptr,
			.short_name = '(',
			.long_help_descr = "\
Open bracket; for grouping expressions.",
			.short_help_descr = "Begin expr group",
			.args_descr = 0,
			.examples_str = "\
note \\( n0 \\)\
	Print the first note\n\
note n0 or \\( n1 and n1 \\)\
	print the first and second notes.",
		},
		.type = CT_TYPE_SPECIAL,
	},
	[CTK_OR] = {
		.keyword = {
			.long_name = "or",
			.short_name = 0,
			.long_help_descr = "\
It's like find's \"or\". This or that, you know.",
			.short_help_descr = "\"or\"",
			.args_descr = 0,
			.examples_str = "\
note /this/ or /that/\
	Match notes whose bodies contain \"this\" or \"that\".",
		},
		.type = CT_TYPE_OPERATOR,
	},
	[CTK_PARENT] = {
		.keyword = {
			.long_name = "parent",
			.short_name = 'p',
			.long_help_descr = "\
Matches the first notefile called \"notes.nxt\" upwards from the current \
eirectory. There's no corrosponding Parent action; see local for a justification. \
Though to be honest, in the time it's taken me to justify myself I could have \
made it so I didn't need a justification.",
			.short_help_descr = "Match \"notes.nxt\" upwards",
			.args_descr = 0,
			.examples_str = "\
note p\
	Might match ../../notes.nxt.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_POSTPONE] = {
		.keyword = {
			.long_name = "postpone",
			.short_name = 0,
			.long_help_descr = "\
Matches notes whose \"postpones\" are between TIME RANGE.",
			.short_help_descr = "Match by \"postpone\"",
			.args_descr = "TIME STR",
			.examples_str = "\
note postpone=-0d..1dT10\
	Matches notes whose postpones are between now and 10 tomorrow.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_PREV_LIST] = {
		.keyword = {
			.long_name = "view",
			.short_name = 'v',
			.long_help_descr = "\
Matches the view produced in statement number NUMBER.",
			.short_help_descr = "Match prev view",
			.args_descr = "INTEGER",
			.examples_str = "\
note n0..10 Remove; l0 /whatever/\
	Removes the first ten notes; prints those in that ten whose bodies match \
\"whatever\".",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_PRIORITY] = {
		.keyword = {
			.long_name = "priority",
			.short_name = 'p',
			.long_help_descr = "\
Match notes whose priorities are between RANGE.",
			.short_help_descr = "Match by \"priority\"",
			.args_descr = "INTEGER 0...10",
			.examples_str = "\
note priority=4..10\
	Match notes whose priorities are between 4 and 10.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_PROP] = {
		.keyword = {
			.long_name = "prop",
			.short_name = 0,
			.long_help_descr = "\
Indicates that the rest of the statement refers specifically to \"properties\".",
			.short_help_descr = "Begin referring to props",
			.args_descr = 0,
			.examples_str = "\
note n-1 prop ylink n0\
	Print the first link in the last note in the file.",

			// Don't ask me why the type is this. I think there was a
			// reason.
		},
		.type = ':',
	},
	[CTK_RELATIVE_RANGE] = {
		.keyword = {
			.long_name = "relative-number",
			.short_name = 'n',
			.long_help_descr = "\
Match the already-matched notes by INT LIST.",
			.short_help_descr = "Match by view range",
			.args_descr = "INT RANGE",
			.examples_str = "\
note /this/ n-1\
	Match the last of the notes whose bodies contain \"this\".",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_REMINDER] = {
		.keyword = {
			.long_name = "reminder",
			.short_name = 'm',
			.long_help_descr = "\
Match reminds.",
			.short_help_descr = "Match be \"reminder\"",
			.args_descr = "DURATION",
			.examples_str = "\
note remind=0d..10d\
	Print notes whose reminders are within now and ten days.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_REMOVE] = {
		.keyword = {
			.long_name = "remove",
			.short_name = 'r',
			.long_help_descr = "\
Matches notes that are being removed. Only useful in autocmds.",
			.short_help_descr = "Match removed notes",
			.args_descr = 0,
			.examples_str = "\
remove Sfile deleted.nxt\
	Move removed notes to the shortcut file \"deleted.nxt\".",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_SEMICOLON] = {
		.keyword = {
			.long_name = nullptr,
			.short_name = ';',
			.long_help_descr = "\
Statement separator.",
			.short_help_descr = "Statement separator",
			.args_descr = 0,
			.examples_str = "\
note n0 Remove \\; n0\
	Removes the first note, then prints it.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_SFILE] = {
		.keyword = {
			.long_name = "sfile",
			.short_name = 's',
			.long_help_descr =  "\
Make a list consisting of all notes in SHORTCUT NOTEFILE. \"Shortcut notefiles\" are \
just notefiles in ~/.local/share/note that you refer to as only their basename, (and \
optionally without suffix). Note also loads two shortcut notefiles, \"initial.nxt\" \
and \"notes.nxt\". If you specify one, it doesn't load \"notes.nxt\".",
			.short_help_descr = "Match by \"sfile\"",
			.args_descr = "BASENAME",
			.examples_str = "\
note sfile calendar-dates.nxt on=-1d..1d\
	List all notes in shortcut file \"calendar-dates.nxt\" whose \"ons\" lie between \
one day ago and one day in the future.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_SFILE_CREATE] = {
		.keyword = {
			.long_name = "sfilecreate",
			.short_name = 0,
			.long_help_descr = "\
Make a list consisting of all notes in the notefile with BASENAME in your \
shortcut notefiles directory. That's a weird thing to say, of course, since if the \
file doesn't \
exist there will be no notes. But the file filters in this program are what \
load in notefiles, so you need to use one of them. And if you're making a new \
shortcut notefile \
you need to use this one.",
			.short_help_descr = "Match by \"sfile\"",
			.args_descr = "BASENAME",
			.examples_str = "\
note sfilecreate my-shortcut-notefile.nxt ,\"Here's a body for you\"\
	Create \"my-shortcut-notefile.nxt\" in your shortcut notefiles dir \
and put its first note in\n\
note sfilec my-shortcut-notefile.nxt ,\"Here's a body for you\"\
	Create \"my-shortcut-notefile.nxt\" in your shortcut notefiles dir \
and put its first note in",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_MATCH_SFILE] = {
		.keyword = {
			.long_name = "matchsfile",
			.short_name = 0,
			.long_help_descr = "\
See \"matchfile\". Only difference here is we're matching shortcut notefiles, \
so \"hello\" won't match \"/tmp/hello\" unless your shortcut-dir is \"/tmp\" \
(or unless there's a bug in this program)",
			.short_help_descr = "Match file",
			.args_descr = "PATH",
			.examples_str = "\
note sfile my-shortcut-notefile.nxt matchsfile notefile\
	Print all notes in the shortcut notefile my-notefile.nxt. You don't need the \
\"matchsfile\" for this, and \
in fact almost never *need* to use matchfile. It's just a little more efficient",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_STOP_REPEATING] = {
		.keyword = {
			.long_name = "stop-repeating",
			.short_name = 0,
			.long_help_descr = "\
Matches notes whose \"stop-repeatings\" are between TIME RANGE.",
			.short_help_descr = "Match by \"stop-repeating\"",
			.args_descr = "TIME STR",
			.examples_str = "\
note stop-repeating=-0d..1dT10\
	Matches notes whose stop-repeatings are between now and 10 tomorrow.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_SUPER_LOCKED] = {
		.keyword = {
			.long_name = "super-locked",
			.short_name = 0,
			.long_help_descr = "\
Matches notes that are super-locked.",
			.short_help_descr = "Match by \"super-locked\"",
			.args_descr = 0,
			.examples_str = "\
note super-locked Remove\
	Removes notes that are super-locked.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_TAG] = {
		.keyword = {
			.long_name = "tag",
			.short_name = 't',
			.long_help_descr = "\
Match notes whose tags match REGEX.",
			.short_help_descr = "Match by tag",
			.args_descr = "REGEX",
			.examples_str = "\
note tthis\
	Print notes whose tags match \"this\".",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_TYPE] = {
		.keyword = {
			.long_name = "type",
			.short_name = 'y',
			.long_help_descr = "\
Matches items whose types match TYPE NAME.",
			.short_help_descr = "Match by \"type\"",
			.args_descr = "TYPE NAME",
			.examples_str = "\
note yautocmd\
	Print all autocmds\n\
note ya\
	Print all autocmds\n\
note yn n0..10\
	Print the first ten notes.",
		},
		.type = CT_TYPE_FILTER,
	},
	[CTK_END]           = {.type = CT_TYPE_SPECIAL},
	[CTK_IMPLICIT_LIST] = {.type = CT_TYPE_SPECIAL},
	[CTK_ERROR]         = {.type = CT_TYPE_SPECIAL},
	[CTK_INT_LIST]      = {.type = CT_TYPE_SPECIAL},
	[CTK_NULL_BYTE]     = {.type = CT_TYPE_SPECIAL},
	[CTK_WORD]          = {.type = CT_TYPE_SPECIAL},
};

