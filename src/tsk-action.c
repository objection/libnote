#define _GNU_SOURCE

#include "tsk-change.h"
#include "n-fs.h"
#include "config.h"
#include "misc.h"
#include "useful.h"
#include "tsk-action.h"
#include "rab.h"
#include "lib/slurp/slurp.h"
#include "get-line.h"
#include "tsk-err.h"
#include "tsk-item.h"
#include "tsk-write.h"
#include "read-notefile.h"
#include "tsk-print.h"
#include "tsk-cmd.h"
#include "notefile.h"
#include "wrap.h"
#include <assert.h>
#include <search.h>
#include <ctype.h>

enum { N_TS_BUF = 64 };

struct saved_item {
	struct tsk_item item;
	enum confirmation_mode confirmation_mode;
};
$make_arr (struct saved_item, saved_items);

int action_err (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor, int cmd_words_idx,
		struct err_info *read_notefile_err_info, struct tsk_statement *statement,
		char *fmt, ...) {

	char out[BUFSIZ];

	char msg_buf[BUFSIZ / 2];
	va_list ap;
	va_start (ap);
	vsnprintf (msg_buf, sizeof msg_buf, fmt, ap);
	va_end (ap);

	if (view_cursor != AE_NO_ITEM_TO_PRINT) {
		auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
		char body_str[ctx->opts->truncate_len * 3 + 5];
		if (item->body)
			get_display_str (ctx, sizeof body_str, body_str,
					&(dso) {.gqesm_mode = GQESO_SURROUND_WITH_QUOTES_ALWAYS},
					item->body);
		else
			sprintf (body_str, "with no body");

		if (item->has & (1 << IP_NEW))
			snprintf (out, sizeof out, "While processing new %s %s: %s",
					ITEM_TYPE_KEYWORDS[item->type].long_name,
					body_str, msg_buf);

		else
			snprintf (out, sizeof out, "While processing %d%s %s matched, %s: %s",
					view_cursor, rab_get_num_suffix (view_cursor),
					ITEM_TYPE_KEYWORDS[item->type].long_name,
					body_str, msg_buf);
	} else
		snprintf (out, sizeof out, "%s", msg_buf);

	struct err_info err_info;
	err_info = get_cmd_err_info (cmd, cmd_words_idx, statement->cmd_words_idx);
	arr_add (&cmd->err_infos, err_info);

	if (read_notefile_err_info)
		arr_add (&cmd->err_infos, *read_notefile_err_info);

	tsk_warn (ctx, cmd, "%s", out);
	return 1;
}

static int get_unformatted_confirm_text_header (tsk_ctx *ctx, tsk_cmd *cmd, size_t n_pr,
		char *pr, struct tsk_item *old_item) {
	FILE *f = fmemopen (pr, n_pr * sizeof *pr, "w");
	fprintf (f, "Proceed with ");
	fprintf (f, "%s", CMD_SOURCE_STRS[cmd->source]);
	char display_str[BUFSIZ];
	struct display_str_opts display_str_opts = {
		.gqesm_mode = GQESO_SURROUND_WITH_QUOTES_ALWAYS
	};
	get_display_str (ctx, sizeof display_str, display_str, &display_str_opts,
			cmd->text);
	fprintf (f, " %s", display_str);

	if (old_item->body) {
		fprintf (f, " on ");
		if (old_item->has & (1 << IP_SUPER_LOCK))
			fprintf (f, "super-locked ");
		else if (old_item->has & (1 << IP_LOCK))
			fprintf (f, "locked ");
		fprintf (f, "%s ", ITEM_TYPE_KEYWORDS[old_item->type].long_name);

		// I'm just getting rid of the truncation for now. I find the
		// confirmations too annoying to read, partly because of how
		// much info it puts out, possibly because of the the body is
		// truncated and so makes no sense.
		display_str_opts.dont_truncate = 1;
		get_display_str (ctx, sizeof display_str, display_str, &display_str_opts,
				old_item->body);
		fprintf (f, "%s", display_str);
	}
	fprintf (f, "?");
	fclose (f);
	pr[n_pr - 1] = 0;
	return 0;
}

static int print_confirm_text_header (tsk_ctx *ctx, tsk_cmd *cmd,
		struct tsk_item *old_item,
		int (*print_fn) (struct tsk_print_ctx *, char *, ...)) {

	int r = 0;

	char unformatted[BUFSIZ];
	get_unformatted_confirm_text_header (ctx, cmd, sizeof unformatted, unformatted,
			old_item);

	struct wrap_opts wo = {};

	// We write the potential progname outside of the wrap call
	// because we don't want it to be in colour.
	if (ctx->opts->prog_name)
		wo.to_subtract_from_first_line = add_text (ctx, "%s: ",
				program_invocation_short_name) + 2;
	char wrapped_body[BUFSIZ];

	wrap (wrapped_body, sizeof wrapped_body, &wo, "%s", unformatted);

	start_colour (ctx, TSK_COLOUR_ERROR_HEADER);

	print_fn (ctx->pc, "%s\n", wrapped_body);

	stop_colour (ctx);

	return r;
}

static void print_confirmation_property_change_line (tsk_ctx *ctx,
		struct tsk_item *saved_item, struct tsk_item *item, enum prop_id prop_id,
		struct tsk_print_ctx *pc, bool user_has_confirmed,
		int (*print_fn) (struct tsk_print_ctx *, char *, ...)) {

	enum { n_was_is = 256 * 2 };
	char was_buf[n_was_is] = {}, is_buf[n_was_is] = {};

	stringify_prop_val (ctx, n_was_is, was_buf, prop_id, prop_id_to_member (saved_item,
				prop_id), saved_item->has, 1);
	stringify_prop_val (ctx, n_was_is, is_buf, prop_id, prop_id_to_member (item, prop_id),
			item->has, 1);

	if (*was_buf)

		// We do truncate twice in this function, once each for the
		// "was" and "is", and then again at the end. I think it's
		// good. If we only truncated at the end we'd be more likely
		// to end up with strings that only showed the "was". Probably
		// you'd want to tweak the truncate lens.
		get_truncated_str (ctx, was_buf, was_buf, n_was_is / 2, 1, 0);
	else
		strcpy (was_buf, "not set");

	if (*is_buf)
		get_truncated_str (ctx, is_buf, is_buf, n_was_is / 2, 1, 0);
	else
		strcpy (is_buf, "not set");

	char out[BUFSIZ];

	// Don't think that maybe we don't want this quote_was stuff. With
	// it we're considering the type of the value. We're just not
	// looking for spaces as we do in get_display_str. Later
	// get_display_str could do different things base on the types or
	// flags. Now that I think of it, no, the stringify functions that
	// should do that. So maybe an option to have them quote their
	// output something like ALWAYS, IF_SPACES, etc.
	struct wrap_opts wrap_opts = {.overall_indent = 4, .second_line_indent_incr = 1};
	if (user_has_confirmed)
		wrap (out, sizeof out, &wrap_opts, "\"%s\" is now %s",
				PROP_KEYWORDS[prop_id].long_name, is_buf);
	else
		wrap (out, sizeof out, &wrap_opts, "\"%s\" is %s; will become %s",
				PROP_KEYWORDS[prop_id].long_name, was_buf, is_buf);

	print_fn (pc, "%s\n", out);
}

int tsk_print_confirmation (tsk_ctx *ctx, tsk_cmd *cmd,
		struct tsk_confirmation *confirmation,
		int (*print_fn) (struct tsk_print_ctx *, char *, ...)) {

	print_confirm_text_header (ctx, cmd, confirmation->old_item, print_fn);
	$fori (i, N_PROPS) {
		if (confirmation->changed_props & (1 << i)) {
			print_confirmation_property_change_line (ctx, confirmation->old_item,
					confirmation->new_item, i, ctx->pc, confirmation->user_confirms,
					print_fn);
		}
	}
	return 0;
}

static int print_tags (tsk_ctx *ctx, int view_cursor, char *,
		struct tsk_statement *statement) {
	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	struct tsk_strs strs = {};

	if (item->type != IT_NOTE)
		return 0;

	arr_each (item->props, _) {
		if (_->type != IT_TAG)
			continue;

		// This means that you only get unique tags. Might be better
		// to just spit them all out.
		if (tsk_index_of_str_match (_->body, strs.d, strs.n) == -1) {
			if (strpbrk (_->body, " \t\n"))
				tsk_plain_msg (ctx, "\"%s\"\n", _->body);
			else
				tsk_plain_msg (ctx, "%s\n", _->body);
			arr_add (&strs, _->body);
		}
		break;
	}

	if (strs.d)
		free (strs.d);
	return 0;
}

static int action_unset (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece, struct tsk_statement *statement) {

	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	if (item->type != IT_NOTE)
		return 0;

	int desired;
	char err_buf[TSK_MAX_ERR_BUF];
	assert (action_piece.str);
	int r = get_bitfield_from_comma_list (action_piece.str,
			strchr (action_piece.str, 0), N_PROPS, PROP_KEYWORDS,
			TSK_CMD_KEYWORD_LONG_NAME_GETTER, err_buf, &desired);
	if (r) {
		action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement,
				"Couldn't Unset: %s", err_buf);
		return r;
	}

	// We unset things by simply remove with "has" entry. We don't
	// bother deallocating or something. I suppose this means that
	// nothing ever gets deallocated even in the TUI, which might be a
	// problem.
	item->has &= ~desired;
	return 0;
}

// This and command are basically the same
static int action_open (tsk_ctx *ctx, int view_cursor, char *,
		struct tsk_statement *statement) {
	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	if (item->type == IT_LINK) {
		n_fs_systemf ("xdg-open %s", item->body);
		return 0;
	}
	u_each (str, item->props->n, item->props->d) {
		if (str->type != IT_LINK)
			continue;
		n_fs_systemf ("xdg-open %s", *str);
	}
	return 0;
}

static int action_remove (tsk_ctx *ctx, tsk_view *view, int view_cursor) {
	auto item = ctx->items.d + view->d[view_cursor].item_idx;
	item->has |= (1 << IP_REMOVE);
	return 0;
}

int action_remind (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		bool props_were_specified, enum prop_id print_what,
		struct action_piece action_piece, struct tsk_statement *statement) {

	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;

	if (

			   !(item->has & (1 << NP_ON))
			|| !(item->has & (1 << NP_REMINDER))
			|| (item->has & (1 << NP_DONE)))
		return 0;

	// The time we wait after last-reminded to remind again.
	time_t reminder_dur = TS_SECS_FROM_HOURS (1);

	if (item->times.on.time.ts < TIMES.now.time.ts

			// Remind up to an hour after the thing. Hard-coded.
			+ reminder_dur)
		return 0;

	if (       (item->has & (1 << NP_LAST_REMINDED))
			&& TIMES.now.time.ts - item->times.last_reminded.time.ts < reminder_dur)
		return 0;

	if (	   item->has & (1 << NP_ENDING)
			&& item->times.ending.time.ts > TIMES.now.time.ts)
		return 0;

	tsk_print (ctx, cmd, statement->view->d[view_cursor], view_cursor,
			props_were_specified, print_what);

	char *msg;
	char time[256];
	ts_str_from_time (time, 256, &item->times.on.time, TSK_ISO_DATE);
	asprintf (&msg, "%s\\n%s", item->body, time);
	char *escaped_msg = tsk_vim_strsave_shellescape (msg, 0);
	free (msg);

	char *shell_cmd = tsk_get_str ("\
			dunstify -t 100000 -a  Note Note \"%s\" --action=C,Close \
			--action=A,\"Acknowledged; don't remind until last-reminded up\" \
			--action=D,Done",
			escaped_msg);
	char *user_resp = slurp_pipe (shell_cmd);
	if (!user_resp) {
		action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement,
				"\"%s\" failed. Really, we should print stderr here?", shell_cmd);
		return 1;
	}
	free (shell_cmd);
	if (*user_resp != 'C') {
		if (*user_resp == 'D')
			item->has = (1 << NP_DONE);
		else { // 'A'
			item->times.last_reminded = (struct ts_constant) {
				.time = TIMES.now.time, .type = TS_CONSTANT_TYPE_TIME
			};
			item->has |= (1 << NP_LAST_REMINDED);
		}
	}
	free (user_resp);
	free (escaped_msg);

	return 0;
}

static int do_the_add (tsk_ctx *ctx, tsk_cmd *cmd, struct tsk_ints *pr,
		tsk_notefile *matching_notefile, char *path_or_basename,
		enum notefile_flags flags, bool create, tsk_item *item) {
	if (!matching_notefile) {
		struct rn_err_info rn_err_info;
		auto notefile = create_notefile_struct (ctx, path_or_basename, flags);
		if (n_fs_fexists (notefile.path)) {
			if (read_notefile (ctx, &notefile, 0,
					&rn_err_info, &ctx->items)) {
				tsk_warn (ctx, cmd, "Couldn't load in \"%s\" to move a note to");
				free_notefile (&notefile);
				return 1;
			}
			arr_add (pr, ctx->notefiles.n - 1);
		} else if (!create) {
			tsk_warn (ctx, cmd, "\
You're trying move a note to a notefile that doesn't exist. If you want to do that, \
use one of \"create\" file-move variations, \"Filecreate\", \"Sfilecreate\", \
\"Copycreate\", \"Scopycreate\"");

			free_notefile (&notefile);
			return 1;
		} else {
			arr_add (&ctx->notefiles, notefile);
			arr_add (pr, ctx->notefiles.n - 1);
		}
	} else {

		// Don't add notefiles that the item is already in.
		int matching_notefile_idx = matching_notefile - ctx->notefiles.d;
		for (int i = 0; i < item->notefile_idxes.n; i++) {
			if (matching_notefile_idx == item->notefile_idxes.d[i])
				return 0;
		}
		arr_add (pr, matching_notefile_idx);
	}
	return 0;
}

// Returns an int array by parameter though only one match can
// actually be found. This is because copy_or_move_item can deal with
// sfiles, where you can copy to multiple.
[[nodiscard]]
static int add_normal_notefile_idxes (tsk_ctx *ctx, tsk_cmd *cmd, struct tsk_ints *pr,
		char *path, bool create, tsk_item *item) {
	auto matching_notefile = arr_find (&ctx->notefiles,
			&(tsk_notefile) {.path = path}, cmp_notefiles_by_path);
	return do_the_add (ctx, cmd, pr, matching_notefile, path, 0, create, item);
}

[[nodiscard]]
static int add_shortcut_notefile_idxes (tsk_ctx *ctx, tsk_cmd *cmd, struct tsk_ints *pr,
		int view_cursor, struct action_piece action_piece,
		struct tsk_statement *statement, bool create, tsk_item *item) {
	char err_buf[TSK_MAX_ERR_BUF];
	struct tsk_ints sfile_matches = {};
	int rc = regmatch_arr (&sfile_matches, action_piece.str, ctx->sfiles.n, ctx->sfiles.d,
			err_buf);
	if (rc) {
		action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement,
				"%s", err_buf);
		return 1;
	}
	if (!sfile_matches.n) {
		if (!create) {
			action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, 0, statement, "\
No shortcut notefile matches \"%s\". if you want to create it use Sfilecreate or \
Scopycreate", action_piece.str);
			return 1;
		}
		if (do_the_add (ctx, cmd, pr, nullptr, action_piece.str, TSK_FILE_FLAG_IS_SHORTCUT,
					create, item))
			return 1;
	} else {
		arr_each (&sfile_matches, sfile_match) {
			char full_path[PATH_MAX];
			char *basename = ctx->sfiles.d[*sfile_match];
			snprintf (full_path, PATH_MAX, "%s/%s", ctx->opts->data_dir, basename);
			auto matching_notefile = arr_find (&ctx->notefiles,
					&(tsk_notefile) {.path = full_path}, cmp_notefiles_by_path);
			if (do_the_add (ctx, cmd, pr, matching_notefile, basename, TSK_FILE_FLAG_IS_SHORTCUT,
						create, item))
				return 1;
		}
	}
	return  0;
}

// This maybe could be better. We change the item's notefile id, hence
// "copying it"; we read a new notefile if we need; we set the item's
// "froms".
[[nodiscard]]
static int copy_or_move_item (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece, bool is_shortcut, bool move,
		bool create, struct tsk_statement *statement) {

	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	if (item->type != IT_NOTE && item->type != IT_AUTOCMD) {
		action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, nullptr, statement,
				"You can't copy %ss", ITEM_TYPE_KEYWORDS[item->type].long_name);
		return 1;
	}

	struct tsk_ints matches = {};

	int rc = is_shortcut ?
		add_shortcut_notefile_idxes (ctx, cmd, &matches, view_cursor,
				action_piece, statement, create, item) :
		add_normal_notefile_idxes (ctx, cmd, &matches, action_piece.str, create, item);
	if (rc)
		return 1;

	// Trickiness: add_shortcut_notefile_idxes and
	// add_normal_notefile_idxes can return 0 and have it not be an
	// error if you're trying to move an item into a notefile it's
	// already in. I don't want you accidentally adding a note to the
	// notefile it's in and having it get its own notefile as a
	// "from". That's just crap.
	if (!matches.n)
		return 0;

	// We might have reallocated items.
	// to check.
	item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	if (!item->froms)
		item->froms = u_calloc (sizeof *item->froms);

	assert (ctx->notefiles.n);
	arr_add (item->froms,

			// Note that the "from" we add is the note's *last*
			// notefile ID. This means that if, in the TUI, you copied
			// a note to one file and then copied it to the other, the
			// note would get two froms. Another way of doing it would
			// be to only use the first from, but I think this is more
			// comprehensible.
			tsk_make_item_with_body (ctx->notefiles.d[*arr_last (&item->notefile_idxes)].path,
				IT_FROM, TIMES.now.time));

	// This line here means you can't do this:
	// 		note Sfile=ons Sfile=notes
	// ... and expect it to move the files to ons and notes and remove
	// it from the one it's in. It would end up in only notes.
	if (move)
		item->notefile_idxes.n = 0;
	item->has |= (1 << IP_COPY);

	// Right now, this line means that copied items will get the
	// "from" of the notefile they were copied to. Not the one that
	// ultimately ends up in that other notefile, the one that
	// remains. How to solve this? You could solve it in
	// tsk_run_autocmds, but then any notefile that use "from" would
	// match items that have been copied, not just their copies. Well,
	// the simple solution is to actually make a new item here. I'm
	// not keen on that.
	arr_each (&matches, _)
		arr_add (&item->notefile_idxes, *_);

	return 0;
}

static int action_copy (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece,
		struct tsk_statement *statement) {
	return copy_or_move_item (ctx, cmd, view_cursor, action_piece, false, false, false,
			statement);
}

static int action_copy_create (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece, struct tsk_statement *statement) {
	return copy_or_move_item (ctx, cmd, view_cursor, action_piece, false, false, true,
			statement);
}

static int action_scopy (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece,
		struct tsk_statement *statement) {
	return copy_or_move_item (ctx, cmd, view_cursor, action_piece, true, false, false,
			statement);
}

static int action_scopy_create (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece,
		struct tsk_statement *statement) {
	return copy_or_move_item (ctx, cmd, view_cursor, action_piece, true, false, true,
			statement);
}

static int action_file (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece,
		struct tsk_statement *statement) {
	return copy_or_move_item (ctx, cmd, view_cursor, action_piece, false, true, false,
			statement);
}

static int action_file_create (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor, struct action_piece action_piece,
		struct tsk_statement *statement) {
	return copy_or_move_item (ctx, cmd, view_cursor, action_piece, false, true, true,
			statement);
}

static int action_sfile (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece, struct tsk_statement *statement) {
	return copy_or_move_item (ctx, cmd, view_cursor, action_piece, true, true, false,
			statement);
}

static int action_sfile_create (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece, struct tsk_statement *statement) {
	return copy_or_move_item (ctx, cmd, view_cursor, action_piece, true, true, true,
			statement);
}

static void print_help_entry (tsk_ctx *ctx, int token) {

	auto cmd_keyword = &CMD_KEYWORDS[token];
	auto keyword = &cmd_keyword->keyword;

	// If there's no long or short token, it's something like
	// CTK_END, ie, not really a token.
	if (!keyword->long_name && !keyword->short_name

			// ":" is CTK_NOT_SET. See the top of cmd.h for half a
			// justification of this.
			|| (keyword->short_name == ':'))
		return;
	char wrapped_text[TSK_N_SCRATCH];
	struct wrap_opts wrap_opts = {};

#define PF(indentation_level, fmt, ...) \
	do { \
		wrap_opts.initial_tabs = indentation_level; \
		wrap (wrapped_text, TSK_N_SCRATCH, &wrap_opts, fmt __VA_OPT__(,) __VA_ARGS__); \
		ctx->pc->msg (ctx->pc, "%s\n", wrapped_text); \
	} while (0)

	char short_name_text[8];
	int l = 0;
	if (keyword->short_name)
		l += sprintf (short_name_text, "%c", keyword->short_name);
	else
		l += sprintf (short_name_text, " ");
	assert (l < 7);
	l = 0;
	char long_name_text[64];
    if (keyword->long_name)
		l += sprintf (long_name_text, "| %s", keyword->long_name);
    else
		l += *long_name_text = 0;
	assert (l < 63);

	PF (0, "%s %s%s%s\n",
			short_name_text,
			long_name_text,
			keyword->args_descr ? "=" : "",
			keyword->args_descr ? keyword->args_descr : "");
	if (keyword->long_help_descr)
		PF (1, "%s\n", keyword->long_help_descr);
	if (keyword->examples_str) {
		for (const char *p = keyword->examples_str;;) {

			char buf[TSK_N_SCRATCH];
			memset (buf, 0, TSK_N_SCRATCH);

			char *tab = strchr (p, '\t');
			assert (tab);
			strncpy (buf, p, tab - p);

			PF (1, "%s", buf);
			memset (buf, 0, TSK_N_SCRATCH);
			tab++;
			char *descr_end = strchrnul (tab, '\n');
			strncpy (buf, tab, descr_end - tab);
			PF (2, "%s\n", buf);
			if (*descr_end == 0)
				break;
			p = descr_end + 1;
		}
	}
#undef PF
}

static int action_help (tsk_ctx *ctx, tsk_cmd *cmd, struct action action,
		struct tsk_statement *statement) {

	struct tsk_ints help_idxes = {};

	char err_buf[TSK_MAX_ERR_BUF];

	// We gather all of the messages before printing them so we don't
	// print some and then error, which looks bad.
	arr_each (&action.pieces, action_piece) {
		if (!action_piece->str) {
			for (int i = 0; i < N_CTK; i++)
				arr_add (&help_idxes, i);
			continue;
		}
		int rc = append_to_comma_list_indexes (&help_idxes, action_piece->str,
				action_piece->str ? strchr (action_piece->str, 0) : 0,
				N_CTK, CMD_KEYWORDS, TSK_CMD_KEYWORD_LONG_NAME_GETTER, err_buf);
		if (rc) {
			action_err (ctx, cmd, 0, action_piece->cmd_words_idx, 0, statement,
					"Bad help thing: %s", err_buf);
			return 1;
		}

		// Note it's an error to not match anything at all in a help
		// action piece. With "Help=sdlkfjdsf Help=interval" you'd get
		// an error. But no with "Help=sdflkd,interval". In that case
		// you match something. I think that's fine for a help lookup,
		// since you don't know the names of things. You can use
		// regexes in Help actions, like "Help=b.*,o.*". That's two
		// regexes, two searches. It would be best to show exactly
		// which regex failed. Here we print the whole string.
		if (!help_idxes.n) {
			action_err (ctx, cmd, AE_NO_ITEM_TO_PRINT, action_piece->cmd_words_idx, 0, statement,
					"Nothing in \"%s\" matched anything", action_piece->str);
			return 1;
		}
	}

	BEGIN_MSG (ctx);
	arr_each (&help_idxes, _)
		print_help_entry (ctx, *_);
	END_MSG (ctx);
	free (help_idxes.d);
	return 0;
}

static int action_write (tsk_ctx *ctx, tsk_cmd *cmd) {
	write_to_notefiles (ctx, cmd, &ctx->notefiles, &ctx->items);
	return 0;
}

[[noreturn]]
static void action_write_quit (tsk_ctx *ctx, tsk_cmd *cmd) {
	write_to_notefiles (ctx, cmd, &ctx->notefiles, &ctx->items);
	exit (0);
}

static int check_prop_deps (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		struct action_piece action_piece, struct tsk_statement *statement) {

	// -1 means it's not a prop.
	if (action_piece.prop == -1)
		return 0;
	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	int missing_dep = note_is_missing_this_prop_dep (item, action_piece.prop);
	if (missing_dep > -1) {
		action_err (ctx, cmd, view_cursor, action_piece.cmd_words_idx, nullptr, statement,
				"Can't have %s without %s", PROP_KEYWORDS[action_piece.prop].long_name,
				PROP_KEYWORDS[missing_dep].long_name);
		return 1;
	}
	return 0;
}

static int do_item_action_piece (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor,
		bool props_were_specified, enum prop_id print_what,
		struct action_piece action_piece, enum action_id action_id,
		struct tsk_statement *statement) {

	int r;
	switch (action_id) {
	case A_REMIND:       r = action_remind (ctx, cmd, view_cursor, props_were_specified,
						 		print_what, action_piece, statement); break;
	case A_PRINT_TAGS:   r = print_tags (ctx, view_cursor, action_piece.str, statement); break;
	case A_CHANGE:       r = action_change (ctx, cmd, statement->view, view_cursor,
						 		action_piece, statement); break;
	case A_FMT:          r = tsk_print_fmt (ctx, cmd, view_cursor, action_piece, &ctx->items, statement); break;
	case A_REMOVE:       r = action_remove (ctx, statement->view, view_cursor); break;
	case A_UNSET:        r = action_unset (ctx, cmd, view_cursor, action_piece, statement); break;
	case A_OPEN:         r = action_open (ctx, view_cursor, action_piece.str, statement); break;
	case A_FILE:         r = action_file (ctx, cmd, view_cursor, action_piece, statement); break;
	case A_FILE_CREATE:  r = action_file_create (ctx, cmd, view_cursor, action_piece, statement); break;
	case A_SFILE:        r = action_sfile (ctx, cmd, view_cursor, action_piece, statement); break;
	case A_SFILE_CREATE: r = action_sfile_create (ctx, cmd, view_cursor, action_piece, statement); break;
	case A_SCOPY:        r = action_scopy (ctx, cmd, view_cursor, action_piece, statement); break;
	case A_SCOPY_CREATE: r = action_scopy_create (ctx, cmd, view_cursor, action_piece, statement); break;
	case A_COPY:         r = action_copy (ctx, cmd, view_cursor, action_piece, statement); break;
	case A_COPY_CREATE:  r = action_copy_create (ctx, cmd, view_cursor, action_piece, statement); break;
	case A_NOP:          r = 0; break; // Do nothing; same as Update.
	default:             assert  (0);
	}
	if (r || (r = check_prop_deps (ctx, cmd, view_cursor, action_piece, statement)))
		return r;

	return r;
}

int tsk_get_print_what (enum prop_id *pr, char *str, char err_buf[TSK_MAX_ERR_BUF]) {
	return get_bitfield_from_comma_list (str, strchr (str, 0), N_PROPS, PROP_KEYWORDS,
			TSK_KEYWORD_LONG_NAME_GETTER,
			err_buf, (int *) pr);
}

static int expand_mod_print_what (tsk_ctx *ctx, tsk_cmd *cmd, struct action *action,
		struct tsk_statement *statement) {

	auto print_what = &action->mods.print_what;
	auto pieces = &action->mods.print_what.pieces;
	if (!pieces->n) {
		if (action->id == A_PRINT && ctx->opts->print_what) {
			print_what->val.prop_id = ctx->opts->print_what;
			return 0;
		}
		if (action->id == A_PRINT &&

				// This line here suggests edit_in_editor should be an
				// action after all. But then we do do other actions
				// and then it.
				!action->edit_in_editor.val.yes)
			print_what->val.prop_id = (unsigned) -1;
		else
			print_what->val.prop_id = 0;
		return 0;
	}

	auto val = &print_what->val;
	int r = 0;
	arr_each (pieces, _) {
		if (!_->str) {
			val->prop_id |= (unsigned) -1;
			continue;
		}
		char err_buf[TSK_MAX_ERR_BUF];
		int rc = tsk_get_print_what (&val->prop_id, _->str, err_buf);
		if (rc) {
			action_err (ctx, cmd, AE_NO_ITEM_TO_PRINT, _->cmd_words_idx, 0, statement,
					"Bad :print: %s", err_buf);
			return rc;
		}
	}

	return r;
}

static int append_to_saved_items_if_not_already_there (tsk_ctx *ctx,
		struct saved_items *saved_items, tsk_view *view,
		enum confirmation_mode confirmation_mode, struct tsk_statement *statement) {

	// We don't bother saving each property. They're not sub-notes. I
	// do think we should just define an "obj" type, and props become
	// those. Though probably items would have to go inside them. In
	// which case we can just use a pointer and thus save space.
	if (view != statement->view ||
			!(statement->flags & SF_ACTION_MODIFIES_NOTES))
		return 0;
	arr_each (view, _) {
		auto item = ctx->items.d + _->item_idx;
		arr_each (saved_items, saved_item) {
			if (saved_item->item.item_idx == item - ctx->items.d)
				return 0;
		}
		arr_add (saved_items,
				((struct saved_item) {
					.item = tsk_item_dup (item, 1),
					.confirmation_mode = confirmation_mode,
			}));
		arr_last (saved_items)->item.item_idx = item - ctx->items.d;
	}
	return 0;
}

static int print_msg (tsk_ctx *ctx, tsk_cmd *cmd, int view_cursor, char *msg,
		struct tsk_statement *statement) {
	auto item = ctx->items.d + statement->view->d[view_cursor].item_idx;
	struct display_str_opts display_str_opts = {
		.no_wrap = 1,
		.gqesm_mode = GQESO_SURROUND_WITH_QUOTES_ALWAYS,

		// Definitely don't need or want to print the whole text
		// since the text will definitely contain the actual message.
		// And let's use this value for bodies, too.
		.truncate_len = ctx->opts->truncate_len,
	};
	char cmd_text[BUFSIZ], item_text[BUFSIZ];
	get_display_str (ctx, sizeof cmd_text, cmd_text, &display_str_opts,
			cmd->text);
	get_display_str (ctx, sizeof item_text, item_text, &display_str_opts,
			item->body);
	char out[BUFSIZ];
	wrap (out, sizeof out, &(wrap_opts) {.second_line_indent_incr = 2},
			"A message from %s %s, operating on %s %s", CMD_SOURCE_STRS[cmd->source],
			cmd_text, ITEM_TYPE_KEYWORDS[item->type].long_name, item_text);
	start_colour (ctx, TSK_COLOUR_ERROR_HEADER);
	tsk_plain_msg (ctx, "%s\n", out);
	stop_colour (ctx);
	wrap (out, sizeof out, &(wrap_opts) {.second_line_indent_incr = 1,
			.overall_indent = 4}, "%s", msg);
	tsk_plain_msg (ctx, "%s\n", out);
	return 0;
}

static int do_prop_action_piece (tsk_ctx *ctx, tsk_view *view, int view_cursor,
		struct action_piece action_piece, enum action_id action_id) {

	int r = 1;
	switch (action_id) {
		case A_CHANGE: r = action_change_prop (ctx, view, view_cursor, action_piece); break;
		case A_REMOVE: r = action_remove (ctx, view, view_cursor); break;
		default: assert (!"You can't have this action_id for props");
	}
	return r;
}

// Note this function does not stand all by items. It requires items
// to point to an item's array props.
static int do_prop_actions (tsk_ctx *ctx, struct action action, tsk_view *view) {

	int r;
	$fori (i, view->n) {
		arr_each (&action.pieces, _)
			r = do_prop_action_piece (ctx, view, i, *_, action.id);
	}
	// With props, you can only change their bodies.
	return r;
}

static int do_item_actions (tsk_ctx *ctx, tsk_cmd *cmd, struct action action, tsk_view *view,
		struct saved_items *saved_items, struct tsk_statement *statement) {

	int r = 0;

	switch (action.id) {
	case A_COUNT:       tsk_plain_msg (ctx, "%zu\n", view->n); return 0;
	case A_ADD:         init_added_items (ctx, cmd, action, &ctx->items, statement);
						action.id = A_CHANGE;
						break;
	case A_QUIT:        exit (0);
	case A_WRITE:       r = action_write (ctx, cmd); return 0;
	case A_WRITE_QUIT:  action_write_quit (ctx, cmd); return 0;
	default: break;
	}

	append_to_saved_items_if_not_already_there (ctx, saved_items, view,
			action.mods.confirmation_mode, statement);

	$fori (i, view->n) {

		// Understand that the cursor is the index into the *view*
		// array.
		view_node *view_node  = view->d + i;

		auto item = ctx->items.d + view_node->item_idx;

		// It's not sensible to use recursion when printing because
		// the important part of that function is the *printing*, the
		// formatting. It'd be convoluted as fuck to funnel printing
		// through this recursion model.
		if (action.id != A_PRINT && view->d[i].next) {
			auto real_items = ctx->items;
			ctx->items = *item->props;
			do_prop_actions (ctx, action, view->d[i].next);
			ctx->items = real_items;
			continue;
		}

		$fori (j, action.pieces.n) {
			if (action.msg)
				print_msg (ctx, cmd, i, action.msg, statement);
			r = do_item_action_piece (ctx, cmd, i, view->props_were_specified,
					action.mods.print_what.val.prop_id, action.pieces.d[j], action.id,
					statement);
			if (r)
				goto out;
		}
		if (action.edit_in_editor.val.yes) {
			r = tsk_change_in_editor (ctx, cmd, view, statement,

					// This is pass so if something goes wrong we can
					// print a proper error message, because the
					// mod_pieces have cmd_words_idx in them. Even
					// though it's possible to specify :edit more than
					// once, tsk_change_in_editor will only be called
					// once; hence we only pass the first
					// edit_in_editor mod_piece.
					action.edit_in_editor.pieces.d[0]);
			if (r)
				goto out;
		}
	}

out:

	return r;
}

static bool items_prop_id_bits_are_equal (struct tsk_item *saved_item,
		struct tsk_item *item, enum prop_id prop_id) {
	bool saved_set = saved_item->has & (1 << prop_id);
	bool item_set = item->has & (1 << prop_id);
	return saved_set == item_set;
}

static bool is_change (struct tsk_item *saved_item, struct tsk_item *item,
		enum prop_id prop_id) {

	void *saved_item_member = prop_id_to_member (saved_item, prop_id);
	void *item_member = prop_id_to_member (item, prop_id);
	bool is_different = !items_prop_id_bits_are_equal (saved_item, item, prop_id);
	if (!is_different)
		is_different = cmp_prop (saved_item_member, item_member, prop_id);
	if (is_different)
		return 1;
	return 0;
}


static bool set_changed_props_bit (struct tsk_item *saved_item,
		struct tsk_item *item, enum prop_id prop_id, enum prop_id *props_changed) {

	if (is_change (saved_item, item, prop_id)) {
		*props_changed |= (1 << prop_id);
		return 1;
	}
	return 0;
}

// Returns the number of props changed.
static int get_prop_id_bitfield_of_changes (tsk_ctx *ctx, enum prop_id *pr,
		struct tsk_item *saved_item, int item_idx) {

	int r = 0;
	auto item = ctx->items.d + item_idx;

	r += set_changed_props_bit (saved_item, item, IP_BODY, pr);
	r += set_changed_props_bit (saved_item, item, IP_TYPE, pr);
	r += set_changed_props_bit (saved_item, item, IP_NAME, pr);

	if (item->type == IT_NOTE) {

		$fori (i, N_TIMES) {
			if (i != TIME_CREATION && i != TIME_NOW)
				r += set_changed_props_bit (saved_item, item,
						time_id_to_prop(i), pr);
		}

		r += set_changed_props_bit (saved_item, item, NP_EFFORT, pr);
		r += set_changed_props_bit (saved_item, item, NP_PRIORITY, pr);

		r += set_changed_props_bit (saved_item, item, NP_TAG, pr);
		r += set_changed_props_bit (saved_item, item, NP_ANNOTATION, pr);
		r += set_changed_props_bit (saved_item, item, NP_LINK, pr);
		r += set_changed_props_bit (saved_item, item, NP_ATTACHMENT, pr);

	}

	r += set_changed_props_bit (saved_item, item, IP_NOTEFILE_IDS, pr);

	// Mop up all of the other properties that have changed, the ones
	// which had values and now don't and vice-versa, and the one that
	// don't have string values.
	$fori (i, N_PROPS) {
		if (/* Course it's changed */ i == IP_CHANGED ||
				i == IP_NEW ||
				/* Should be caught by IP_NOTEFILE_IDS */
				i == IP_COPY)
			continue;

		// If the prop bit hasn't already been marked as changed and
		// it's been the two items' bits are not equal mark it as
		// changed.
		if (!(*pr & (1 << i)) && !items_prop_id_bits_are_equal (saved_item, item, i)) {
			*pr |= (1 << i);
			r++;
		}
	}
	return r;
}


static int free_confirmations (struct tsk_confirmations *confirmations) {
	free (confirmations->d);
	*confirmations = (typeof (*confirmations)) {};
	return 0;
}

static enum tsk_confirmation_type get_confirmation_type (enum prop_id old_item_has,
		enum confirmation_mode confirmation_mode) {

	if (confirmation_mode == CM_FORCE)
		return TCT_NO_CONFIRMATION_NECESSARY;
	else if (old_item_has & (1 << IP_SUPER_LOCK))
		return TCT_SUPER_LOCK;
	else if (old_item_has & (1 << IP_LOCK))
		return TCT_LOCK;
	else if (confirmation_mode == CM_CONFIRM)
		return TCT_CONFIRM;
	return TCT_NO_CONFIRMATION_NECESSARY;
}

// Returns the number of changed items, which are otherwise ignored.
// Only items that need confirmations are returned. If you wanted you
// could return them all, as a single array maybe, though you'd have
// to make the user (me) do more in their multi-confirm func, or you
// could return them as a struct with two arrays. Probably that's the
// best way. But we don't care about just changed items for now.
int make_confirmations_return_n_changed_items (tsk_ctx *ctx, tsk_cmd *cmd,
		struct tsk_confirmations *pr, struct saved_items *saved_items) {
	*pr = (struct tsk_confirmations) {};
	int r = 0;
	arr_each (saved_items, _) {

		auto changed_item = ctx->items.d + _->item.item_idx;

		struct tsk_confirmation confirmation = {};
	 	int n_props_changed = get_prop_id_bitfield_of_changes (ctx,
				&confirmation.changed_props, &_->item, _->item.item_idx);
		if (!n_props_changed)
			continue;

		r++;

		confirmation.type = get_confirmation_type (_->item.has,

				// This is a fudge. The confirmation mode is set in
				// the statement but we don't call this in a loop over
				// the statements, so I'm just going to do this.
				// Presumably we need to do it in a loop. I can
				// imagin that I didn't want to do that, would rather
				// be prompted fot all the statements' items at once.
				// But in that case what to do about the confirmation
				// mode? Maybe, then, confirmation mode shouldn't be
				// unique for each statement.
				cmd->statements.d[0].action.mods.confirmation_mode);

		// Note this line here filters out notes that don't have
		// to be confirmed. We do it after getting the number of
		// changes so we can return the number of changes. We
		// just don't add them to the confirmations array because
		// we don't care, and keeping them (non-confirmations,
		// if changes) in the confirmations array is useless info
		// since we don't have a --print-changes option. If you
		// want to implement said option, take this away.
		// You'll have to do extra logic in the main.c
		// confirmation loop. I just didn't want "the user" (me)
		// to have to do something annoying like pointer
		// arithmetic based on whether the confirmation was a real
		// one or not.
		if (confirmation.type == TCT_NO_CONFIRMATION_NECESSARY)
			continue;

		confirmation.old_item = &_->item;
		confirmation.new_item = changed_item;

		arr_add (pr, confirmation);
	}
	return r;
}

// Returns the number of changes actually done. If you roll back
// a change by saying no to the confirmation prompt, that's not a
// change.
static int maybe_restore_items (tsk_ctx *ctx, tsk_cmd *cmd, struct saved_items *saved_items) {

	struct tsk_confirmations confirmations;
	int r = make_confirmations_return_n_changed_items (ctx, cmd, &confirmations,
			saved_items);
	if (confirmations.n)
		ctx->pc->multi_confirm (ctx, cmd, &confirmations);

	arr_each (&confirmations, _) {
		if (_->new_item->flags & TIF_USER_CANCELLED_EDIT_IN_EDITOR ||
				!_->user_confirms) {

			// In write_item we assert against writing notes with
			// empty bodies. But if the user says no the
			// saved_item, which is what gets passed along here,
			// will be empty. So we add IP_REMOVE so we don't
			// write it.
			if (_->new_item->has & (1 << IP_NEW))
				_->new_item->has |= (1 << IP_REMOVE);

			tsk_free_item (_->new_item);
			*_->new_item = tsk_item_dup (_->old_item, 1);
			r--;
		}
	}

	free_confirmations (&confirmations);

	return r;
}

static int expand_str_insert_shell_result (tsk_ctx *ctx, tsk_cmd *cmd, FILE *f, char **p,
		char *s, char *e, int cmd_words_idx, struct tsk_statement *statement) {

	// We pass everything from the \%{ so that we can print a decent
	// error message.
	char *shell_cmd_start = *p + 4;
	char *close_curly = rab_find_unescaped_ch (shell_cmd_start, e, '}');
	int r = 1;
	if (!close_curly) {
		action_err (ctx, cmd, AE_NO_ITEM_TO_PRINT, cmd_words_idx, 0, statement, "\
In \"%.*s\", you've started expansion wiht \"\\%%{\" but you haven't closed it off \
with \"}\"", e - s, s);
		return r;
	}

	char *shell_cmd = strndup (shell_cmd_start, close_curly - shell_cmd_start);

	auto line = make_line (shell_cmd, GLF_IS_PIPE);
	if (!line.f) {
		action_err (ctx, cmd, AE_NO_ITEM_TO_PRINT, cmd_words_idx, 0, statement, "Bad shell command in %s",
				*p);
		nuke_line (&line);
		goto out;
	}
	ssize_t shell_result_len = get_delim (&line, 0);
	if (shell_result_len == -1) {
		action_err (ctx, cmd, AE_NO_ITEM_TO_PRINT, cmd_words_idx, nullptr, statement,
				"Shell command %.*s failed", (close_curly - *p), *p);
		goto out;
	}
	if (!line.buf || rab_is_all_x (line.buf, isspace)) {
		action_err (ctx, cmd, AE_NO_ITEM_TO_PRINT, cmd_words_idx, nullptr, statement,
				"After expanding str \"%s\", you've ended up with an empty one", s);
		return r;
	}
	r = 0;
	fputs (line.buf, f);
out:
	*p = close_curly + 1;
	nuke_line (&line);
	return r;
}

static int expand_str_insert_user_input (tsk_ctx *ctx, tsk_cmd *cmd, FILE *f, char **p,
		char *e, char *property_name) {
	int r = 1;
	char *cmd_start = *p + 3 + /* u_len ("Get-input") */ 9;
	char *close_curly = rab_find_unescaped_ch (cmd_start, e, '}');
	struct wrap_opts wo = {.second_line_indent_incr = 1};
	char quoted_cmd_text[BUFSIZ];
	get_display_str (ctx, sizeof quoted_cmd_text, quoted_cmd_text,
			&(dso) {.no_wrap = 1, .gqesm_mode = GQESO_SURROUND_WITH_QUOTES_ALWAYS},
			cmd->text);
	char wrapped[BUFSIZ];
	if (*cmd_start == ':')
		wrap (wrapped, sizeof wrapped, &wo, "From cmd %s:\n\t%.*s",
				quoted_cmd_text, close_curly - cmd_start, cmd_start);
	else if (property_name)
		wrap (wrapped, sizeof wrapped, &wo, "From cmd %s:\n\tEnter %s", quoted_cmd_text,
				property_name);
	else
		wrap (wrapped, sizeof wrapped, &wo, "From cmd %s:\n\tEnter something",
				quoted_cmd_text, property_name);
	char resp[BUFSIZ];
	if (ctx->pc->input (sizeof resp, resp, ctx->pc, "%s: ", wrapped))

		// It's not an error to get no response; don't print an error
		// message.
		return r;
	fputs (resp, f);
	r = 0;
	*p = close_curly + 1;
	return r;
}

static int expand_str (tsk_ctx *ctx, tsk_cmd *cmd, char **pr, char *s, char *e,
		char *property_name, int cmd_words_idx, struct tsk_statement *statement) {

	if (!e)
		e = strchr (s, 0);

	size_t size;
	FILE *f = open_memstream (pr, &size);
	assert (f);
	*pr = 0;

	// I'm reserving this so if you do something like this: 'note
	// "\%"', you'll not get a segfault. Because "\%" expands to
	// nothing, and I do that by skipping the arr_insert_mul call
	// below.
	int r = 1;

#define not_escaped(_p, _s) (_p == _s || (*(_p - 1) != '\\'))

	for (char *p = s; p != e; p++) {

		// If a str starts with !, it's a shell command. Run it
		// and replace the whole of r with it.
		if (*p == '\\' && p[1] == '%' && p[2] == '{' && not_escaped (p, s)) {
			if (p[3] == '!') {
				if (expand_str_insert_shell_result (ctx, cmd, f, &p, s, e, cmd_words_idx,
							statement))
					goto out;
			} else if (!strncmp (p + 3, "Get-input", 9)) {
				if (expand_str_insert_user_input (ctx, cmd, f, &p, e, property_name))
					goto out;

			}
		}
		if (!*p)
			break;
		fputc (*p, f);
	}

	r = 0;
	// Stick the 0 on the end.
out:
	assert (!fclose (f));
	if (r == 1) {
		free (*pr);
		*pr = 0;
	}
	return r;
}

int expand_action (tsk_ctx *ctx, tsk_cmd *cmd, struct action *action,
		struct tsk_statement *statement) {
	arr_each (&action->pieces, _) {
		if (!_->str)
			return 0;
		char *new;
		char *str_to_print;
		if (_->prop != -1)
			str_to_print = PROP_KEYWORDS[_->prop].long_name;
		else
			str_to_print = ACTION_ID_STRS[action->id];
		if (expand_str (ctx, cmd, &new, _->str, strchr (_->str, 0), str_to_print,
					_->cmd_words_idx, statement)) {
			action_err (ctx, cmd, AE_NO_ITEM_TO_PRINT, _->cmd_words_idx, 0, statement,
					"Couldn't expand \"%s\"", _->str);
			return 1;
		}
		free (_->str);
		_->str = new;
	}
	return 0;
}

static int expand_mods (tsk_ctx *ctx, tsk_cmd *cmd, struct action *action,
		struct tsk_statement *statement) {
	int rc = expand_mod_print_what (ctx, cmd, action, statement);
	if (rc)
		return rc;
	if (!action->mods.confirmation_mode)
		action->mods.confirmation_mode = ctx->opts->confirmation_mode;
	return 0;
}

static int do_statement (tsk_ctx *ctx, tsk_cmd *cmd, struct tsk_statement *statement,
		struct saved_items *saved_items) {

	// Setting a global.

	int r = 0;
	auto view = statement->view;
	auto action = &statement->action;

	if (action->id == A_LIST_SHORTCUTS) {
		if (!ctx->sfiles.n)
			tsk_warn (ctx, cmd, "No shortcuts in %s", ctx->opts->data_dir);
		else
			arr_each (&ctx->sfiles, _)
				tsk_plain_msg (ctx, "%s", *_);
	}
	int rc = expand_mods (ctx, cmd, action, statement);
	if (rc)
		return rc;


	if (expand_action (ctx, cmd, action, statement))
		return 1;

	// Nop and Update actually do the same thing -- nothing.
	// Autocmds do get run with both. That's why update exists.
	// Nop exists so you can make a list and not print it so you
	// can use it in the next statement. They're identical but I'm
	// leaving them in because you wouldn't think to use Nop to
	// mean "update", and wouldn't think to use Update to mean
	// "nop".
	if (action->id == A_NOP || action->id == A_UPDATE)
		return 0;

	if (action->id == A_HELP)
		action_help (ctx, cmd, *action, statement);
	else if (do_item_actions (ctx, cmd, *action, view, saved_items, statement))
		return 1;

	return r;
}

static int free_saved_item (struct saved_item *saved_item) {
	tsk_free_item (&saved_item->item);
	return 0;
}

static int free_saved_items (struct saved_items *saved_items) {
	if (!saved_items->a)
		return 0;
	arr_each (saved_items, _)
		free_saved_item (_);
	free (saved_items->d);
	*saved_items = (typeof (*saved_items)) {};
	return 0;
}

// Check all the statements over before getting into do_statement so
// that we don't start printing messages and then print this.
static int check_statements_over (tsk_ctx *ctx, tsk_cmd *cmd) {

	arr_each (&cmd->statements, _) {

		// I'm back-and-forth about whether this check should be done here
		// or in cmd.c. I notice now that this code means that if any
		// statement fails to match the whole thing does. That's probably
		// for the best.
		if (!_->view->n &&

				// You want Count to print 0 for no matches, not "no
				// matches"
				_->action.id != A_COUNT &&
				_->action.id != A_UPDATE &&
				_->action.id != A_NOP &&
				_->action.id != A_HELP) {

			action_err (ctx, cmd, AE_NO_ITEM_TO_PRINT,

					// This here means that if you don't match, the whole
					// line is considered an error, statement, and a squiggly is
					// drawn under all of it. It may be the case that you
					// could print something more correct, in cmd.c. You
					// could highlight the first failed list, or all
					// of them. Bear in mind that it's not an error to not
					// match something, else you couldn't look for thing
					// ("/this or /that").
					AE_ERR_IS_WHOLE_LINE, 0, _, "No matching items");
			return 1;
		}
	}
	return 0;
}

// Returns (1 << 22) for if you've done something that means a file
// will change. I'm not sure if that's ridiculous or not.
int tsk_do_cmd (tsk_ctx *ctx, struct tsk_cmd *cmd, bool *notes_were_updated) {

	*notes_were_updated = 0;
	// Setting a global.

	int r = 0, rc = 0;

	struct saved_items saved_items = {};

	bool update_was_used = 0;
	if (check_statements_over (ctx, cmd))
		return 1;
	arr_each (&cmd->statements, statement) {
		update_was_used |= statement->action.id == A_UPDATE;
		if (do_statement (ctx, cmd, statement, &saved_items))
			return rc;
	}

	int n_changes = maybe_restore_items (ctx, cmd, &saved_items);
	*notes_were_updated |= !!n_changes | update_was_used;
	free_saved_items (&saved_items);
	return r;
}


// Not sure if this function should be here. Maybe it should just be
// an action_loop sort of thing. What's the difference between an
// autocmd and a cmd? There is a difference. But should this lib be
// deciding that? Maybe, so things are consistent. Or maybe not.
//
// FIXME: shouldn't this return info telling you if the file has
// changed?
int tsk_run_autocmds (tsk_ctx *ctx, tsk_cmd *prev_cmd, bool *notes_were_updated) {

	// We get n_items beforehand and use it since items->n might grow
	// if an autocmd loads a file. And then *those* autocmds get ran
	// and so on. In the future we'll have a list for just matching
	// notefiles, not loading them too. Even with that we'll want to
	// do it like this since it will be possible for autocmds to load
	// notefiles -- unless we want to ban, and I don't see any reason
	// to.
	size_t n_items = ctx->items.n;
	$fori (i, n_items) {

		auto item = ctx->items.d + i;

		if (item->type == IT_AUTOCMD &&

				// Don't run autocmds right away, because changed
				// items are appended to the end. So if you changed
				// any autocmd to "n-1 ,'Whatever'" it would change
				// the autocmd itself.
				!(item->has & ((1 << IP_NEW) | (1 << IP_REMOVE))) &&

				// Don't run autocmds that were *only* loaded by by
				// actions like Sfile. Only run:
				// 		* Ones loaded by the user with "sfile" or
				// 		  "file" -- the "user" can be an autocmd.
				// 		* The ones from initial.nxt. That's loaded in
				// 		  tsk_update_ctx.
				item_was_loaded_by_at_least_one_list (ctx, item)) {

			size_t n_words;

			char **words = rab_shell_split (item->body, (ssize_t *) &n_words);
			// I'm using WRDE_NOCMD here. It's probably safer. Easier
			// to understand.
			if (!n_words)

				// tsk_err is good here. There's no error text to
				// print out at all. There's no err_info to gather
				// either.
				return tsk_warn (ctx, prev_cmd, "\
Got no words when splitting autocmd \"%s\"", item->body);

			auto autocmd = tsk_parse_cmd (ctx, words, n_words, CMD_SOURCE_AUTOCMD,
					prev_cmd);

			if (autocmd.flags & TCF_FAILED)
				return 1;

			arr_each (&autocmd.statements, statement) {
				assert (!(statement->flags & SF_FAILED));
				bool notes_were_updated_this_time = 0;
				if (statement->view->n && tsk_do_cmd (ctx, &autocmd, notes_were_updated))
					return 1;
				*notes_were_updated |= notes_were_updated_this_time;
			}
			tsk_free_cmd (&autocmd);
		}
	}
	return 0;
}
