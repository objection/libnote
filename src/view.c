#define _GNU_SOURCE
#include "view.h"
#include "lib/useful/useful.h"

tsk_view *view_clone (tsk_view *view) {
	if (!view)
		return 0;
	tsk_view *r = u_calloc (sizeof *r);
	if (view_copy (r, view))
		return 0;
	return r;
}

int view_copy (tsk_view *to, const tsk_view *from) {
	if (!from)
		return 0;
	$fori (i, from->n) {
		view_node new_node = {
			.item_idx = from->d[i].item_idx,
		};
		if (from->d[i].next) {
			new_node.next = u_calloc (sizeof *to->d[i].next);
			view_copy (new_node.next, from->d[i].next);
		}

		arr_add (to, new_node);
	}
	return 0;
}

// Frees all but the toplevel list. Most of the time you want to
// re-use the buffer.
int view_free (tsk_view *view) {
	if (!view)
		return 0;
	$fori (i, view->n) {
		if (view->d[i].next) {
			view_free (view->d[i].next);
			*view->d[i].next = (tsk_view) {};
			free (view->d[i].next);
			view->d[i].next = 0;
		}
	}
	if (view->a)
		free (view->d);
	*view = (typeof (*view)) {};
	return 0;
}

int view_set_diff (tsk_view *a, tsk_view *b) {

	tsk_view tmp = {};

	$fori (i, b->n) {
		int match = 0;
		$fori (j, a->n) {
			if (a->d[j].item_idx == b->d[i].item_idx)
				match = 1;
		}
		if (!match)

			// Note that we're copying the whole view_node. This might
			// cause chaos -- or at least memory leaks -- because of
			// the next pointers.
			arr_add (&tmp, b->d[i]);
	}
	free (a->d);
	*a = tmp;
	return 0;
}

void view_and (tsk_view *a, tsk_view *b) {

	tsk_view tmp = {};
	arr_reserve (&tmp, a->n);

	for (size_t i = 0; i < a->n; ++i) {
		bool seen = 0;
		for (size_t j = 0; j < b->n; ++j) {
			if (b->d[j].item_idx == a->d[i].item_idx) {
				seen = 1;
				break;
			}
		}
		if (seen)
			arr_add (&tmp, a->d[i]);
	}

	a->n = 0;
	arr_each (&tmp, _)
		arr_add (a, *_);
	free (tmp.d);
}

// Note (2023-01-29T01:50:46): Probably this function shouldn't be
// here. It's not especially to do with strings. But what are strings?
// I have a feeling this library might become just a grab-bag, maybe
// with a focus on generic functions. This function's adapted from
// something found on the internet.
int view_remove_dups (tsk_view *view) {

	$fori (i, view->n) {

        for (int j = i + 1; j < view->n; j++) {

            if (view->d[i].item_idx == view->d[j].item_idx) {

				assert (!view->d[j].next);

				size_t remaining_elems = view->n - j;
				memcpy (view->d + j, view->d + 1,
						remaining_elems * sizeof *view->d);

                view->n--;

                j--;
            }
        }
    }
	return 0;
}
